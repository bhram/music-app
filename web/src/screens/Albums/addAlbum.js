import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import Dropzone from 'react-dropzone';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';

class AddAlbum extends Component {
  state ={
    loading: false, 
    titleEn: '',  
    titleAr: '', 
    year: '', 
    category: '',
    photo: undefined,
    cover: undefined,
    coverUrl: '',
  }
  componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
  }
  
  componentWillReceiveProps(props) {
    if (props.data.getCategories.length) {
      this.setState({ category: props.data.getCategories[0].id });
    }  
  }
  async onSubmit(e) {
    e.preventDefault();
    const { coverUrl, loading, titleEn, titleAr, year, photo, category } = this.state;

    if (!(titleEn.length > 3)) {
      alert('العنوان بلغة الانكليزية يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (!(titleAr.length > 3)) {
      alert('الاسم بلغة العربية لايجب ان يكون فارغ');
      return;
    }
    if (!photo) {
      alert('قم باختيار صورة للابوم رجاءاً');
      return;
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      await this.props.addAlbum({ variables: { cover: coverUrl, titleEn, titleAr, year, photo, category } });
      document.location = '/albums';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.props.data.loading) {
      return (
        <h2>
                      جاري التحميل       
        </h2>
      );
    }
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم الالبوم بالعربي</label>

                <Input
                  onChange={event => this.setState({ titleAr: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم الالبوم بالانكليزي</label>

                <Input
                  onChange={event => this.setState({ titleEn: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>القسم</label>
                <Select
                  value={this.state.category}
                  onChange={(e) => this.setState({ category: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {this.props.data.getCategories.map((c) => (
                    <MenuItem key={c.id} value={c.id}>
                      <em>{c.titleAr}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div>  
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>السنة</label>

                <Input
                  onChange={event => this.setState({ year: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>صورة الالبوم</label>
                <Dropzone
                  accept={['image/*']}
                  multiple={false}
                  onDrop={([file]) => {
                    this.setState({ photo: file });
                  }}
                >
                  {this.state.photo ?
                    <img src={this.state.photo.preview} width='100%' height='100%' />
                    :
                    <h4>
                    اسحب وافلت الصورة هنا ، او اضغط  لاختيار صورة 
                    </h4>
                  } 
                </Dropzone>
              </div>
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>كفر الالبوم</label>
                <Dropzone
                  className='cover'
                  accept={['image/*']}
                  multiple={false}
                  onDrop={async ([file]) => {
                    try {
                      const { data } = await this.props.uploadPhoto({ variables: { photo: file } });
                      this.setState({ cover: file, coverUrl: data.uploadPhoto });
                    } catch (error) {
                      alert('حدث خطا عند الرفع اعد تحديث الصفحه ثم حاول مجددا');
                    }
                  }}
                >
                  <img src={this.state.cover ? this.state.cover.preview : this.state.coverUrl} width='100%' height='100%' />
                </Dropzone>
                <p>يجب ان يكون قياس الكفر 3:2</p>
              </div>
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                    
                  >
               اضافة 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const addAlbum = gql`
  mutation addAlbum($photo:Upload!,$cover:String,$titleAr:String!,$titleEn:String!,$year:String,$category:Int){
    addAlbum(photo:$photo,cover:$cover,titleAr:$titleAr,titleEn:$titleEn,year:$year,category:$category)
  }
`;
const uploadPhoto = gql`
   mutation uploadPhoto($photo:Upload!){
    uploadPhoto(photo:$photo)
   }
`;

export default compose(
  graphql(gql`{getCategories(type:"audios"){id,titleAr,titleEn}}`),
  graphql(uploadPhoto, { name: 'uploadPhoto' }),
  graphql(addAlbum, { name: 'addAlbum' }),
)(AddAlbum);
