import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Paper, Table, TableCell, 
  TableHead, TableRow, TableBody, TableFooter, TablePagination } from '@material-ui/core';

const PER_PAGE = 10;
class ALbums extends Component {
  state ={
    page: 0,
  }
  componentDidMount() {
    console.log(this.props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }
  handleChangePage(event, page) {
    this.setState({ page }, () => {
      console.log(page);
      this.props.data.fetchMore({ 
        variables: {
          limit: PER_PAGE,
          offset: page * PER_PAGE,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          console.log(fetchMoreResult);
          if (!fetchMoreResult) { return previousResult; }
          return Object.assign({}, previousResult, {
            getAlbums: fetchMoreResult.getAlbums,
          });
        }, 
      });
    });
  }
  async deleteAlbum(albumId) {
    if (window.confirm('هل انت متاكد من حذفك للابوم ؟ \n حذفك للابوم سيؤدي لحذف جميع الصوتيات الموجوده بداخله  ')) {
      await this.props.deleteAlbum({
        variables: {
          albumId,
        },
      });
      window.location.reload();
    }
  }

  render() {
    if (this.props.data.loading) {
      return null;
    }
    if (this.props.data.error) {
      return <h4>خطا </h4>;
    }
    console.log(this.props); 
  
    return (
      <div>
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <Paper >
            <Button
              variant="raised" type='submit' color="primary" style={{ marginTop: 30, color: 'white' }}
              href='/albums/add'
              fullWidth
            >
               اضافة البوم جديد
            </Button>
            <Table >
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>صورة الالبوم</TableCell>
                  <TableCell>كفر الالبوم</TableCell>
                  <TableCell>اسم القسم</TableCell>
                  <TableCell>عنوان العربي</TableCell>
                  <TableCell>عنوان الانكليزي</TableCell>
                  <TableCell>عدد الاغاني</TableCell>
                  <TableCell>عدد الاستماعات </TableCell>
                  <TableCell >اجراءات</TableCell>
                </TableRow>
              </TableHead>
              <TableBody >
                {!this.props.data.getAlbums.length && <h4>فارغ</h4> }
                {
                  this.props.data.getAlbums.map((album) => (
                    <TableRow key={album.id}>
                      <TableCell component="th" scope="row">
                        {album.id}
                      </TableCell>
                      <TableCell>
                        <img src={album.photo} width={55} />                          
                      </TableCell>
                      <TableCell>
                        {album.cover ? 
                          <img src={album.cover} width={55} />  
                          :                        
                          <h4>لايوجد</h4>
                        }
                      </TableCell>
                      <TableCell>
                        <h4>{album.category}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{album.titleAr}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{album.titleEn}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{album.songCount || 0}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{album.listenCount || 0}</h4>
                      </TableCell>
                      <TableCell>
                        <div>
                          <Button
                            style={{ marginLeft: 17 }}
                            variant="raised" type='submit' onClick={() => {
                              this.deleteAlbum(album.id);
                            }} color="secondary" fullWidth
                          >
               حذف 
                          </Button> 
                          <Button
                            variant="raised" type='submit' color="primary" fullWidth
                            href={`/albums/${album.id}/edit`}
                          >
               تعديل 
                          </Button> 
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    labelRowsPerPage='عدد الالبومات لكل سجل'
                    colSpan={3}
                    rowsPerPage={PER_PAGE}
                    labelDisplayedRows={({ from, to, count }) => ` ${from}-${to} من عدد الكل ${count}`}
                    count={this.props.data.getAlbumCount}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
            
          </Paper>
        </Grid>
      </div>
          
    );
  }
}

const GET_ALBUMS = gql`
query($limit:Int,$offset:Int){
  getAlbums(limit:$limit, offset:$offset){
    id
    titleAr
    titleEn
    photo
    cover
    songCount
    listenCount
    category
}
  getAlbumCount
}
`;

const deleteAlbum = gql`
  mutation deleteAlbum($albumId:Int!){
    deleteAlbum(albumId:$albumId )
  }
`;

export default compose(
  graphql(GET_ALBUMS, { variables: { limit: PER_PAGE, offset: 0 } }),
  graphql(deleteAlbum, { name: 'deleteAlbum' }),
)(ALbums);
