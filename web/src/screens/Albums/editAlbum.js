import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import Dropzone from 'react-dropzone';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';

class EditAlbum extends Component {
  state ={
    init: true, 
    loading: false,
    titleEn: '', 
    titleAr: '', 
    year: '', 
    category: '', 
    photo: undefined,
    photoUrl: '',
    cover: undefined,
    coverUrl: '',
    albumId: '',
  }
  async componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
    const { id } = this.props.match.params;
    this.setState({ albumId: id });
    const { error, data } = await this.props.getAlbum({
      variables: {
        albumId: id,
      },
    });
    console.log(data);
    const { titleEn, titleAr, year, cover, photo, categoryId } = data.getAlbum;
    this.setState({ titleAr, titleEn, category: categoryId, coverUrl: cover, year, photoUrl: photo, error, init: false });
  }
  async onSubmit(e) {
    e.preventDefault();
    console.log(this.props);
    const { loading, titleEn, coverUrl, titleAr, year, category, photo, albumId, photoUrl } = this.state;

    if (!(titleEn.length > 3)) {
      alert('العنوان بلغة الانكليزية يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (!(titleAr.length > 3)) {
      alert('الاسم بلغة العربية لايجب ان يكون فارغ');
      return;
    }
    if (!photo && !photoUrl) {
      if (!photo) {
        alert('قم باختيار صورة للابوم رجاءاً');
        return;
      }
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      if (photo) {
        await this.props.updateAlbum({ variables: { albumId, category, cover: coverUrl, titleEn, titleAr, year, photo } });
      } else {
        await this.props.updateAlbumString({ variables: { albumId,
          category,
          titleEn,
          titleAr, 
          year,
          cover: coverUrl,
          photo: photoUrl } });
      }
      
      document.location = '/albums';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
      this.setState({ loading: false });
    } 
  }

  render() {
    if (this.props.data.loading || this.state.init) {
      return (
        <h2>
            جاري التحميل       
        </h2>
      );
    }
    const { titleEn, titleAr, year, photo, photoUrl, cover, coverUrl } = this.state;
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم الالبوم بالعربي</label>

                <Input
                  onChange={event => this.setState({ titleAr: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={titleAr}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم الالبوم بالانكليزي</label>

                <Input
                  onChange={event => this.setState({ titleEn: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={titleEn}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>القسم</label>
                <Select
                  value={this.state.category}
                  onChange={(e) => this.setState({ category: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {this.props.data.getCategories.map((c) => (
                    <MenuItem key={c.id} value={c.id}>
                      <em>{c.titleAr}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div>  
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>السنة</label>

                <Input
                  onChange={event => this.setState({ year: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={year}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>صورة الالبوم</label>
                <Dropzone
                  accept={['image/*']}
                  multiple={false}
                  onDrop={([file]) => {
                    this.setState({ photo: file });
                  }}
                >
                  {photo ?
                    <img src={photo.preview} width='100%' height='100%' />
                    :
                    <img src={photoUrl} width='100%' height='100%' />                          
                  } 
                </Dropzone>
              </div>
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>كفر الالبوم</label>
                <Dropzone
                  className='cover'
                  accept={['image/*']}
                  multiple={false}
                  onDrop={async ([file]) => {
                    try {
                      const { data } = await this.props.uploadPhoto({ variables: { photo: file } });
                      this.setState({ cover: file, coverUrl: data.uploadPhoto });
                    } catch (error) {
                      alert('حدث خطا عند الرفع اعد تحديث الصفحه ثم حاول مجددا');
                    }
                  }}
                >
                  {(cover || coverUrl) ?
                    <img src={cover ? cover.preview : coverUrl} width='100%' height='100%' />
                    :
                    <h4>لايوجد كفر </h4>
                  } 
                </Dropzone>
                <p>يجب ان يكون قياس الكفر 3:2</p>
              </div>
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري التعديل ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               تعديل 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
    );
  }
}

const getAlbum = gql`
  mutation getAlbum($albumId:Int!){
    getAlbum(albumId:$albumId){
      id
      titleAr
      titleEn
      year
      photo
      cover
      categoryId
    }
   }
`;
const updateAlbum = gql`
   mutation updateAlbum($albumId:Int!,$photo:Upload!,$cover:String,$titleAr:String!,$titleEn:String!,$year:String,$category:Int){
    updateAlbum(albumId:$albumId,photo:$photo,cover:$cover,titleAr:$titleAr,titleEn:$titleEn,year:$year,category:$category)
   }
`;
const updateAlbumString = gql`
   mutation updateAlbumString($albumId:Int!,$photo:String!,$cover:String,$titleAr:String!,$titleEn:String!,$year:String,$category:Int){
    updateAlbumString(albumId:$albumId,photo:$photo,cover:$cover,titleAr:$titleAr,titleEn:$titleEn,year:$year,category:$category)
   }
`;
const uploadPhoto = gql`
   mutation uploadPhoto($photo:Upload!){
    uploadPhoto(photo:$photo)
   }
`;

export default compose(
  graphql(gql`{getCategories(type:"audios"){id,titleAr,titleEn}}`),
  graphql(getAlbum, { name: 'getAlbum' }),
  graphql(uploadPhoto, { name: 'uploadPhoto' }),
  graphql(updateAlbum, { name: 'updateAlbum' }),
  graphql(updateAlbumString, { name: 'updateAlbumString' }),
)(EditAlbum);
