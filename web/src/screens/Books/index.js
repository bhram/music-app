import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Paper, Table, TableCell, 
  TableHead, TableRow, TableBody, TableFooter, TablePagination } from '@material-ui/core';

const PER_PAGE = 10;
class Books extends Component {
  state ={
    page: 0,
  }
  componentDidMount() {
    console.log(this.props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }
  handleChangePage(event, page) {
    this.setState({ page }, () => {
      this.props.data.fetchMore({ 
        variables: {
          limit: PER_PAGE,
          offset: page * PER_PAGE,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          console.log(fetchMoreResult);
          if (!fetchMoreResult) { return previousResult; }
          return Object.assign({}, previousResult, {
            getBooks: fetchMoreResult.getBooks,
          });
        }, 
      });
    });
  }
  async deleteBook(bookId) {
    if (window.confirm('هل انت متاكد من الحذف ؟')) {
      await this.props.deleteBook({
        variables: {
          bookId,
        },
      });
      window.location.reload();
    }
  }

  render() {
    if (this.props.data.loading) {
      return null;
    }
    if (this.props.data.error) {
      return <h4>خطا </h4>;
    }
    console.log(this.props); 
  
    return (
      <div>
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <Paper >
            <Button
              variant="raised" type='submit' color="primary" style={{ marginTop: 30, color: 'white' }}
              href='/books/add'
              fullWidth
            >
               اضافة كتاب جديد
            </Button>
            <Table >
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>صورة الكتاب</TableCell>
                  <TableCell>العنوان </TableCell>
                  <TableCell>عدد التحميلات </TableCell>
                  <TableCell >اجراءات</TableCell>
                </TableRow>
              </TableHead>
              <TableBody >
                {!this.props.data.getBooks.length && <h4>فارغ</h4> }
                {
                  this.props.data.getBooks.map((book) => (
                    <TableRow key={book.id}>
                      <TableCell component="th" scope="row">
                        {book.id}
                      </TableCell>
                      <TableCell>
                        {book.photo ? <img src={book.photo} width={55} /> : <h4>لاتوجد صورة</h4>}
                      </TableCell>
                      <TableCell>
                        <a href={book.url} target='_blank'>
                          <h4 style={{ color: 'blue' }}>{book.title}</h4>
                        </a>
                      </TableCell>
                      <TableCell>
                        <h4>{book.downloadCount}</h4>
                      </TableCell>
                      <TableCell>
                        <div>
                          <Button
                            style={{ marginLeft: 17 }}
                            variant="raised" type='submit' onClick={() => {
                              this.deleteBook(book.id);
                            }} color="secondary" fullWidth
                          >
               حذف 
                          </Button> 
                          <Button
                            variant="raised" type='submit' 
                            color="primary" fullWidth
                            href={`/books/${book.id}/edit`}
                          >
               تعديل 
                          </Button> 
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    labelRowsPerPage='عدد لكل سجل'
                    colSpan={3}
                    rowsPerPage={PER_PAGE}
                    labelDisplayedRows={({ from, to, count }) => ` ${from}-${to} من عدد الكل ${count}`}
                    count={this.props.data.getBooksCount}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
            
          </Paper>
        </Grid>
      </div>
          
    );
  }
}

const GET_BOOKS = gql`
query($limit:Int = 15,$offset:Int = 0){
  getBooks(limit:$limit, offset:$offset){
    id
    title
    photo
    downloadCount
    url
}
  getBooksCount
}
`;

const deleteBook = gql`
  mutation deleteBook($bookId:Int!){
    deleteBook(bookId:$bookId )
  }
`;
export default compose(
  graphql(GET_BOOKS),
  graphql(deleteBook, { name: 'deleteBook' }),
)(Books);
