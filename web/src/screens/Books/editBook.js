import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Input } from '@material-ui/core';

class EditBook extends Component {
  state ={
    init: true,
    bookId: '',
    title: '', 
  }
  async componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
    const { id } = this.props.match.params;
    this.setState({ bookId: id });
    const { data } = await this.props.getBook({ variables: { bookId: id } });
    const { title, url } = data.getBook;
    this.setState({ title, url, init: false });
  }
  
  async onSubmit(e) {
    e.preventDefault();
    const { loading, title, bookId } = this.state;

    if (!(title.length > 3)) {
      alert('العنوان  يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      await this.props.updateBook({ variables: { bookId, title } });
      document.location = '/books';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.state.init) {
      return (
        <h2>
            جاري التحميل       
        </h2>
      );
    }
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>العنوان </label>

                <Input
                  onChange={event => this.setState({ title: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={this.state.title}
                />        
              </div>   
             
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               تعديل 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const GET_BOOK = gql`
    mutation getBook($bookId:Int!){
        getBook(bookId:$bookId){
            id
            title
        }
    }
`;
const updateBook = gql`
  mutation updateBook($bookId:Int!,$title:String!){
    updateBook(bookId:$bookId,title:$title)
  }
`;
export default compose(
  graphql(GET_BOOK, { name: 'getBook' }),
  graphql(updateBook, { name: 'updateBook' }),
)(EditBook);
