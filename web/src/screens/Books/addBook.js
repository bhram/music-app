import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import Dropzone from 'react-dropzone';
import gql from 'graphql-tag';
import { Grid, Input } from '@material-ui/core';

class AddBook extends Component {
  state ={
    loading: false, 
    title: '', 
    book: undefined, 
    photo: undefined,
  }
  componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(e) {
    e.preventDefault();
    const { loading, title, book, photo } = this.state;
    console.log(book);
   
    if (!(title.length > 3)) {
      alert('العنوان يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (!book) {
      alert('قم باختيار  كتاب رجاءاً');
      return;
    }
    if (!photo) {
      alert('قم باختيار صورة للكتاب رجاءاً');
      return;
    }
    if (book.type !== 'application/pdf') {
      alert('يجب ان تكون صيغة الكتاب PDFً');
      return;
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      const { data } = await this.props.uploadPhotoBook({ variables: { photo } });
      await this.props.addBook({ variables: { title, book, photo: data.uploadPhotoBook } });
      document.location = '/books';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.props.data.loading) {
      return (
        <h2>
                      جاري التحميل       
        </h2>
      );
    }
    console.log(this.props.data);
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>عنوان الكتاب</label>

                <Input
                  onChange={event => this.setState({ title: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>صورة الكتاب</label>
                <br />
                <Dropzone
                  accept={['image/*']}
                  multiple={false}
                  onDrop={([file]) => {
                    this.setState({ photo: file });
                  }}
                >
                  {this.state.photo ?
                    <img src={this.state.photo.preview} width='100%' height='100%' />
                    :
                    <h4>
                    اسحب وافلت الصورة هنا ، او اضغط  لاختيار صورة 
                    </h4>
                  } 
                </Dropzone>
              </div>  
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>ارفع الكتاب</label>
                <br />
                <Input
                  type="file" accept="application/pdf" onChange={(e) =>
                    this.setState({ book: e.target.files[0] })}
                />
              </div>  
             
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               اضافة 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const addBook = gql`
  mutation addBook($book:Upload!,$photo:String!,$title:String!){
    addBook(book:$book,photo:$photo,title:$title)
  }
`;
const uploadPhotoBook = gql`
  mutation uploadPhotoBook($photo:Upload!){
    uploadPhotoBook(photo:$photo)
  }
`;
export default compose(
  graphql(gql`{getAlbums{id,titleAr}}`),
  graphql(addBook, { name: 'addBook' }),
  graphql(uploadPhotoBook, { name: 'uploadPhotoBook' }),
)(AddBook);
