import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Paper, Table, TableCell, 
  TableHead, TableRow, TableBody, TableFooter, TablePagination } from '@material-ui/core';

const PER_PAGE = 10;
class Songs extends Component {
  state ={
    page: 0,
  }
  componentDidMount() {
    console.log(this.props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }
  handleChangePage(event, page) {
    this.setState({ page }, () => {
      this.props.data.fetchMore({ 
        variables: {
          limit: PER_PAGE,
          offset: page * PER_PAGE,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          console.log(fetchMoreResult);
          if (!fetchMoreResult) { return previousResult; }
          return Object.assign({}, previousResult, {
            getSongs: fetchMoreResult.getSongs,
          });
        }, 
      });
    });
  }
  async deleteSong(songId) {
    if (window.confirm('هل انت متاكد من الحذف ؟')) {
      await this.props.deleteSong({
        variables: {
          songId,
        },
      });
      window.location.reload();
    }
  }

  render() {
    if (this.props.data.loading) {
      return null;
    }
    if (this.props.data.error) {
      return <h4>خطا </h4>;
    }
    console.log(this.props); 
  
    return (
      <div>
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <Paper >
            <Button
              variant="raised" type='submit' color="primary" style={{ marginTop: 30, color: 'white' }}
              href='/songs/add'
              fullWidth
            >
               اضافة مقطع صوتي جديد
            </Button>
            <Table >
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>صورة الالبوم</TableCell>
                  <TableCell>اسم الالبوم</TableCell>
                  <TableCell>عنوان العربي</TableCell>
                  <TableCell>عنوان الانكليزي</TableCell>
                  <TableCell>عدد المعجبين</TableCell>
                  <TableCell>عدد الاستماعات </TableCell>
                  <TableCell >اجراءات</TableCell>
                </TableRow>
              </TableHead>
              <TableBody >
                {!this.props.data.getSongs.length && <h4>فارغ</h4> }
                {
                  this.props.data.getSongs.map((song) => (
                    <TableRow key={song.id}>
                      <TableCell component="th" scope="row">
                        {song.id}
                      </TableCell>
                      <TableCell>
                        <img src={song.photo} width={55} />                          
                      </TableCell>
                      <TableCell>
                        <h4>{song.album}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{song.titleAr}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{song.titleEn}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{song.favsCount}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{song.listenCount}</h4>
                      </TableCell>
                      <TableCell>
                        <div>
                          <Button
                            style={{ marginLeft: 17 }}
                            variant="raised" type='submit' onClick={() => {
                              this.deleteSong(song.id);
                            }} color="secondary" fullWidth
                          >
               حذف 
                          </Button> 
                          <Button
                            variant="raised" type='submit' 
                            color="primary" fullWidth
                            href={`/songs/${song.id}/edit`}
                          >
               تعديل 
                          </Button> 
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    labelRowsPerPage='عدد لكل سجل'
                    colSpan={3}
                    rowsPerPage={PER_PAGE}
                    labelDisplayedRows={({ from, to, count }) => ` ${from}-${to} من عدد الكل ${count}`}
                    count={this.props.data.getSongsCount}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
           
          </Paper>
        </Grid>
      </div>
          
    );
  }
}

const GET_SONGS = gql`
query($limit:Int = 15,$offset:Int = 0){
  getSongs(limit:$limit, offset:$offset){
    id
    titleAr
    titleEn
    url
    photo
    duration
    albumId
    listenCount
    favsCount
    album
}
  getSongsCount
}
`;

const deleteSong = gql`
  mutation deleteSong($songId:Int!){
    deleteSong(songId:$songId )
  }
`;
export default compose(
  graphql(GET_SONGS),
  graphql(deleteSong, { name: 'deleteSong' }),
)(Songs);
