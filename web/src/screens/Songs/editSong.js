import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';

class EditSong extends Component {
  state ={
    init: true,
    songId: '',
    loading: false, 
    titleEn: '', 
    titleAr: '', 
    albumId: '', 
    url: '',
    audio: undefined,
  }
  async componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
    const { id } = this.props.match.params;
    this.setState({ songId: id });
    const { data } = await this.props.getSong({ variables: { songId: id } });
    const { titleAr, titleEn, url, albumId } = data.getSong;
    console.log(albumId);
    this.setState({ titleAr, titleEn, url, albumId, init: false });
  }
  
  async onSubmit(e) {
    e.preventDefault();
    const { loading, titleEn, titleAr, albumId, songId } = this.state;

    if (!(titleEn.length > 3)) {
      alert('العنوان بلغة الانكليزية يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (!(titleAr.length > 3)) {
      alert('الاسم بلغة العربية لايجب ان يكون فارغ');
      return;
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      await this.props.updateSong({ variables: { songId, titleEn, titleAr, albumId } });
      document.location = '/songs';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.props.data.loading || this.state.init) {
      return (
        <h2>
            جاري التحميل       
        </h2>
      );
    }
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم المقطع بالعربي</label>

                <Input
                  onChange={event => this.setState({ titleAr: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={this.state.titleAr}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم المقطع بالانكليزي</label>

                <Input
                  onChange={event => this.setState({ titleEn: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={this.state.titleEn}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>الالبوم</label>
                <Select
                  value={this.state.albumId}
                  onChange={(e) => this.setState({ albumId: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {this.props.data.getAlbums.map((album) => (
                    <MenuItem key={album.id} value={album.id}>
                      <em>{album.titleAr}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div>  

              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>المقطع</label>
                <br />
                <audio controls >
                  <source src={this.state.url} type="audio/mpeg" />
                </audio>
              </div>  
             
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               تعديل 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const GET_SONG = gql`
    mutation getSong($songId:Int!){
        getSong(songId:$songId){
            id
            titleAr
            titleEn
            url
            albumId
        }
    }
`;
const updateSong = gql`
  mutation updateSong($songId:Int!,$titleAr:String!,$titleEn:String!,$albumId:Int!){
    updateSong(songId:$songId,titleAr:$titleAr,titleEn:$titleEn,albumId:$albumId)
  }
`;
export default compose(
  graphql(gql`{getAlbums{id,titleAr}}`),
  graphql(GET_SONG, { name: 'getSong' }),
  graphql(updateSong, { name: 'updateSong' }),
)(EditSong);
