import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';

class AddSong extends Component {
  state ={
    loading: false, 
    titleEn: '', 
    titleAr: '', 
    albumId: '', 
    audio: undefined,
  }
  componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillReceiveProps(props) {
    if (props.data.getAlbums.length) {
      this.setState({ albumId: props.data.getAlbums[0].id });
    }  
  }
  async onSubmit(e) {
    e.preventDefault();
    const { loading, titleEn, titleAr, albumId, audio } = this.state;

    if (!(titleEn.length > 3)) {
      alert('العنوان بلغة الانكليزية يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (!(titleAr.length > 3)) {
      alert('الاسم بلغة العربية لايجب ان يكون فارغ');
      return;
    }
    if (!audio) {
      alert('قم باختيار مقطع صوتي رجاءاً');
      return;
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      await this.props.addSong({ variables: { titleEn, titleAr, audio, albumId } });
      document.location = '/songs';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.props.data.loading) {
      return (
        <h2>
                      جاري التحميل       
        </h2>
      );
    }
    console.log(this.props.data);
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم المقطع بالعربي</label>

                <Input
                  onChange={event => this.setState({ titleAr: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم المقطع بالانكليزي</label>

                <Input
                  onChange={event => this.setState({ titleEn: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>الالبوم</label>
                <Select
                  value={this.state.albumId}
                  onChange={(e) => this.setState({ albumId: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {this.props.data.getAlbums.map((album) => (
                    <MenuItem key={album.id} value={album.id}>
                      <em>{album.titleAr}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div>  

              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>ارفع المقطع</label>
                <br />
                <Input
                  type="file" accept=".mp3,audio/*" onChange={(e) =>
                    this.setState({ audio: e.target.files[0] })}
                />
              </div>  
             
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               اضافة 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const addSong = gql`
  mutation addSong($audio:Upload!,$titleAr:String!,$titleEn:String!,$albumId:Int!){
    addSong(audio:$audio,titleAr:$titleAr,titleEn:$titleEn,albumId:$albumId)
  }
`;
export default compose(
  graphql(gql`{getAlbums{id,titleAr}}`),
  graphql(addSong, { name: 'addSong' }),
)(AddSong);
