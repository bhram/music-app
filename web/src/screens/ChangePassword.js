import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Input } from '@material-ui/core';
import 'react-datepicker/dist/react-datepicker.css';

class ChangePassword extends Component {
  state ={
    loading: false, 
    oldPassword: '',
    newPassword: '', 
    reNewPassword: '',
  }
  componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
  }
  
  async onSubmit(e) {
    e.preventDefault();
    const { loading, oldPassword, newPassword, reNewPassword } = this.state;
  
    if (!(oldPassword.length > 5)) {
      alert('كلمة السر القديمة يجب ان تكون اكبر من 6 احرف');
      return;
    }
    if (!(oldPassword.length > 5)) {
      alert('كلمة السر الجديدة يجب ان كون اكبر من 6 احرف');
      return;
    }
    if (newPassword !== reNewPassword) {
      alert('كلمة السر الجديده غير متطابقة');
      return;
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      await this.props.updatePassword({ variables: { newPassword, oldPassword } });
      document.location = '/';
    } catch (error) {
      if (/password/.test(error.message)) {
        alert('كلمة السر القديمة غير صحيحة');
      }
    } 
    this.setState({ loading: false });
  }

  render() {
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>كلمة السر القديمة </label>

                <Input
                  onChange={event => this.setState({ oldPassword: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  type='password'
                />        
              </div>    
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>كلمة السر الجديدة </label>

                <Input
                  onChange={event => this.setState({ newPassword: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  type='password'
                />        
              </div>    
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اعد كتابة كلمة السر الجديدة </label>

                <Input
                  onChange={event => this.setState({ reNewPassword: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  type='password'
                />        
              </div>    

              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الحفظ ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               حفظ 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const updatePassword = gql`
  mutation updatePassword($oldPassword:String!,$newPassword:String!){
    updatePassword(oldPassword:$oldPassword,newPassword:$newPassword)
  }
`;
export default compose(
  graphql(updatePassword, { name: 'updatePassword' }),
)(ChangePassword);
