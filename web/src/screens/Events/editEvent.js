import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import gql from 'graphql-tag';
import { Grid, Input } from '@material-ui/core';
import 'react-datepicker/dist/react-datepicker.css';

class EditEvent extends Component {
  state ={
    init: true,
    loading: false, 
    eventId: '',
    title: '',
    date: moment(), 
    dateEnd: null, 
  }
  async componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
    const { id } = this.props.match.params;
    this.setState({ eventId: id });
    const { data } = await this.props.getEvent({ variables: { eventId: id } });
    const { title, date, dateEnd } = data.getEvent;
    console.log(date);
    console.log(moment(date));
    this.setState({ title, date: moment(date), init: false, dateEnd: dateEnd ? moment(dateEnd) : null });
  }
  
  async onSubmit(e) {
    e.preventDefault();
    const { loading, title, dateEnd, date, eventId } = this.state;
    if (!(title.length > 3)) {
      alert('يجب ان يكون عنوان المناسبة اكبر من 3 احرف ');
      return;
    }
    if (!date) {
      alert('قم بكتابة التاريخ رجاءءً');
      return;
    }
    if (loading) {
      return;
    }
 
    this.setState({ loading: true });
  
    try {
      await this.props.updateEvent({ variables: { eventId,
        title,
        dateEnd: dateEnd ? dateEnd.format('MM/DD/YYYY') : null, 
        date: date.format('MM/DD/YYYY') } });
      document.location = '/events';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.state.init) {
      return (
        <h2>
            جاري التحميل       
        </h2>
      );
    }
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>عنوان او وصف للمناسبة </label>

                <Input
                  onChange={event => this.setState({ title: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={this.state.title}
                  multiline
                />        
              </div>   
             
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>التاريخ</label>

                <DatePicker
                  selected={this.state.date}
                  onChange={(date) => this.setState({ date })}
                />
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>تاريخ انتهاء المناسبة </label>

                <DatePicker
                  selected={this.state.dateEnd}
                  onChange={(dateEnd) => this.setState({ dateEnd })}
                />
              </div>   

              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري التعديل ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               تعديل 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}
const GET_EVENT = gql`
    mutation getEvent($eventId:Int!){
        getEvent(eventId:$eventId){
            id
            title
            date
            dateEnd
        }
    }
`;
const updateEvent = gql`
  mutation updateEvent($eventId:Int!,$title:String!,$date:String!,,$dateEnd:String){
    updateEvent(eventId:$eventId,title:$title,date:$date,dateEnd:$dateEnd)
  }
`;
export default compose(
  graphql(updateEvent, { name: 'updateEvent' }),
  graphql(GET_EVENT, { name: 'getEvent' }),
)(EditEvent);
