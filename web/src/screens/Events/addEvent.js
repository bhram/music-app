import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Dropzone from 'react-dropzone';
import gql from 'graphql-tag';
import { Grid, Input } from '@material-ui/core';
import 'react-datepicker/dist/react-datepicker.css';

class AddEvent extends Component {
  state ={
    loading: false, 
    title: '',
    photo: undefined,
    date: moment(), 
    dateEnd: null,
  }
  componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
  }
  
  async onSubmit(e) {
    e.preventDefault();
    const { loading, title, dateEnd, date, photo } = this.state;
    if (!(title.length > 3)) {
      alert('يجب ان يكون عنوان المناسبة اكبر من 3 احرف ');
      return;
    }
    if (!date) {
      alert('قم بكتابة التاريخ رجاءءً');
      return;
    }
    if (loading) {
      return;
    }
 
    this.setState({ loading: true });
  
    try {
      await this.props.addEvent({ variables: {
        title, 
        dateEnd: dateEnd ? dateEnd.format('MM/DD/YYYY') : null, 
        date:
         date.format('MM/DD/YYYY'),
        photo } });
      document.location = '/events';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>عنوان او وصف للمناسبة </label>

                <Input
                  onChange={event => this.setState({ title: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  multiline
                />        
              </div>   
             
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>التاريخ</label>

                <DatePicker
                  selected={this.state.date}
                  onChange={(date) => this.setState({ date })}
                />
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>تاريخ انتهاء المناسبة ( اختياري ) </label>

                <DatePicker
                  selected={this.state.dateEnd}
                  onChange={(dateEnd) => this.setState({ dateEnd })}
                />
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>صورة الالبوم</label>
                <Dropzone
                  accept={['image/*']}
                  multiple={false}
                  onDrop={([file]) => {
                    this.setState({ photo: file });
                  }}
                >
                  {this.state.photo ?
                    <img src={this.state.photo.preview} width='100%' height='100%' />
                    :
                    <h4>
                    اسحب وافلت الصورة هنا ، او اضغط  لاختيار صورة 
                    </h4>
                  } 
                </Dropzone>
              </div>
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               اضافة 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const addEvent = gql`
  mutation addEvent($title:String!,$date:String!,$dateEnd:String,$photo:Upload){
    addEvent(title:$title,date:$date,dateEnd:$dateEnd,photo:$photo)
  }
`;
export default compose(
  graphql(addEvent, { name: 'addEvent' }),
)(AddEvent);
