import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Paper, Table, TableCell, 
  TableHead, TableRow, TableBody, TableFooter, TablePagination } from '@material-ui/core';

const PER_PAGE = 10;
class Events extends Component {
  state ={
    page: 0,
  }
  componentDidMount() {
    this.handleChangePage = this.handleChangePage.bind(this);
  }
  handleChangePage(event, page) {
    this.setState({ page }, () => {
      console.log(page);
      this.props.data.fetchMore({ 
        variables: {
          limit: PER_PAGE,
          offset: page * PER_PAGE,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          console.log(fetchMoreResult);
          if (!fetchMoreResult) { return previousResult; }
          return Object.assign({}, previousResult, {
            getEvents: fetchMoreResult.getEvents,
          });
        }, 
      });
    });
  }
  async deleteEvent(eventId) {
    if (window.confirm('هل انت متاكد من حذفك لهذه المناسبة ؟')) {
      await this.props.deleteEvent({
        variables: {
          eventId,
        },
      });
      window.location.reload();
    }
  }

  render() {
    if (this.props.data.loading) {
      return null;
    }
    if (this.props.data.error) {
      return <h4>خطا </h4>;
    }
    console.log(this.props); 
  
    return (
      <div>
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <Paper >
            <Button
              variant="raised" type='submit' color="primary" style={{ marginTop: 30, color: 'white' }}
              href='/events/add'
              fullWidth
            >
               اضافة مناسبة جديدة
            </Button>
            <Table >
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>صورة المناسبة</TableCell>
                  <TableCell>عنوان المناسبة</TableCell>
                  <TableCell>التاريخ </TableCell>
                  <TableCell>تاريخ انتهاء المناسبة  </TableCell>
                  <TableCell >اجراءات</TableCell>
                </TableRow>
              </TableHead>
              <TableBody >
                {!this.props.data.getEvents.length && <h4>فارغ</h4> }
                {
                  this.props.data.getEvents.map((event) => (
                    <TableRow key={event.id}>
                      <TableCell component="th" scope="row">
                        {event.id}
                      </TableCell>
                      <TableCell>
                        {event.photo ? <img src={event.photo} width={55} /> : <h4>لاتوجد صورة</h4>}                          
                      </TableCell>
                      <TableCell>
                        <h4>{event.title}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{event.date}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{event.dateEnd}</h4>
                      </TableCell>
                    
                      <TableCell>
                        <div>
                          <Button
                            style={{ marginLeft: 17 }}
                            variant="raised" type='submit' onClick={() => {
                              this.deleteEvent(event.id);
                            }} color="secondary" fullWidth
                          >
               حذف 
                          </Button> 
                          <Button
                            variant="raised" type='submit' color="primary" fullWidth
                            href={`/events/${event.id}/edit`}
                          >
               تعديل 
                          </Button> 
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    labelRowsPerPage='عدد المناسبات لكل سجل'
                    colSpan={3}
                    rowsPerPage={PER_PAGE}
                    labelDisplayedRows={({ from, to, count }) => ` ${from}-${to} من عدد الكل ${count}`}
                    count={this.props.data.getEventsCount}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
           
          </Paper>
        </Grid>
      </div>
          
    );
  }
}

const GET_EVENTS = gql`
query($limit:Int,$offset:Int){
  getEvents(limit:$limit, offset:$offset){
    id
    title
    date
    dateEnd
    photo
}
  getEventsCount
}
`;

const deleteEvent = gql`
  mutation deleteEvent($eventId:Int!){
    deleteEvent(eventId:$eventId )
  }
`;

export default compose(
  graphql(GET_EVENTS, { variables: { limit: PER_PAGE, offset: 0 } }),
  graphql(deleteEvent, { name: 'deleteEvent' }),
)(Events);
