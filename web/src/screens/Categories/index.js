import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Paper, Table, TableCell, 
  TableHead, TableRow, TableBody } from '@material-ui/core';

class Category extends Component {
  async deleteCategory(categoryId) {
    if (window.confirm('هل انت متاكد من حذفك لهذه القسم ؟  ')) {
      await this.props.deleteCategory({
        variables: {
          categoryId,
        },
      });
      window.location.reload();
    }
  }

  render() {
    if (this.props.data.loading) {
      return null;
    }
    if (this.props.data.error) {
      return <h4>خطا </h4>;
    }
    console.log(this.props); 
  
    return (
      <div>
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <Paper >
            <Button
              variant="raised" type='submit' color="primary" style={{ marginTop: 30, color: 'white' }}
              href='/categories/add'
              fullWidth
            >
               اضافة قسم جديد
            </Button>
            <Table >
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>صورة القسم</TableCell>
                  <TableCell>العنوان بالانكليزي</TableCell>
                  <TableCell>العنوان بالعربي</TableCell>
                  <TableCell>عدد الالبومات</TableCell>
                  <TableCell>النوع</TableCell>
                  <TableCell >اجراءات</TableCell>
                </TableRow>
              </TableHead>
              <TableBody >
                {!this.props.data.getCategories.length && <h4>فارغ</h4> }
                {
                  this.props.data.getCategories.map((c) => (
                    <TableRow key={c.id}>
                      <TableCell component="th" scope="row">
                        {c.id}
                      </TableCell>
                      <TableCell>
                        <img src={c.photo} width={55} />                          
                      </TableCell>
                      <TableCell>
                        <h4>{c.titleEn}</h4>                         
                      </TableCell>
                      <TableCell>
                        <h4>{c.titleAr}</h4>                         
                      </TableCell>
                      <TableCell>
                        <h4>{c.type === 'audios' ? 'صوتيات' : 'للفيديوهات'}</h4>                         
                      </TableCell>
                      <TableCell>
                        <h4>{c.albumsCount}</h4>                         
                      </TableCell>
                      <TableCell>
                        <div>
                          <Button
                            style={{ marginLeft: 17 }}
                            variant="raised" type='submit' onClick={() => {
                              this.deleteCategory(c.id);
                            }} color="secondary" fullWidth
                          >
               حذف 
                          </Button> 
                          <Button
                            variant="raised" type='submit' color="primary" fullWidth
                            href={`/categories/${c.id}/edit`}
                          >
               تعديل 
                          </Button> 
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
            </Table>
            
          </Paper>
        </Grid>
      </div>
          
    );
  }
}

const GET_CATEGORY = gql`
{
  getCategories{
    id
    titleAr
    titleEn
    photo
    type
    albumsCount
}
}
`;

const deleteCategory = gql`
  mutation deleteCategory($categoryId:Int!){
    deleteCategory(categoryId:$categoryId )
  }
`;

export default compose(
  graphql(GET_CATEGORY),
  graphql(deleteCategory, { name: 'deleteCategory' }),
)(Category);
