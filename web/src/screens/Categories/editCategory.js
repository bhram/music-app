import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import Dropzone from 'react-dropzone';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';

export const types = [{ val: 'audios', title: 'للصوتيات' }, { val: 'videos', title: 'للفيديوهات' }];
class EditCategory extends Component {
  state ={
    loading: true, 
    titleEn: '', 
    titleAr: '', 
    type: '',
    photo: undefined,
    photoUrl: '',
    category: '',
  }
  async componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
    const { id } = this.props.match.params;
    this.setState({ categoryId: id });
    const { error, data } = await this.props.getCategory({
      variables: {
        categoryId: id,
      },
    });
    console.log(error);
    const { titleEn, titleAr, photo, type } = data.getCategory;
    this.setState({ titleAr, type, titleEn, photoUrl: photo, error, loading: false });
  }
  async onSubmit(e) {
    e.preventDefault();
    console.log(this.props);
    const { type, loading, titleEn, titleAr, photo, categoryId, photoUrl } = this.state;

    if (!(titleEn.length > 3)) {
      alert('العنوان بلغة الانكليزية يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (!(titleAr.length > 3)) {
      alert('الاسم بلغة العربية لايجب ان يكون فارغ');
      return;
    }
    if (!photo && !photoUrl) {
      if (!photo) {
        alert('قم باختيار صورة للابوم رجاءاً');
        return;
      }
    }
    if (loading) {
      return;
    }
    this.setState({ loadingAdd: true });
  
    try {
      if (photo) {
        await this.props.updateCategory({ variables: { type, categoryId, titleEn, titleAr, photo } });
      } else {
        await this.props.updateCategoryString({ variables: { type, categoryId, titleEn, titleAr, photo: photoUrl } });
      }
      
      document.location = '/categories';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
      this.setState({ loadingAdd: false });
    } 
  }

  render() {
    const { loading, error } = this.state;
    if (loading) {
      return (
        <h2>
                جاري التحميل       
        </h2>
      );
    }
    if (error) {
      return (
        <h2>
                هذه الالبوم غير موجود
        </h2>
      );
    }
    const { titleEn, titleAr, photo, photoUrl } = this.state;
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم الالبوم بالعربي</label>

                <Input
                  onChange={event => this.setState({ titleAr: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={titleAr}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم الالبوم بالانكليزي</label>

                <Input
                  onChange={event => this.setState({ titleEn: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  defaultValue={titleEn}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>النوع</label>
                <Select
                  value={this.state.type}
                  onChange={(e) => this.setState({ type: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {types.map((s) => (
                    <MenuItem key={s.val} value={s.val}>
                      <em>{s.title}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div>  
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>صورة الالبوم</label>
                <Dropzone
                  accept={['image/*']}
                  multiple={false}
                  onDrop={([file]) => {
                    this.setState({ photo: file });
                  }}
                >
                  {photo ?
                    <img src={photo.preview} width='100%' height='100%' />
                    :
                    <img src={photoUrl} width='100%' height='100%' />                          
                  } 
                </Dropzone>
              </div>
              {
                this.state.loadingAdd ? <h4 style={{ textAlign: 'center' }}>جاري التعديل ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               تعديل 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
    );
  }
}

const getCategory = gql`
  mutation getCategory($categoryId:Int!){
    getCategory(categoryId:$categoryId){
      id
      titleAr
      titleEn
      photo
      type
    }
   }
`;
const updateCategory = gql`
   mutation updateCategory($categoryId:Int!,$type:String!,$photo:Upload!,$titleAr:String!,$titleEn:String!){
    updateCategory(categoryId:$categoryId,type:$type,photo:$photo,titleAr:$titleAr,titleEn:$titleEn)
   }
`;
const updateCategoryString = gql`
   mutation updateCategoryString($categoryId:Int!,$type:String!,$photo:String!,$titleAr:String!,$titleEn:String!){
    updateCategoryString(categoryId:$categoryId,type:$type,photo:$photo,titleAr:$titleAr,titleEn:$titleEn)
   }
`;

export default compose(
  graphql(getCategory, { name: 'getCategory' }),
  graphql(updateCategory, { name: 'updateCategory' }),
  graphql(updateCategoryString, { name: 'updateCategoryString' }),
)(EditCategory);
