import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import Dropzone from 'react-dropzone';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';
import { types } from './editCategory';

class AddCategory extends Component {
  state ={
    type: types[0].val,
    loading: false, 
    titleEn: '', 
    titleAr: '', 
    categoryId: '',
    photo: undefined,
  }
  componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
  }
  
  async onSubmit(e) {
    e.preventDefault();
    const { type, loading, titleEn, titleAr, photo, categoryId } = this.state;

    if (!(titleEn.length > 3)) {
      alert('العنوان بلغة الانكليزية يجب ان يكون اكبر من 3 احرف ');
      return;
    }
    if (!(titleAr.length > 3)) {
      alert('الاسم بلغة العربية لايجب ان يكون فارغ');
      return;
    }
    if (!photo) {
      alert('قم باختيار صورة للابوم رجاءاً');
      return;
    }
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      await this.props.addCategory({ variables: { type, titleEn, titleAr, photo, categoryId } });
      document.location = '/categories';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم القسم بالعربي</label>

                <Input
                  onChange={event => this.setState({ titleAr: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                  
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>اسم القسم بالانكليزي</label>

                <Input
                  onChange={event => this.setState({ titleEn: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder=""
                  fullWidth
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>النوع</label>
                <Select
                  value={this.state.type}
                  onChange={(e) => this.setState({ type: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {types.map((s) => (
                    <MenuItem key={s.val} value={s.val}>
                      <em>{s.title}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div> 
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>صورة القسم</label>
                <Dropzone
                  accept={['image/*']}
                  multiple={false}
                  onDrop={([file]) => {
                    this.setState({ photo: file });
                  }}
                >
                  {this.state.photo ?
                    <img src={this.state.photo.preview} width='100%' height='100%' />
                    :
                    <h4>
                    اسحب وافلت الصورة هنا ، او اضغط  لاختيار صورة 
                    </h4>
                  } 
                </Dropzone>
              </div>
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               اضافة 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const addCategory = gql`
  mutation addCategory($photo:Upload!,$type:String!,$titleAr:String!,$titleEn:String!){
    addCategory(photo:$photo,type:$type,titleAr:$titleAr,titleEn:$titleEn)
  }
`;
export default compose(
  graphql(addCategory, { name: 'addCategory' }),
)(AddCategory);
