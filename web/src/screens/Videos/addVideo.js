import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';

class AddVideo extends Component {
  state ={
    loading: false, 
    videoUrl: undefined,
    category: null,
  }
  componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(e) {
    e.preventDefault();
    const { category, loading, videoUrl, year } = this.state;
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\\&v=|\?v=)([^#\\&\\?]*).*/;

    if (!videoUrl) {
      alert(' ادخل رابط الفيديو اولا');
      return;
    }
    if (!(videoUrl.match(regExp))) {
      alert(' تاكد من رابط الفيديو');
      return;
    }
   
    if (loading) {
      return;
    }
    this.setState({ loading: true });
    const cate = category || this.props.data.getCategories[0].id;
    try {
      await this.props.addVideo({ variables: { category: cate, videoUrl, year } });
      document.location = '/videos';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.props.data.loading) {
      return (
        <h2>
              جاري التحميل       
        </h2>
      );
    }
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>رابط المقطع</label>

                <Input
                  onChange={event => this.setState({ videoUrl: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder="ضع هنا رابط المقطع من اليوتوب"
                  fullWidth
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>السنه</label>

                <Input
                  onChange={event => this.setState({ year: event.target.value })}
                  style={{ marginTop: 20 }}            
                  fullWidth
                />        
              </div>  
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>القسم</label>
                <Select
                  value={this.state.category || this.props.data.getCategories[0].id}
                  onChange={(e) => this.setState({ category: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {this.props.data.getCategories.map((c) => (
                    <MenuItem key={c.id} value={c.id}>
                      <em>{c.titleAr}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div>   
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               اضافة 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}

const addVideo = gql`
  mutation addVideo($videoUrl:String!,$category:Int!,$year:String){
    addVideo(videoUrl:$videoUrl,category:$category,year:$year)
  }
`;
export default compose(
  graphql(gql`{getCategories(type:"videos"){id,titleAr,titleEn}}`),
  graphql(addVideo, { name: 'addVideo' }),
)(AddVideo);
