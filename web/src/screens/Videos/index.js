import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Paper, Table, TableCell, 
  TableHead, TableRow, TableBody, TableFooter, TablePagination } from '@material-ui/core';

const PER_PAGE = 10;
class Videos extends Component {
  state ={
    page: 0,
  }
  componentDidMount() {
    console.log(this.props);
    this.handleChangePage = this.handleChangePage.bind(this);
  }
  handleChangePage(event, page) {
    this.setState({ page }, () => {
      this.props.data.fetchMore({ 
        variables: {
          limit: PER_PAGE,
          offset: page * PER_PAGE,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          console.log(fetchMoreResult);
          if (!fetchMoreResult) { return previousResult; }
          return Object.assign({}, previousResult, {
            getVideos: fetchMoreResult.getVideos,
          });
        }, 
      });
    });
  }
  async deleteVideo(videoId) {
    if (window.confirm('هل انت متاكد من الحذف ؟')) {
      await this.props.deleteVideo({
        variables: {
          videoId,
        },
      });
      window.location.reload();
    }
  }

  render() {
    if (this.props.data.loading) {
      return null;
    }
    if (this.props.data.error) {
      return <h4>خطا </h4>;
    }
    console.log(this.props); 
  
    return (
      <div>
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <Paper >
            <Button
              variant="raised" type='submit' color="primary" style={{ marginTop: 30, color: 'white' }}
              href='/videos/add'
              fullWidth
            >
               اضافة فيديو جديد
            </Button>
            <Table >
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>صورة</TableCell>
                  <TableCell>العنوان</TableCell>
                  <TableCell>القسم</TableCell>
                  <TableCell>عدد مشاهدات</TableCell>
                  <TableCell >سنة الاصدار</TableCell>
                  <TableCell >اجراءات</TableCell>
                </TableRow>
              </TableHead>
              <TableBody >
                {!this.props.data.getVideos.length && <h4>فارغ</h4> }
                {
                  this.props.data.getVideos.map((video) => (
                    <TableRow key={video.id}>
                      <TableCell component="th" scope="row">
                        {video.id}
                      </TableCell>
                      <TableCell>
                        <img src={video.thumbnail} width={55} />                          
                      </TableCell>
                      <TableCell>
                        <a href={`https://www.youtube.com/watch?v=${video.videoId}`} target='_blank'>
                          <h4 style={{ color: 'blue' }}>{video.title}</h4>
                        </a>
                      </TableCell>
                      <TableCell>
                        <h4>{video.category}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{video.showCount}</h4>
                      </TableCell>
                      <TableCell>
                        <h4>{video.year || ''}</h4>
                      </TableCell>
                      <TableCell>
                        <div>
                          <Button
                            style={{ marginLeft: 17 }}
                            variant="raised" type='submit' onClick={() => {
                              this.deleteVideo(video.id);
                            }} color="secondary" fullWidth
                          >
               حذف 
                          </Button> 
                          <Button
                            variant="raised" type='submit' 
                            color="primary" fullWidth
                            href={`videos/${video.id}/edit`}
                          >
               تعديل 
                          </Button> 
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    labelRowsPerPage='عدد لكل سجل'
                    colSpan={3}
                    rowsPerPage={PER_PAGE}
                    labelDisplayedRows={({ from, to, count }) => ` ${from}-${to} من عدد الكل ${count}`}
                    count={this.props.data.getVideosCount}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
           
          </Paper>
        </Grid>
      </div>
          
    );
  }
}

const GET_VIDEOS = gql`
query($limit:Int = 15,$offset:Int = 0){
  getVideos(limit:$limit, offset:$offset){
    id
    title
    showCount
    videoId
    thumbnail
    year
    category
}
  getVideosCount
}
`;

const deleteVideo = gql`
  mutation deleteVideo($videoId:Int!){
    deleteVideo(videoId:$videoId )
  }
`;
export default compose(
  graphql(GET_VIDEOS),
  graphql(deleteVideo, { name: 'deleteVideo' }),
)(Videos);
