import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Button from '@material-ui/core/Button';
import gql from 'graphql-tag';
import { Grid, Input, Select, MenuItem } from '@material-ui/core';

class EditVideo extends Component {
  state ={
    init: true,
    loading: false, 
    videoUrl: undefined,
    year: '',
    videoId: '',
    category: '',
  }
  async componentDidMount() {
    this.onSubmit = this.onSubmit.bind(this);
    const { id } = this.props.match.params;
    this.setState({ videoId: id });
    const { data } = await this.props.getVideo({ variables: { videoId: id } });
    console.log(data);
    const { categoryId, videoId, year } = data.getVideo;
    const category = categoryId;
    this.setState({ videoUrl: `https://www.youtube.com/watch?v=${videoId}`, category, year, init: false });
  }

  async onSubmit(e) {
    e.preventDefault();
    const { category, loading, videoUrl, year, videoId } = this.state;
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\\&v=|\?v=)([^#\\&\\?]*).*/;

    if (!videoUrl) {
      alert(' ادخل رابط الفيديو اولا');
      return;
    }
    if (!(videoUrl.match(regExp))) {
      alert(' تاكد من رابط الفيديو');
      return;
    }
   
    if (loading) {
      return;
    }
    this.setState({ loading: true });
  
    try {
      const cate = category || this.props.data.getCategories[0].id;
      await this.props.updateVideo({ variables: { category: cate, videoUrl, year, videoId } });
      document.location = '/videos';
    } catch (error) {
      alert('خطا غير معروف');
      console.log(error);
    } 
    this.setState({ loading: false });
  }

  render() {
    if (this.state.init || this.props.data.loading) {
      return (
        <h2>
              جاري التحميل       
        </h2>
      );
    }
    return (
      <div >
        <Grid container justify='center' style={{ marginTop: 50 }} >
          <form onSubmit={this.onSubmit} className='wrapp-border'>
            <Grid >
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>رابط المقطع</label>

                <Input
                  onChange={event => this.setState({ videoUrl: event.target.value })}
                  style={{ marginTop: 20 }}            
                  placeholder="ضع هنا رابط المقطع من اليوتوب"
                  fullWidth
                  defaultValue={this.state.videoUrl}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>السنه</label>

                <Input
                  onChange={event => this.setState({ year: event.target.value })}
                  style={{ marginTop: 20 }}            
                  fullWidth
                  defaultValue={this.state.year}
                />        
              </div>   
              <div style={{ marginTop: 20 }}>
                <label htmlFor='select'>القسم</label>
                <Select
                  value={this.state.category || this.props.data.getCategories[0].id}
                  onChange={(e) => this.setState({ category: e.target.value })}
                  name="name"
                  fullWidth
                  input={<Input fullWidth />}
                >
                  {this.props.data.getCategories.map((c) => (
                    <MenuItem key={c.id} value={c.id}>
                      <em>{c.titleAr}</em>
                    </MenuItem>
                  ))}
                 
                </Select>
              </div>  
              {
                this.state.loading ? <h4 style={{ textAlign: 'center' }}>جاري الاضافة ...</h4> :
                  <Button
                    variant="raised" type='submit' color="primary" style={{ marginTop: 30 }}
                    fullWidth
                  >
               تعديل 
                  </Button>
              }
              
            </Grid>
          </form>
        </Grid>
      </div>
          
    );
  }
}
const GET_VIDEO = gql`
    mutation getVideo($videoId:Int!){
        getVideo(videoId:$videoId){
            id
            videoId
            year
            categoryId
        }
    }
`;
const updateVideo = gql`
  mutation updateVideo($videoId:Int!,$category:Int!,$videoUrl:String!,$year:String){
    updateVideo(videoId:$videoId,category:$category,videoUrl:$videoUrl,year:$year)
  }
`;
export default compose(
  graphql(gql`{getCategories(type:"videos"){id,titleAr,titleEn}}`),
  graphql(GET_VIDEO, { name: 'getVideo' }),
  graphql(updateVideo, { name: 'updateVideo' }),
)(EditVideo);
