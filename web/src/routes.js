import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import Toolbar from './components/Toolbar';
import Albums from './screens/Albums';
import addAlbum from './screens/Albums/addAlbum';
import editAlbum from './screens/Albums/editAlbum';

import Songs from './screens/Songs';
import addSong from './screens/Songs/addSong';
import editSong from './screens/Songs/editSong';

import Videos from './screens/Videos';
import addVideo from './screens/Videos/addVideo';
import editVideo from './screens/Videos/editVideo';

import Books from './screens/Books';
import addBook from './screens/Books/addBook';
import editBook from './screens/Books/editBook';
import Events from './screens/Events';
import addEvent from './screens/Events/addEvent';
import editEvent from './screens/Events/editEvent';
import ChangePassword from './screens/ChangePassword';
import Categories from './screens/Categories';
import addCategory from './screens/Categories/addCategory';
import editCategory from './screens/Categories/editCategory';

export default () => (
  <Router>
    <div>
      <Toolbar />     
         
      <Route exact path="/categories" component={Categories} />
      <Route exact path="/categories/add" component={addCategory} />
      <Route exact path="/categories/:id/edit" component={editCategory} />

      <Route exact path="/" component={Albums} />
      <Route exact path="/albums" component={Albums} />
      <Route exact path="/albums/add" component={addAlbum} />
      <Route exact path="/albums/:id/edit" component={editAlbum} />

      <Route exact path="/songs" component={Songs} />
      <Route exact path="/songs/add" component={addSong} />
      <Route exact path="/songs/:id/edit" component={editSong} />

      <Route exact path="/videos" component={Videos} />
      <Route exact path="/videos/add" component={addVideo} /> 
      <Route exact path="/videos/:id/edit" component={editVideo} />

      <Route exact path="/books" component={Books} />
      <Route exact path="/books/add" component={addBook} />
      <Route exact path="/books/:id/edit" component={editBook} />

      <Route exact path="/events" component={Events} />
      <Route exact path="/events/add" component={addEvent} />
      <Route exact path="/events/:id/edit" component={editEvent} />

      <Route exact path="/changepassword" component={ChangePassword} />

    </div>
  </Router>
);
