import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as Icon from 'react-icons/lib/fa';
import { Toolbar, AppBar, Typography, IconButton, Drawer, List, ListItem, Divider, Button, Menu, MenuItem } from '@material-ui/core';

class CustomToolbar extends Component {
  state = { open: false, anchorEl: null }
  componentWillMount() {
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleMenu = this.handleMenu.bind(this);
  }
  handleDrawerToggle = () => {
    this.setState({ open: !this.state.open });
  };
  handleDrawerOpen = () => {
    this.setState({ open: true });
  };
  handleMenu(event) {
    this.setState({ anchorEl: event.currentTarget });
  }
  render() {
    const open = Boolean(this.state.anchorEl);
    return (
      <div>
        <AppBar position="static" color="primary">
          <Toolbar>
            <IconButton
              onClick={this.handleDrawerOpen}
              color="inherit" aria-label="Menu"
            >
              <Icon.FaBars />
            </IconButton>
            <Typography variant="title" color="inherit">
            الصفحة الرئيسية 
            </Typography>
            <Button style={{ color: 'white' }} href='/categories' >
 الاقسام 
            </Button>
            <div className='divVertical' />
            <Button style={{ color: 'white' }} href='/albums' >
 الالبومات 
            </Button>
            <div className='divVertical' />
            <Button style={{ color: 'white' }} href='/songs' >
 الصوتيات 
            </Button>
            <div className='divVertical' />
            <Button style={{ color: 'white' }} href='/videos' >
 الفيديوهات     
            </Button>
            <div className='divVertical' />            
            <Button style={{ color: 'white' }} href='/books' >
 الكتب     
            </Button>
            <div className='divVertical' />            
            <Button style={{ color: 'white' }} href='/events' >
 المناسبات     
            </Button>
            <div className='divVertical' />            
            <div style={{ flex: 1, textAlign: 'left' }}>
              <IconButton
                aria-owns={open ? 'menu-appbar' : null}
                aria-haspopup="true"
                onClick={this.handleMenu}
                color="inherit"
              >
                <Icon.FaUser />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={this.state.anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={() => this.setState({ anchorEl: null })}
              >
                <MenuItem >
                  <Link to='/changepassword'>تغير كلمه السر</Link>
                </MenuItem>
                <MenuItem onClick={async () => {
                  window.location = '/logout';
                }}
                >تسجيل الخروح</MenuItem>
              </Menu>
            </div>
          </Toolbar>
        </AppBar>
        <Drawer
          // variant="permanent"
        
          anchor={'right'}
          open={this.state.open}
          onClose={this.handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <List dense={false}>
            <ListItem>
              <Link to="/categories"> الاقسام</Link>
            </ListItem>
            <ListItem>
              <Link to="/albums"> الالبومات</Link>
            </ListItem>
            <Divider />
            <ListItem>
              <Link to="/songs"> الصوتيات</Link>
            </ListItem>
            <Divider />
            <ListItem>
              <Link to="/videos">الفيديوهات</Link>
            </ListItem>
            <ListItem>
              <Link to="/books">الكتب</Link>
            </ListItem>
            <ListItem>
              <Link to="/events">المناسبات</Link>
            </ListItem>
            
          </List>
        </Drawer>
      </div>
    );
  }
}

export default CustomToolbar; 
