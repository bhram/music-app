import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Routes from './routes';
import client from './client';

const theme = createMuiTheme({
  typography: {
    direction: 'rtl',
    fontFamily: [
      'Cairo',
    ].join(','),
  },

  drawerPaper: {
    position: 'relative',
    width: 655,
  },
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <ApolloProvider client={client}>
          <Routes />
        </ApolloProvider>
      </MuiThemeProvider>
    );
  }
}

export default App;
