import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createUploadLink } from 'apollo-upload-client';
import { ApolloClient } from 'apollo-client';

const httpLink = createUploadLink({ uri: 
    `http://${window.location.hostname}:${window.location.port ? 4000 : 80}` });
  
const middlewareLink = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      authorization: window.token || null,
    },
  });
  return forward(operation);
});
  
const link = middlewareLink.concat(httpLink);
// const link = createUploadLink ({})
  
export default new ApolloClient({
  link,
  cache: new InMemoryCache(),
});
