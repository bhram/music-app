import { FETCH_VIDEOS, SHOW_VIDEO, HIDE_VIDEO, FETCH_PROFILE } from '../utils/types';

const INIT_STATE = {
  videos: [],
  error: false,
  loading: true,
  isShow: false,
  video: null,
};
export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_PROFILE.COMPLETE:
      return {
        videos: action.payload.getVideos,
      };
    case FETCH_VIDEOS.COMPLETE:
      return {
        ...state,
        videos: [...state.videos, ...action.videos],
        loading: false,
        error: false,
      };
    case FETCH_VIDEOS.LOADING:
      return {
        ...state,
        videos: [],
        loading: true,
      };
    case FETCH_VIDEOS.ERROR:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case SHOW_VIDEO:
      return {
        ...state,
        isShow: true,
        video: action.video,
      };  
    case HIDE_VIDEO:
      return {
        ...state,
        isShow: false,
        video: {},
      };  
    default:
      return state;
  }
};
