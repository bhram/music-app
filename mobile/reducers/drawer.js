import { TOOGLE_DRAWER } from '../utils/types';

export default (state = { isOpen: false }, action) => {
  switch (action.type) {
    case TOOGLE_DRAWER:
      return { isOpen: !state.isOpen }; 
    default:
      return state;
  }
};
