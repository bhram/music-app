import MusicControl from 'react-native-music-control';
import { SWITCH_SONG, SEND_PAYLOAD, CLOSE_AUDIO } from '../utils/types';
import { isArabic } from '../locales/I18n';

const INIT_STATE = {
  currentTrack: {},
  paused: false,
  isBuffring: false,
  duration: 0,
  currentTime: 0,
  repeat: false,
  shuffle: false,
  fav: false,
  switch: false,
};
export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SWITCH_SONG:
      SwitchSong(action.track);
      return {
        ...state,
        switch: true,
        currentTrack: action.track,
        fav: action.fav,
      };
    case SEND_PAYLOAD:
      if (action.payload.isBuffring) {
        MusicControl.updatePlayback({ state: MusicControl.STATE_BUFFERING });
      } else if (action.payload.paused) {
        MusicControl.updatePlayback({ state: MusicControl.STATE_PLAYING });
      } else {
        MusicControl.updatePlayback({ state: MusicControl.STATE_PAUSED });
      }
     
      return {
        ...state,
        ...action.payload,
        switch: false,
      };
    case CLOSE_AUDIO:
      MusicControl.stopControl();
      return {
        ...state,
        currentTrack: {},
        switch: false,
      };
    default:
      return state;
  }
};

const SwitchSong = (track) => {
  MusicControl.setNowPlaying({
    title: track[isArabic ? 'titleAr' : 'titleEn'],
    artwork: track.photo, 
    album: 'Hussain Al Akraf',
    duration: track.duration, 
    color: 0xFFFFFF, 
    notificationIcon: 'ic_launcher', 
  });
};
