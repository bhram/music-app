import { FETCH_PROFILE } from '../utils/types';

const INIT_STATE = {
  loading: true,
};
export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_PROFILE.LOADING: {
      return {
        ...state,
        loading: true,
      };
    }
    case FETCH_PROFILE.COMPLETE:
      return {
        ...state,
        ...action.payload,
        loading: false,
      };
    case FETCH_PROFILE.LOGOUT:
      return {
        ...state,
        me: undefined,
      };
    default:
      return state;
  }
};
