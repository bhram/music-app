import { SHOW_VIDEO, CLEAR_HISTORY_VIDEO } from '../utils/types';

export default (state = { videos: [] }, action) => {
  switch (action.type) {
    case SHOW_VIDEO:
      return {
        videos: [action.video, ...state.videos.filter((s) => s.id !== action.video.id)],
      }; 
    case CLEAR_HISTORY_VIDEO:
      return {
        videos: [],
      };
    default:
      return state;
  }
};
