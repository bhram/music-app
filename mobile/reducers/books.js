import { FETCH_BOOKS } from '../utils/types';

const INIT_STATE = {
  books: [],
  error: false,
  loading: true,
};
export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_BOOKS.COMPLETE:
      return {
        books: [...state.books, ...action.books],
        loading: false,
        error: false,
      };
    case FETCH_BOOKS.LOADING:
      return {
        books: [],
        loading: true,
      };
    case FETCH_BOOKS.ERROR:
      return {
        ...state,
        loading: true,
        error: false,
      };
    default:
      return state;
  }
};
