import { FETCH_SONGS, PLAYBACK_TRACK, FETCH_PROFILE } from '../utils/types';

const INIT_STATE = {
  songs: [],
  error: false,
  loading: true,
};
export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_PROFILE.COMPLETE:
      return {
        songs: action.payload.getSongs,
      };
    case FETCH_SONGS.COMPLETE:
      return {
        songs: [...state.songs, ...action.songs],
        loading: false,
        error: false,
      };
    case FETCH_SONGS.LOADING:
      return {
        songs: [],
        loading: true,
      };
    case FETCH_SONGS.ERROR:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case PLAYBACK_TRACK:
      return {
        ...state,
        currentTrack: action.track,
      };
    default:
      return state;
  }
};
