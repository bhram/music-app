import { REHYDRATE } from 'redux-persist';
import { TOOGLE_FAVS } from '../utils/types';

export default (state = { songs: [] }, action) => {
  switch (action.type) {
    case REHYDRATE:
      if (action.payload) {
        return {
          songs: action.payload.favs.songs,
        };
      }
      return { ...state };
    case TOOGLE_FAVS:
      if (state.songs.findIndex((e) => e.id === action.song.id)) {
        return {
          songs: [action.song, ...state.songs],
        }; 
      }
      return { songs: state.songs.filter((s) => s.id !== action.song.id) };
    default:
      return state;
  }
};
