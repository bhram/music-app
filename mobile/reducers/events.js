import { FETCH_EVNETS } from '../utils/types';

const INIT_STATE = {
  events: [],
  error: false,
  loading: true,
};
export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_EVNETS.COMPLETE:
      return {
        events: [...state.events, ...action.events],
        loading: false,
        error: false,
      };
    case FETCH_EVNETS.LOADING:
      return {
        events: [],
        loading: true,
      };
    case FETCH_EVNETS.ERROR:
      return {
        ...state,
        loading: true,
        error: false,
      };
    default:
      return state;
  }
};
