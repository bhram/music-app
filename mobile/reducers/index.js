import { persistCombineReducers } from 'redux-persist';
import { AsyncStorage } from 'react-native';
import songs from './songs';
import player from './player';
import albums from './albums';
import progress from './progress';
import favs from './favs';
import profile from './profile';
import history from './history';
import videos from './videos';
import historyVideos from './historyVideos';
import books from './books';
import events from './events';
import nav from './navReducer';
import drawer from './drawer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['favs', 'history', 'historyVideos'],
};
export default persistCombineReducers(persistConfig, {
  songs,
  player,
  albums,
  progress,
  favs,
  profile,
  history,
  videos,
  books,
  events,
  historyVideos,
  nav,
  drawer,
});
