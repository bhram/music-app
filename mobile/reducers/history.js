import { SWITCH_SONG, CLEAR_HISTORY } from '../utils/types';

export default (state = { songs: [] }, action) => {
  switch (action.type) {
    case SWITCH_SONG:
      return {
        songs: [action.track, ...state.songs.filter((s) => s.id !== action.track.id)],
      }; 
    case CLEAR_HISTORY:
      return {
        songs: [],
      };
    default:
      return state;
  }
};
