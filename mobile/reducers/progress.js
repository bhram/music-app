import { PROGRESS_TIME } from '../utils/types';

export default (state = { currentTime: 0 }, action) => {
  switch (action.type) {
    case PROGRESS_TIME:
      return { currentTime: action.currentTime };
    default:
      return state;
  }
};
