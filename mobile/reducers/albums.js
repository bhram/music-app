import { FETCH_ALBUMS } from '../utils/types';

const INIT_STATE = {
  loading: true,
  error: [],
  albums: [],

};
export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ALBUMS.LOADING:
      return { ...state, loading: true };
    case FETCH_ALBUMS.COMPLETE:
      return { ...state, loading: false, albums: action.albums };
    case FETCH_ALBUMS.Error:
      return { ...state, loading: false, error: true };
    default:
      return state;
  }
};
