import { Client, ADD_TO_FAVOURITE, REMOVE_FROM_FAVOURITE } from '../graphql';
import { TOOGLE_FAVS, SEND_PAYLOAD } from '../utils/types';
import { isSignIn } from '../utils/tokens';

export const addToFav = (song) => async dispatch => {
  if (isSignIn()) {
    await Client.mutate({
      mutation: ADD_TO_FAVOURITE,
      variables: { songId: song.id },
    });
  }
  dispatch({ type: TOOGLE_FAVS, song });
  dispatch({ type: SEND_PAYLOAD, payload: { fav: true } });
};

export const removeFav = (song) => async (dispatch) => {
  if (isSignIn()) {
    await Client.mutate({
      mutation: REMOVE_FROM_FAVOURITE,
      variables: { songId: song.id },
    });
  }
  dispatch({ type: TOOGLE_FAVS, song });
  dispatch({ type: SEND_PAYLOAD, payload: { fav: false } });
};

// return bool
// export const checkInFav = (id) => (_, getState) => {
//   const index = getState().favs.songs.findIndex((s) => s.id === id);
//   return index > -1;
// };
