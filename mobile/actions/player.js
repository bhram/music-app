import { SEND_PAYLOAD, SWITCH_SONG } from '../utils/types';

export const sendPayload = (payload) => ({
  type: SEND_PAYLOAD,
  payload,
});

export const play = () => ({
  type: SEND_PAYLOAD,
  payload: { paused: false },
});

export const pause = () => ({
  type: SEND_PAYLOAD,
  payload: { paused: true },
});

export const backward = () => (dispatch, getState) => {
  const { currentTrack } = getState().player;
  const { songs } = getState().songs;
  const index = songs.findIndex((s) => s.id === currentTrack.id);
  let i = 0;
  if (index > 0) {
    i = index - 1;
  } else {
    i = songs.length - 1;
  }
  dispatch({
    type: SWITCH_SONG,
    track: songs[i],
  });
};

export const forward = () => (dispatch, getState) => {
  const { currentTrack } = getState().player;
  const { songs } = getState().songs; 
  const index = songs.findIndex((s) => s.id === currentTrack.id);
  let i = 0;
  if (index < songs.length - 1) {
    i = index + 1;
  }
  dispatch({
    type: SWITCH_SONG,
    track: songs[i],
  });
};
