import { FETCH_PROFILE, SEND_PAYLOAD, CLEAR_HISTORY, CLEAR_HISTORY_VIDEO } from '../utils/types';
import { Client, ME, HOME_WITHOUT_LOGIN } from '../graphql';
import { isSignIn } from '../utils/tokens';

export const fetchProfile = () => async dispatch => {
  dispatch({ type: FETCH_PROFILE.LOADING });
  try {
    const { data } = await Client.query({
      query: isSignIn() ? ME : HOME_WITHOUT_LOGIN, 
      fetchPolicy: 'network-only',
    });
  
    dispatch({ type: FETCH_PROFILE.COMPLETE, payload: { ...data } });
  } catch (error) {
    console.log(error);
    dispatch({ type: FETCH_PROFILE.ERROR });
  }
};

export const logout = () => async dispatch => {
  dispatch({ type: SEND_PAYLOAD, payload: { currentTrack: {} } });
  dispatch({ type: CLEAR_HISTORY });
  dispatch({ type: FETCH_PROFILE.LOGOUT });
  dispatch({ type: CLEAR_HISTORY_VIDEO });
};

