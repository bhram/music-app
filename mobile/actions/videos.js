import { FETCH_VIDEOS, SHOW_VIDEO, HIDE_VIDEO } from '../utils/types';
import { Client, GET_VIDEOS } from '../graphql';

export const fetchVideos = (isRefresh, variables) => async dispatch => {
  if (isRefresh) {
    dispatch({ type: FETCH_VIDEOS.LOADING });
  }
  try {
    const { data } = await Client.query({
      query: GET_VIDEOS,
      variables,
      fetchPolicy: 'network-only',
    });    
    dispatch({ type: FETCH_VIDEOS.COMPLETE, videos: data.getVideos });
  } catch (error) {
    dispatch({ type: FETCH_VIDEOS.LOADING, payload: error.message });
  }
  return Promise.resolve();
};

export const showVideo = (video) => ({
  type: SHOW_VIDEO, video,
});
export const hideVideo = () => ({
  type: HIDE_VIDEO,
});
