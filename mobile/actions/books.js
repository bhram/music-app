import { FETCH_BOOKS } from '../utils/types';
import { Client, GET_BOOKS } from '../graphql';

export const fetchBooks = (isRefresh, variables) => async dispatch => {
  if (isRefresh) {
    dispatch({ type: FETCH_BOOKS.LOADING });
  }
  try {
    const { data } = await Client.query({
      query: GET_BOOKS,
      variables,
      fetchPolicy: 'network-only',

    });    
    dispatch({ type: FETCH_BOOKS.COMPLETE, books: data.getBooks });
  } catch (error) {
    dispatch({ type: FETCH_BOOKS.LOADING, payload: error.message });
  }
  return Promise.resolve();
};
  
