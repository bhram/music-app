import { FETCH_ALBUMS } from '../utils/types';
import { Client, GET_ALBUMS } from '../graphql';

export const fetchAlbums = (categoryId) => async dispatch => {
  dispatch({ type: FETCH_ALBUMS.LOADING });
  try {
    const { data } = await Client.query({
      query: GET_ALBUMS,
      variables: { categoryId },
      fetchPolicy: 'network-only',
    });    
    dispatch({ type: FETCH_ALBUMS.COMPLETE, albums: data.getAlbums });
  } catch (error) {
    dispatch({ type: FETCH_ALBUMS.LOADING, payload: error.message });
  }
};
