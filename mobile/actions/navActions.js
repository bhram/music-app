import { NavigationActions } from 'react-navigation';
import { TOOGLE_DRAWER } from '../utils/types';

export const navigate = (routeName, params) => dispatch => {
  dispatch(NavigationActions.navigate({ routeName, params }));
};

export const toggleDrawer = () => dispatch => {
  dispatch({ type: TOOGLE_DRAWER });
};
