import { FETCH_EVNETS } from '../utils/types';
import { Client, GET_EVENTS } from '../graphql';

export const fetchEvents = (isRefresh, variables) => async dispatch => {
  if (isRefresh) {
    dispatch({ type: FETCH_EVNETS.LOADING });
  }
  try {
    const { data } = await Client.query({
      query: GET_EVENTS,
      variables,
    });    
    dispatch({ type: FETCH_EVNETS.COMPLETE, events: data.getEvents });
  } catch (error) {
    dispatch({ type: FETCH_EVNETS.LOADING, payload: error.message });
  }
  return Promise.resolve();
};
  
