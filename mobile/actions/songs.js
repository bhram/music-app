import { Client, GET_SONG_ALBUM, INCREMENT_LISTEN } from '../graphql';
import { FETCH_SONGS, SWITCH_SONG, PROGRESS_TIME, CLOSE_AUDIO } from '../utils/types';

export const switchSong = (track) => (dispatch, getState) => {
  Client.mutate({
    mutation: INCREMENT_LISTEN,
    variables: { songId: track.id },
  });
  const index = getState().favs.songs.findIndex((s) => s.id === track.id);
  const fav = index > -1;
  dispatch({
    type: SWITCH_SONG,
    track,
    fav,
  });
};

export const runShuffle = () => (dispatch, getState) => {
  const { songs } = getState().songs;
  const track = songs[Math.floor(songs.length * Math.random())];
  dispatch({
    type: SWITCH_SONG,
    track,
  });
};

export const changeCurrentTime = (currentTime) => ({
  type: PROGRESS_TIME,
  currentTime,
});

export const fetchSongs = (isRefresh, variables) => async dispatch => {
  if (isRefresh) {
    dispatch({ type: FETCH_SONGS.LOADING });
  }
  try {
    const { data } = await Client.query({
      query: GET_SONG_ALBUM,
      variables,
      fetchPolicy: 'network-only',
    });    
    dispatch({ type: FETCH_SONGS.COMPLETE, songs: data.getSongs });
  } catch (error) {
    dispatch({ type: FETCH_SONGS.LOADING, payload: error.message });
  }
  return Promise.resolve();
};

export const closeAudio = () => ({
  type: CLOSE_AUDIO,
});
