import React, { Component } from 'react';
import MusicControl from 'react-native-music-control';
import { View, StatusBar } from 'react-native';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import { store } from './utils/Store';
import { Client } from './graphql';
import { Colors } from './utils/constances';
import Player from './screens/Player';
import MiniPlayer from './components/MiniPlayer';
import { pause, play, backward, forward, closeAudio } from './actions';
import DrawerNavigator from './navigations/DrawerNavigator';
import VideoEmbed from './screens/VideoEmbed';

console.disableYellowBox = true;

class Root extends Component {
  componentDidMount() {
    // setTimeout(() => {
    //   this.jsm.toggleSideMenu(true);
    // }, 2000);
    MusicControl.enableControl('play', true);
    MusicControl.enableControl('pause', true);
    MusicControl.enableControl('stop', false);
    MusicControl.enableControl('nextTrack', true);
    MusicControl.enableControl('previousTrack', true);
    MusicControl.on('play', () => {
      store.dispatch(pause());
    });
    MusicControl.on('pause', () => {
      store.dispatch(play());
    });

    MusicControl.on('nextTrack', () => {
      store.dispatch(forward());
    });

    MusicControl.on('previousTrack', () => {
      store.dispatch(backward());
    });
    MusicControl.on('closeNotification', () => {
      store.dispatch(closeAudio());
    });
  }
  render() {
    return (
      <Provider store={store}>
        <ApolloProvider client={Client}>
          <React.Fragment>
            <View flex={1}>
              <Player />
              <StatusBar backgroundColor={Colors.stateBar} barStyle="light-content" />
              <DrawerNavigator />
              <VideoEmbed navigation={this.props.navigation} />
            </View>
            <MiniPlayer />
          </React.Fragment>
          
        </ApolloProvider>
      </Provider>      
    );
  }
}

// export default App;
export default Root;
