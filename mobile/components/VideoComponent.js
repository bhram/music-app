import React, { PureComponent } from 'react';
import { StyleSheet, ImageBackground, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Row, View } from '../components';
import { Text, Icon, IconPlay } from './commons';
import * as actions from '../actions';
import { Colors } from '../utils/constances';
import { nFormatter, convertTimeFormat } from '../utils';
import { isArabic } from '../locales/I18n';
import { Client, INCREMENT_VIEW_VIDEO } from '../graphql';
    
class VideoComponent extends PureComponent {
  componentWillMount() {
    this.openVideo = this.openVideo.bind(this);
  }
  openVideo() {
    this.props.closeAudio();
    Client.mutate({
      mutation: INCREMENT_VIEW_VIDEO,
      variables: { videoId: this.props.video.id },
    });
    this.props.showVideo(this.props.video);
  }
  render() {
    const { video, transparent } = this.props;
    const color = transparent ? 'white' : Colors.btnColors;
    return (
      <TouchableOpacity
        activeOpacity={0.9} onPress={this.openVideo} 
        style={[styles.rowBox, transparent && styles.borderLeft]}
      >
        <ImageBackground style={styles.image} source={{ uri: video.thumbnail }} >
          <IconPlay />
          <Text
            bold style={styles.durationStyle}
          >{convertTimeFormat(video.duration)}</Text>
        </ImageBackground>
        <View style={styles.wrappText}>
          <Text style={[styles.title, { color: transparent ? 'white' : 'black' }]}>{video.title}</Text> 
          <View style={styles.flexEnd}>
            <Row>
              <Row style={styles.marginHor}>
                <Icon color={color} name={'ios-eye'} size={25} />
                <Text style={[styles.iconStyle, { color }]}>{nFormatter(video.showCount || 0)}</Text>
              </Row>
              {video.year &&
            <Row style={styles.marginHor}>
              <Icon color={color} name={'ios-film-outline'} size={25} />
              <Text style={[styles.iconStyle, { color }]}>{video.year }</Text>
            </Row>
              }
            </Row>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  rowBox: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgba(255,255,255,.3)',
    marginTop: 10, 
    padding: 10,
  },
  borderLeft: {
    borderLeftColor: Colors.btnColors,
    borderLeftWidth: 2,
  },
  title: { textAlign: isArabic ? 'left' : 'right' },
  flexEnd: { flex: 1, justifyContent: 'flex-end' },
  wrappText: { flex: 1, marginStart: 10 },
  image: { width: 140, height: 90, justifyContent: 'center', alignItems: 'center' },
  marginHor: { marginHorizontal: 10, alignItems: 'center' },
  iconStyle: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.btnColors,
    marginHorizontal: 7, 
  }, 
  durationStyle: { 
    fontSize: 12,
    position: 'absolute',
    bottom: 0, 
    backgroundColor: 'rgba(0,0,0,0.6)',
    paddingHorizontal: 5,
    left: 0 },
});

export default connect(undefined, actions)(VideoComponent);
