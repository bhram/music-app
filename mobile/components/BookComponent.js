import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text, Row, Icon } from './commons';
import { Colors } from '../utils/constances';
import { nFormatter } from '../utils';
import DownloadBook from './DownloadBook';
    
class BookComponent extends Component {
  render() {
    const { book, transparent } = this.props;
    const color = transparent ? 'white' : Colors.btnColors;
    return (
      <Row style={styles.wrapp}>

        <Image
          style={{ width: 100, height: 80 }} source={!book.photo ? require('../assets/images/download.png') :
            { uri: book.photo }}
        />
        
        <View style={styles.wrappText}>
          <Text style={[styles.title, { color: transparent ? 'white' : 'black' }]}>{book.title}</Text> 
          <View style={styles.flexEnd}>
            <Row style={styles.marginHor}>
              <Icon color={color} name={'ios-cloud-download-outline'} size={25} />
              <Text style={[styles.iconStyle, { color }]}>{nFormatter(book.downloadCount || 0)}</Text>
            </Row>
          </View>
        </View>
        <DownloadBook book={book} />
      </Row>
    );
  }
}

const styles = StyleSheet.create({
  wrapp: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,.3)',
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 1,
    marginTop: 10, 
    padding: 10,
  },
  wrappText: { flex: 1, marginStart: 30 },
  flexEnd: { flex: 1, marginTop: 20, justifyContent: 'flex-end' },
  marginHor: { marginHorizontal: 10, alignItems: 'center' },
  title: { alignSelf: 'flex-start' },
  iconStyle: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.btnColors,
    marginHorizontal: 7, 
  }, 
});

export default BookComponent;
