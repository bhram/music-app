import React, { Component } from 'react';
import { TextInput, StyleSheet } from 'react-native';
import { isArabic } from '../locales/I18n';
import { Fonts, isAndroid } from '../utils/constances';
import { Row, Icon } from './commons';

export class Form extends Component {
  clear() {
    this.input.clear();
  }
  focus() {
    this.input.focus();
  }
  render() {
    return (
      <Row
        style={styles.container}
        paddingHorizontal={this.props.paddingHorizontal || 25}
      >
        {this.props.icon &&
        <Icon name={this.props.icon} size={30} />
        }
        <TextInput
          ref={(r) => { this.input = r; }}
          placeholder={this.props.placeholder} 
          style={[styles.style, { paddingVertical: 14 }, this.props.style]}
          paddingHorizontal={15}
          value={this.props.value}
          underlineColorAndroid='transparent'
          placeholderTextColor='white'
          secureTextEntry={this.props.secureTextEntry}
          autoCapitalize={this.props.autoCapitalize || 'none'}
          autoCorrect={false}
          onChangeText={this.props.onChangeText}
          returnKeyType={this.props.returnKeyType}
          blurOnSubmit={this.props.blurOnSubmit}
          onSubmitEditing={this.props.onSubmitEditing}
        />    
      </Row>  
    );
  }
}

const styles = StyleSheet.create({
  style: { 
    color: 'white',
    fontSize: 18, 
    paddingVertical: !isAndroid ? 16 : 0,
    flex: 1,
    fontFamily: Fonts.Bahij,
    textAlign: isArabic ? 'right' : 'left',
  },
  container: {
    alignItems: 'center',
    borderBottomColor: 'rgba(255,255,255,.5)', 
    borderBottomWidth: 1, 
  },
});
