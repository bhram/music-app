import React, { Component } from 'react';
import { FlatList, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Row, View, Text, Icon as Ionicon } from '.';
import { Colors } from '../utils/constances';
import { nFormatter } from '../utils';

export class AlbumsList extends Component {
  render() {
    return (
      <FlatList
        data={this.props.albums}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => this.props.onSelectAlbum(item)} 
            style={styles.albumBox}
          >
            
            <ImageBackground
              style={styles.iconPlay}
              source={{ uri: item.photo }}
            >
              <Icon name='play-circle-outline' color='white' size={40} />
            </ImageBackground>
            <View style={styles.infoWrap}>
              <View flex={1} justifyContent='center'>
                <Text style={styles.gray} >
                  {item.titleEn}
                </Text>
              </View>
              <Row style={styles.wrappIcons}>
                <Row style={{ marginHorizontal: 10 }} alignItems='center' >
                  <Ionicon name={'ios-headset-outline'} size={25} />
                  <Text style={styles.iconStyle}>{nFormatter(item.listenCount || 0)}</Text>
                </Row>
                <Row style={{ marginHorizontal: 10 }} alignItems='center' >
                  <Ionicon name={'ios-list-outline'} size={25} />
                  <Text style={styles.iconStyle}>{nFormatter(item.songCount || 0)}</Text>
                </Row>
              </Row>
            </View>
          </TouchableOpacity>
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  albumBox:
    {
      flexDirection: 'row',
      borderBottomColor: '#f1f1f1',
      borderBottomWidth: 1,
      paddingBottom: 10,
      marginBottom: 10, 
    },
  iconPlay: { 
    width: 120,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',        
    backgroundColor: Colors.gray,
  },
  infoWrap: { marginStart: 10, flex: 1 },
  gray: { color: Colors.gray },
  txtName: { color: '#B1B1B1' },
  wrappIcons: {
    flex: 1,
    paddingEnd: 40 },
  iconStyle: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.btnColors,
    marginStart: 7 },
});
