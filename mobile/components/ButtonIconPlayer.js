import React, { PureComponent } from 'react';
import { TouchableWithoutFeedback, Animated } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
    
class ButtonIconPlayer extends PureComponent {
  componentWillMount() {
    this.onPressIn = this.onPressIn.bind(this);
    this.onPressOut = this.onPressOut.bind(this);
    this.animatedValue = new Animated.Value(1);
  }
  onPressIn() {
    Animated.spring(this.animatedValue, {
      toValue: 0.8,
    }).start();
  }

  onPressOut() {
    Animated.spring(this.animatedValue, {
      toValue: 1,
      friction: 3,
      tension: 40,
    }).start();
  }
  render() {
    const { props } = this;
    return (
      <Animated.View style={[props.style, { padding: 20, margin: -20, transform: [{ scale: this.animatedValue }] }]}>
        <TouchableWithoutFeedback
          onPressIn={this.onPressIn}
          onPressOut={this.onPressOut}
          onPress={props.onPress} 
          activeOpacity={0.9} 
        > 
          {props.children ||
            <FontAwesome
              name={props.name} 
              color={props.color || 'white'}
              size={props.size || 25}
            />
          }
        </TouchableWithoutFeedback>
      </Animated.View>
    );
  }
}

export default ButtonIconPlayer;
