import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Image } from 'react-native';
import { connect } from 'react-redux';
import { Text, Row, Icon } from './commons';
import * as actions from '../actions';
import { Colors } from '../utils/constances';
    
class EventComponent extends Component {
  onNavigate=() => {
    this.props.navigate('EventDetail', { event: this.props.event });
  }
  render() {
    const { event, transparent } = this.props;
    const color = transparent ? 'white' : 'black';
    return (
      <TouchableOpacity onPress={this.onNavigate} style={styles.wrapp}>
        <Image
          style={{ alignSelf: 'center', width: 100, height: 80 }} source={!event.photo ? require('../assets/images/download.png') :
            { uri: event.photo }}
        />
        <View style={styles.wrappText}>
          <Text style={[styles.title, { color }]}>{event.title}</Text> 
          <View style={styles.flexEnd}>
            <Row style={styles.marginHor}>
              <Icon color={color} name={'md-calendar'} size={25} />
              <Text style={[styles.iconStyle, { color, fontSize: 14 }]}>{`${event.date} ${event.dateEnd ? ` - ${event.dateEnd}` : ''}`}</Text>
            </Row>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
export default connect(null, actions)(EventComponent);

const styles = StyleSheet.create({
  wrapp: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgba(255,255,255,.3)',
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 1,
    marginTop: 10, 
    padding: 10,
  },
  wrappText: { flex: 1, marginStart: 20 },
  flexEnd: { flex: 1, marginTop: 20, justifyContent: 'flex-end' },
  marginHor: { marginHorizontal: 10, alignItems: 'center' },
  title: { alignSelf: 'flex-start' },
  iconStyle: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.btnColors,
    marginHorizontal: 7, 
  }, 
});

