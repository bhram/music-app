import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { ImageBackground, View, StyleSheet } from 'react-native';

export const Container = (props) => {
  if (props.background) {
    return (
      <ImageBackground
        style={[styles.flex, props.style]} 
        source={props.background} 
        blurRadius={props.blurRadius}
        resizeMode={props.resizeMode}
      >
        <LinearGradient
          style={styles.LinearGradient}
          start={{ x: 0, y: 0.75 }} end={{ x: 1, y: 0.25 }} 
          colors={['#1d2671', '#c33764']}
        />
        {props.children}
      </ImageBackground>
    );
  }
  
  return (
    <View flex={styles.flex} >
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  flex: { flex: 1 },
  LinearGradient: {
    ...StyleSheet.absoluteFillObject,
    opacity: 0.7,
  },
});
