import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Colors } from '../utils/constances';
import { View, ButtonText } from '.';

export class Tabs extends Component {
  state = { tabs: [] }
  
  render() {
    return (
      <View flexDirection='row' style={styles.wrapp}>
        {this.props.tabs.map((tab) => (
          <ButtonText 
            key={tab.id}
            onPress={() => this.props.onSelect(tab.id)}
            style={styles.tab}
            containerStyle={[
              this.props.selectedId === tab.id && styles.tabSelected]}
          >{tab.name}</ButtonText>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapp: {
    justifyContent: 'space-between',
    marginTop: 15,
  },
  tab: { color: Colors.gray, paddingHorizontal: 10 },
  tabSelected: { 
    borderBottomColor: Colors.btnColors,
    paddingBottom: 5,
    borderBottomWidth: 3,
  },
});
