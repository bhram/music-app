import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import VideoComponent from './VideoComponent';

class VideosList extends PureComponent {
  render() {
    return (
      <FlatList
        contentContainerStyle={this.props.style}
        showsVerticalScrollIndicator={false}
        data={this.props.videos}
        ListFooterComponent={this.props.ListFooterComponent}
        refreshing={this.props.refreshing}
        onRefresh={this.props.onRefresh}
        onEndReachedThreshold={1}
        onEndReached={this.props.onEndReached}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <VideoComponent
            video={item}
            navigation={this.props.navigation}
            transparent={this.props.transparent}
          />
        )}
      />
    );
  }
}

export default VideosList;
