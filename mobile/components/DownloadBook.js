import React, { PureComponent } from 'react';
import { StyleSheet, Linking, PermissionsAndroid, Platform } from 'react-native';
import * as Progress from 'react-native-progress';
import RNFetchBlob from 'rn-fetch-blob';
import FileOpener from 'react-native-file-opener-fix';
import FileProvider from 'rn-fileprovider';
import { Colors, packageBundle } from '../utils/constances';
import { ButtonIcon } from './commons';
import { Client, INCREMENT_BOOK_DOWNLOAD } from '../graphql';

class DownloadBook extends PureComponent {
    state = { 
      path: '', 
      downloading: false,
      completed: false,
      progress: 0,
    }
    componentWillMount() {
      this.startDownloading = this.startDownloading.bind(this);     
      this.openFile = this.openFile.bind(this);

      let cacheFile;
      if (Platform.OS === 'android') {
        cacheFile = `${RNFetchBlob.fs.dirs.DownloadDir}/${this.props.book.id}.pdf`;
      } else {
        cacheFile = `${RNFetchBlob.fs.dirs.DocumentDir}/${this.props.book.id}.pdf`;
      }
      this.setState({ path: cacheFile });
      RNFetchBlob.fs
        .stat(cacheFile).then(() => {
          this.setState({ completed: true, downloading: false });
        });
    }
    async startDownloading() {
      if (Platform.OS === 'android') {
        const PERMISSIION = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
        const check = await PermissionsAndroid.check(PERMISSIION);
        if (!check) {
          const granted = await PermissionsAndroid.request(PERMISSIION);
          if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
            return;
          }
        }
      }
      Client.mutate({
        mutation: INCREMENT_BOOK_DOWNLOAD,
        variables: { bookId: this.props.book.id },
      });
      this.setState({ downloading: true });
      RNFetchBlob.config({
        path: this.state.path,
        overwrite: true,
      })
        .fetch('GET', this.props.book.url)
        .progress((received, total) => {
          this.setState({ progress: (received / total).toFixed(2) });
        })
        .then((res) => {
          RNFetchBlob.fs.stat(res.path())
            .then((stats) => console.log(stats));
          this.setState({ completed: true, downloading: false });
        });
    }
    async openFile() {
      if (Platform.OS === 'android') {
        const PERMISSIION = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
        const check = await PermissionsAndroid.check(PERMISSIION);
        if (!check) {
          const granted = await PermissionsAndroid.request(PERMISSIION);
          if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
            return;
          }
        }
        FileProvider.openFile(this.state.path, 'application/pdf', `${packageBundle}.provider`);
      } else {
        FileOpener.open(
          this.state.path,
          'application/pdf'
        );
        Linking.openURL(this.state.path);
      }
    }
    render() {
      if (this.state.downloading) {
        return (
          <Progress.Circle
            showsText
            formatText={() => `${(this.state.progress * 100).toFixed(0)}%`}
            size={50}
            progress={this.state.progress}
            color={Colors.btnColors}
            direction="counter-clockwise"
          />
        );
      }
      if (!this.state.completed) {
        return (
          <ButtonIcon
            onPress={this.startDownloading}
            style={styles.center} 
            name='md-download' color={Colors.btnColors} size={35}
          />
        );
      }
      return (
        <ButtonIcon
          onPress={this.openFile}
          style={styles.center} 
          name='md-open' color={Colors.btnColors} size={35}
        />
      );
    }
}
const styles = StyleSheet.create({
  center: { alignSelf: 'center' },
});
export default DownloadBook;
