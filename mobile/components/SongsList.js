import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import SongComponent from './SongComponent';

class SongsList extends PureComponent {
  render() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={this.props.songs}
        ListFooterComponent={this.props.ListFooterComponent}
        refreshing={this.props.refreshing}
        onRefresh={this.props.onRefresh}
        onEndReachedThreshold={1}
        onEndReached={this.props.onEndReached}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item, index }) => (
          <SongComponent
            song={item}
            index={index}
            isAnimted={this.props.isAnimted}
            transparent={this.props.transparent}
          />
        )}
      />
    );
  }
}

export default SongsList;
