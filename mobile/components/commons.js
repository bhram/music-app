import React from 'react';
import { TouchableOpacity, TouchableWithoutFeedback, SafeAreaView, View, ActivityIndicator, StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Button as ButtonElement, FormValidationMessage } from 'react-native-elements';
import * as actions from '../actions';
import { Colors, Fonts, isAndroid } from '../utils/constances';
import { isArabic, t } from '../locales/I18n';

export const Text = styled.Text`
    font-size:16px;
    font-weight:100;
    color:white;
    font-family:${props => (props.bold ? Fonts.BahijBold : Fonts.Bahij)};
`;

export const Row = styled.View`
    flex-direction:row;
`;

export const RowToolbar = styled(Row)`
  padding:16px;
`;

export const Toolbar = connect(null, actions)((props) => (
  <SafeAreaView>
    <View style={[styles.Toolbar, props.style]}>  
      {props.title &&
    <Text bold style={[styles.toolbarTitle, props.titleStyle]}>{props.title.toUpperCase()}</Text>}
      {
        props.onBack &&
      <BackIcon onPress={props.onBack} /> 
      }
      {props.onDrawer && 
    <DrawerIcon onPress={() => props.toggleDrawer()} />
      }
      {props.onSearch &&
    <ButtonIcon
      onPress={props.onSearch}
      name='ios-search' size={35} color='white'
    />
      }
    </View>
  </SafeAreaView>

));
export const CenterContainer = styled.View`
  align-items:center;
  justify-content:center;
`;

export const Button = (props) => (
  <View style={{ marginTop: 8 }}>
    <ButtonElement
      loading={props.loading}
      containerViewStyle={{ marginLeft: null, marginRight: null, width: '100%' }}
      buttonStyle={[{ height: 45, paddingHorizontal: 30 }, props.style]}
      title={!props.loading ? props.title : ''}
      icon={props.Icon} 
      onPress={props.onPress} 
      fontFamily={Fonts.Bahij}
      backgroundColor={props.color || Colors.btnColors}
        
    />
  </View>
);
  
export const ButtonText = (props) => (
  <TouchableOpacity onPress={props.onPress} style={props.containerStyle} activeOpacity={0.8} >
    <Text bold={props.bold} style={props.style}>{props.children}</Text>
  </TouchableOpacity>
);
export const ButtonIcon = (props) => (
  <TouchableOpacity onPress={props.onPress} activeOpacity={0.9} style={[styles.bigClick, props.style]}> 
    {props.children ?
      props.children : 
      <Ionicons name={props.name} color={props.color || 'white'} size={props.size || 25} />
    } 
  </TouchableOpacity>
);
export const ButtonIconFeedback = (props) => (
  <TouchableWithoutFeedback
    onPressIn={props.onPressIn}  
    onPressOut={props.onPressOut}  
    onPress={props.onPress}
    activeOpacity={0.9} style={[styles.bigClick, props.style]}
  > 
    {props.children ?
      props.children : 
      <Ionicons name={props.name} color={props.color || 'white'} size={props.size || 25} />
    } 
  </TouchableWithoutFeedback>
);

export const FormValiMsg = (props) => {
  if (props.msg) {
    return (
      <FormValidationMessage >{props.msg}</FormValidationMessage>      
    );
  }
  return null;
};

export const Icon = (props) => <Ionicons color={Colors.btnColors} {...props} />;

export const DrawerIcon = (props) => (
  <ButtonIcon
    onPress={props.onPress} name='ios-menu' size={35} color='white'
  />
);

export const Loading = () => (
  <ActivityIndicator color={Colors.btnColors} size='large' />
);

export const BackIcon = (props) => (
  <ButtonIcon
    onPress={props.onPress}
    style={[styles.rtl, props.style]}
    name='md-arrow-back' size={30}
  />
);

export const SeeMore = (props) => (
  <ButtonText onPress={props.onPress} style={styles.seeMore}>
    {t('seeMore')}
  </ButtonText>
);

export const IconPlay = (props) => (
  <View style={[styles.btnPlay]}>
    <FontAwesome name={'play'} color={props.color || 'white'} size={props.size || 20} />
  </View>
);
const styles = StyleSheet.create({
  Toolbar: { margin: !isAndroid ? 16 : 0, 
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16 },
  seeMore: { 
    color: Colors.btnColors,
    fontSize: 18,
    paddingVertical: 15,
    alignSelf: 'center', 
  },
  rtl: { transform: [{ scaleX: isArabic ? -1 : 1 }] },
  btnPlay:
  {
    width: 36, 
    height: 36,
    paddingStart: 3,
    borderRadius: 18, 
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center', 
    borderWidth: 3,
    backgroundColor: 'rgba(255,255,255,0.4)', 
  },
  toolbarTitle: { 
    position: 'absolute',
    right: 0,
    left: 0,
    fontSize: 18,
    textAlign: 'center' },
  bigClick: { padding: 20, margin: -20 },
});
