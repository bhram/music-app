import React, { PureComponent } from 'react';
import styled from 'styled-components/native';
import { Image, ActivityIndicator, TouchableOpacity, StyleSheet, Animated, Easing } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { View, Text } from '../components';
import { Colors } from '../utils/constances';
import * as actions from '../actions';
import RowIconsSong from './RowIconsSong';
import { isArabic } from '../locales/I18n';

const NUMBER = [0, 100, 200, 300, 400, 500];
const COLORS = ['rgba(66, 134, 241,1)', 'rgba(66, 20, 122,1)',
  'rgba(24, 74, 102,1)', 'rgba(255, 55, 33, 1)', 'rgba(122, 72, 28,1)', 'rgba(28, 122, 120, 1)'];
const Row = styled.View`
  flex-direction:row;
`;
class SongComponent extends PureComponent {
    state = { 
      isTransparent: false,
    }
    componentWillMount() {
      if (this.props.isAnimted) {
        this._animated = new Animated.Value(0);
      }
      this.switchSong = this.switchSong.bind(this);
      this.setState({ isTransparent: this.props.transparent }); 
    }
  
    componentDidMount() {
      this._animatedColor = new Animated.Value(0);
      this._animatedLeftOnPlay = new Animated.Value(0);
      // console.log(this.props.index);
      if (this._animated) {
        Animated.timing(this._animated, {
          toValue: 1,
          duration: 350,
          delay: this.props.index * 150,
          useNativeDriver: false,
        }).start();
      }
    }
    componentWillReceiveProps(props) {
      if (props.switch && (props.song.id === props.currentTrack.id) && props.transparent) {
        // alert('okk');
        this.animtedPlay();
      }
    }
    switchSong() {
      const { song } = this.props;
      if (song.id === this.props.currentTrack.id) {
        if (this.props.paused) {
          this.props.play();
        } else {
          this.props.pause();
        }
      } else {
        this.animtedPlay(() => {
          this.props.switchSong(song);
          this.animtedBorder();
        });
      }
    }
    animtedPlay(callback) {
      this._animated = null;
      Animated.sequence([
        Animated.timing(this._animatedLeftOnPlay, {
          toValue: 1,
          duration: 1000,
        }),
        Animated.timing(this._animatedLeftOnPlay, {
          toValue: 0,
          duration: 1000,
          easing: Easing.bounce,
        }),
      ]).start(callback);
    }
    animtedBorder() {
      Animated.sequence([
        Animated.timing(this._animatedColor, {
          toValue: 500,
          duration: 8000,
        }),
        Animated.timing(this._animatedColor, {
          toValue: 0,
          duration: 8000,
        }),
      ]).start(() => {
        if (this.props.song.id === this.props.currentTrack.id) { this.animtedBorder(); }
      });
    }
    renderButton() {
      const { song, transparent } = this.props;
      let icon = 'play';      
      if (this.props.currentTrack.id === song.id) {
        if (this.props.isBuffring) {
          return (
            <ActivityIndicator color={transparent ? 'white' : Colors.btnColors} />
          );
        }
        if (this.props.paused) {
          icon = 'pause';
        }
      }
      return (
        <TouchableOpacity
          style={styles.bigSpaceClick}
          onPress={this.switchSong} activeOpacity={0.9}
        >
          <FontAwesome
            name={icon} color={transparent ? 'white' : Colors.btnColors} size={20}
          />
        </TouchableOpacity>
      );
    }
    
    render() {
      const { song, transparent } = this.props;
      const rowStyles = this._animated ? {
        transform: [
          { translateX: this._animated.interpolate({
            inputRange: [0, 1],
            outputRange: [300, 0],
            extrapolate: 'clamp',
          }) },
        ],
      } : this._animatedLeftOnPlay && (song.id === this.props.currentTrack.id) && {
        transform: [
          { translateX: this._animatedLeftOnPlay.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 50],
          }),
          },
        ],
      };
      return (
        <Animated.View
          key={song.auther} style={[rowStyles,
            styles.boxSong,
            this._animatedColor && song.id === this.props.currentTrack.id && {
              borderRightColor: this._animatedColor.interpolate({
                inputRange: NUMBER,
                outputRange: COLORS,
              }),
              borderRightWidth: 2,
              borderLeftColor: this._animatedColor.interpolate({
                inputRange: NUMBER,
                outputRange: COLORS,
              }),
              borderLeftWidth: 2,
            }]}
        >
          <Row
            alignItems='center'
          >
            <Image
              source={{ uri: song.photo }}
              style={[styles.imageAvatar, !transparent && { borderColor: '#f1f1f1' }]}
            />
            <View style={styles.margin} flex={1}>
              <Text style={[styles.title, { color: transparent ? 'white' : 'black' }]}>
                {song[isArabic ? 'titleAr' : 'titleEn']}
              </Text>
              <Row
                style={styles.wrappIcons}
              >
                <Row >
                  <RowIconsSong 
                    color={transparent ? 'white' : Colors.btnColors}
                    song={song}
                  />
                </Row>
              </Row>
        
            </View>
          </Row>
          {this.renderButton()}
        </Animated.View>
      );
    }
}

const styles = StyleSheet.create({
  title: { flex: 1 },
  wrappIcons: {
    marginTop: 8,
  },
  boxSong: { 
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,.3)',
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 1,
    marginBottom: 5,
    paddingHorizontal: 16,
    paddingEnd: 30,
    paddingTop: 10,
    paddingBottom: 10,
  },
  imageAvatar: { 
    width: 75,
    height: 75,
    borderRadius: 35,
    borderColor: 'white',
    transform: [{ scaleX: isArabic ? -1 : 1 }],
    borderWidth: 2 },
    
  margin: { paddingStart: 20, paddingEnd: 50 },
  bigSpaceClick: { padding: 20, margin: -20 },
});

const mapStateToProps = ({ player }) => ({
  ...player,
});

export default connect(mapStateToProps, actions)(SongComponent);
