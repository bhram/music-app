import React, { PureComponent, Fragment } from 'react';
import { StyleSheet } from 'react-native';
import { Row, Icon, Text } from '../components';
import { Colors } from '../utils/constances';
import { convertTimeFormat, nFormatter } from '../utils';
    
class RowIconsSong extends PureComponent {
    state = { }
    render() {
      const { color, song } = this.props; 
      return (
        <Fragment>
          <Row style={styles.alignItems}>
            <Icon
              color={color}
              name={'md-heart'} size={25}
            />
            <Text style={[styles.iconStyle, { color }]}>{nFormatter(song.favsCount || 0)}</Text>
          </Row>
          <Row style={styles.marginHor}>
            <Icon color={color} name={'ios-headset-outline'} size={25} />
            <Text style={[styles.iconStyle, { color }]}>{nFormatter(song.listenCount || 0)}</Text>
          </Row>
          <Row style={styles.alignItems}>
            <Icon color={color} name={'ios-time-outline'} size={24} />
            <Text style={[styles.iconStyle, { color }]}>{convertTimeFormat(song.duration)}</Text>
          </Row>
        </Fragment>
      );
    }
}

const styles = StyleSheet.create({
  alignItems: { alignItems: 'center' },
  marginHor: { marginHorizontal: 10, alignItems: 'center' },
  wrappIcons: {
    paddingTop: 10,
    alignItems: 'center', 
  },
  iconStyle: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.btnColors,
    marginHorizontal: 7, 
  },
});
export default RowIconsSong;
