import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Placeholder from 'rn-placeholder';
import { Container } from './container';
import LineTransparent from './LineTransparent';

class SlideComponent extends PureComponent {
  render() {
    if (this.props.loading) {
      return (
        <Container >
          <Placeholder.Box
            height='100%'
            animate="fade"
            width="100%"
          />
          <LineTransparent />
        </Container>
      );
    }
    const { album } = this.props;
    return (
      <Container
        resizeMode={album.cover ? 'stretch' : 'cover'} 
        background={{ uri: album.cover ? album.cover : album.photo }}
      >
        <TouchableOpacity style={styles.flex} onPress={this.props.onPress} activeOpacity={0.9} />
        <LineTransparent />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  flex: { flex: 1 },
});

export default SlideComponent;
