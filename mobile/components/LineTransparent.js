import React from 'react';
import { StyleSheet, View } from 'react-native';
import { isArabic } from '../locales/I18n';
import { WIDTH } from '../utils/constances';

export default () => (
  <View style={[styles.lineTransparent, isArabic ? styles.lineLeft : styles.lineRight]} />

);

const styles = StyleSheet.create({
  lineTransparent: {
    width: 0,
    height: 0,     
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
    borderStyle: 'solid',
    borderTopWidth: WIDTH / 4.8,
    borderTopColor: 'white',
    transform: [{ rotate: '180deg' }], 
  },
  lineRight: {
    borderRightWidth: WIDTH,
    borderRightColor: 'transparent',
  },
  lineLeft: {
    borderLeftWidth: WIDTH,
    borderLeftColor: 'transparent',
  },
});
