import React, { PureComponent } from 'react';
import { ImageBackground, StyleSheet, Easing, Dimensions, TouchableOpacity, Animated } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import { connect } from 'react-redux';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { View, Row, Text, Container } from '../components';
import { convertTimeFormat } from '../utils';
import * as actions from '../actions';
import FullPlayer from '../screens/FullPlayerScreen';
import { isArabic } from '../locales/I18n';

const { height } = Dimensions.get('window');
class MiniPlayer extends PureComponent {
  state={
    visible: false,
  }
  componentWillMount() {
    this.animatedShow = this.animatedShow.bind(this);
    this.animatedValue = new Animated.Value(0);
    this.animatedModel = new Animated.Value(height);
  }
  componentWillReceiveProps(props) {
    if (props.currentTrack.id !== this.props.currentTrack.id) {
      this.setState({ visible: true }, () => {
        this.animatedShow(0);
      });
      this.animatedShowModel(true); 
    }
  }
  onCloseModel() {
    this.animatedShowModel(false, () => this.setState({ visible: false }, () => this.animatedShow(50)));
  }
  animatedShow(value, callback) {
    Animated.timing(this.animatedValue, {
      toValue: value,
      duration: 400,
      easing: Easing.bounce,
    }).start(callback);
  }
 
  animatedShowModel(isShow, callback) {
    Animated.timing(this.animatedModel, {
      toValue: isShow ? 0 : height,
      duration: 400,
      easing: Easing.ease,
    }).start(callback);
  }
  render() {
    const { currentTime, duration, currentTrack } = this.props;
    if (!currentTrack.id) {
      return null;
    }
    if (this.state.visible) {
      return (
        <Animated.View style={{ position: 'absolute', left: 0, right: 0, top: this.animatedModel, bottom: 0 }}>
          <SlidingUpPanel
            visible={this.state.visible}
            allowDragging
            allowMomentum
            onRequestClose={() => this.onCloseModel()}
            children={(dragHandlers) => (
              <View style={[{ flex: 1 }]}>
                <FullPlayer dragHandlers={dragHandlers} back={() => this.onCloseModel()} />
              </View>

            )}
          />
        </Animated.View>
      );
    }

    return (
      <Animated.View style={{ height: this.animatedValue }}>
        <TouchableOpacity
          activeOpacity={0.8} 
          onPress={() => this.animatedShow(0, () => 
            this.setState({ visible: true }, () => this.animatedShowModel(true)))} 
          style={styles.box}
        >
          <ImageBackground
            style={[styles.image, { alignItems: 'center', justifyContent: 'center' }]}
            source={{ uri: currentTrack.photo }}
          />
          <Container background={{ uri: currentTrack.photo }} blurRadius={4} flex={1}>
            <Row style={styles.height} >
              <Row flex={currentTime / duration} style={styles.progress} />
            </Row>
            <View style={styles.wrappText} justifyContent='center'>
              <Row style={styles.justifyContent}>
                <Text style={styles.txt}>
                  {currentTrack[isArabic ? 'titleAr' : 'titleEn']}
                </Text>
                <Row alignItems='center'>
                  <Text style={styles.txt}>
                    {convertTimeFormat(currentTime)}
                  </Text>
                  <TouchableOpacity
                    style={styles.bigCLick}
                    onPress={() => {
                      this.animatedShow(0, () => this.props.closeAudio());
                    }}
                  >
                    <Icon name='close' color='white' size={30} />
                  </TouchableOpacity>
                </Row>
              </Row>
            </View>
          </Container>
        </TouchableOpacity >    
      </Animated.View>  
    );
  }
}

const styles = StyleSheet.create({
  box: { height: 50,
    flexDirection: 'row', 
    borderTopColor: '#f1f1f1',
    borderTopWidth: 1,
  },
    
  image: { width: 50, height: 50, transform: [{ scaleX: isArabic ? -1 : 1 }] },
  height: { height: 3 },
  progress: { backgroundColor: 'red' },
  wrappText: { flex: 1, paddingHorizontal: 20 },
  justifyContent: { alignItems: 'center', justifyContent: 'space-between' },
  txt: { color: 'white' },
  modal: {
    backgroundColor: 'white',
    margin: 0,
    alignItems: undefined,
    justifyContent: undefined,
  },
  bigCLick: { padding: 20, margin: -20 },
});
const mapStateToProps = ({ player, progress }) => ({ ...player, ...progress });
export default connect(mapStateToProps, actions)(MiniPlayer);
