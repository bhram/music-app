import React, { PureComponent, Fragment } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import Slider from '../utils/Slider';

import { Text, View } from '../components';
import { Colors } from '../utils/constances';
import { convertTimeFormat } from '../utils';
import * as actions from '../actions';
import { isArabic } from '../locales/I18n';
    
class ProgressComponent extends PureComponent {
    state = { value: 0 }
    
    componentWillMount() {
      this.onSeek = this.onSeek.bind(this);
    }
    
    onSeek(value) {
      const seek = value * this.props.duration;
      this.props.sendPayload({ seek });
    }
    render() {
      const { currentTime, duration } = this.props;
      return (
        <Fragment>
          <Text>{convertTimeFormat(currentTime)}</Text>
          <View flex={1} style={styles.padding}>
            <Slider
              value={!duration ? 0 : currentTime / duration}
              onValueChange={this.onSeek}
              minimumTrackTintColor={Colors.btnColors}
              maximumTrackTintColor='#B7B7B7'
              thumbTintColor={'white'}
              style={isArabic && { transform: [{ rotate: '180deg' }] }}
            />
          </View>
          <Text>{convertTimeFormat(duration || 0)}</Text>
        </Fragment>
      );
    }
}

const styles = StyleSheet.create({
  padding: { paddingHorizontal: 8 },
});

const mapStateToProps = ({ progress }) => ({ ...progress });
export default connect(mapStateToProps, actions)(ProgressComponent);
