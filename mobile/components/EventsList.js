import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import EventComponent from './EventComponent';

class EventsList extends PureComponent {
  render() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={this.props.events}
        ListFooterComponent={this.props.ListFooterComponent}
        refreshing={this.props.refreshing}
        onRefresh={this.props.onRefresh}
        onEndReachedThreshold={1}
        onEndReached={this.props.onEndReached}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <EventComponent
            event={item}
            transparent={this.props.transparent}
          />
        )}
      />
    );
  }
}

export default EventsList;
