import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import BookComponent from './BookComponent';

class BooksList extends PureComponent {
  render() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={this.props.books}
        ListFooterComponent={this.props.ListFooterComponent}
        refreshing={this.props.refreshing}
        onRefresh={this.props.onRefresh}
        onEndReachedThreshold={1}
        onEndReached={this.props.onEndReached}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <BookComponent
            book={item}
            transparent={this.props.transparent}
          />
        )}
      />
    );
  }
}

export default BooksList;
