import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { View, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Text, BackIcon } from '.';
import { Colors, WIDTH, isAndroid } from '../utils/constances';
import { isArabic } from '../locales/I18n';

export class Header extends Component {
  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        resizeMode='cover' 
        source={{ uri: this.props.background }}
      >
        <LinearGradient
          style={styles.LinearGradient}
          start={{ x: 0, y: 0.75 }} end={{ x: 1, y: 0.25 }} 

          colors={['#1d2671', '#c33764']}
        />
        {this.props.back &&
        <BackIcon
          onPress={this.props.back}
          style={styles.toolbar}
        />
        }
        {this.props.toolbar && 
        <View style={styles.toolbar}>
          {this.props.toolbar}
        </View>}
        <View>
          {this.props.titleHeader && 
          <Text bold style={styles.titleHeader}>{this.props.titleHeader}</Text>
          }
        </View>
        
        <View style={[styles.lineTransparent, isArabic ? styles.lineLeft : styles.lineRight]} />
        {this.props.showPlayButton &&
        <TouchableOpacity
          onPress={this.props.showPlayButton}  
          activeOpacity={0.9} buttonStyle style={[styles.buttonPlay, isArabic ? { left: 10 } : { right: 10 }]}
        >
          <Icon
            style={styles.transform}
            name={this.props.play ? 'play' : 'pause'} color='white' size={25}
          />
        </TouchableOpacity>
        }
      </ImageBackground> 
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: { flex: 0.4, paddingTop: !isAndroid ? 16 : 0, width: null, height: null },
  titleHeader: { alignSelf: 'center', marginTop: 25, fontSize: 20, color: 'white' },
  lineTransparent: {
    width: 0,
    height: 0,     
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
    borderStyle: 'solid',
    borderTopWidth: WIDTH / 4.8,
    borderTopColor: 'white',
    transform: [{ rotate: '180deg' }], 
  },
  lineRight: {
    borderRightWidth: WIDTH,
    borderRightColor: 'transparent',
  },
  lineLeft: {
    borderLeftWidth: WIDTH,
    borderLeftColor: 'transparent',
  },
  toolbar: { 
    justifyContent: 'space-between',
    flexDirection: isArabic ? 'row-reverse' : 'row',
    margin: 2,
    padding: 24, 
  },

  LinearGradient: {
    ...StyleSheet.absoluteFillObject,
    opacity: 0.6,
  },
  buttonPlay: { 
    backgroundColor: Colors.btnColors,
    position: 'absolute',
    bottom: 50,
    paddingStart: 5,
    justifyContent: 'center',
    alignItems: 'center',
    width: 65,
    height: 65,
    borderRadius: 200 },
    
  transform: { transform: [{ scaleX: isArabic ? -1 : 1 }] },
});

