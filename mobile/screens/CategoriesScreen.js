import React, { Component } from 'react';
import { Image, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { Toolbar,
  Row, CenterContainer, Loading, Text, View, Icon,
} from '../components';
import WrappContainer from './WrappContainer';
import { t, isArabic } from '../locales/I18n';
import { Client, GET_CATEGORIES } from '../graphql';
import { Fonts } from '../utils/constances';

class Categories extends Component {
  state = {
    loading: true,
    categories: [],
  }
  async componentDidMount() {
    try {
      const { data } = await Client.query({ query: GET_CATEGORIES,
        variables: { type: 'audios' },
        fetchPolicy: 'network-only',
      });
      this.setState({ loading: false, categories: data.getCategories });
    } catch (error) {
      console.log(error);
    }
  }
  onSelectCategory(category) {
    this.props.navigation.navigate('Albums', { category });
  }
  renderList() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={this.state.categories}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => this.onSelectCategory(item)}
            activeOpacity={0.9}  
            style={styles.boxWrapp}
            key={item.id}
          >
            <Image
              style={styles.avatar}
              source={{ uri: item.photo }}
            />
            <View style={styles.margin}>
              <Text style={styles.title}>{item[isArabic ? 'titleAr' : 'titleEn']}</Text>
              <Row style={styles.wrappIcons}>
                <Row style={styles.iconMargin} alignItems='center' >
                  <Icon name={'ios-list'} size={25} color='white' />
                  <Text style={styles.iconStyle}>{item.albumsCount}</Text>
                </Row>
              </Row>
            </View>
          </TouchableOpacity>
        )} 
      />
    );
  }
  render() {
    return (
      <WrappContainer background={require('../assets/images/1.jpg')}>
        <Toolbar onDrawer title={t('categories')} />
        {this.state.loading ?
          <CenterContainer>
            <Loading />
          </CenterContainer>
          :
          <View style={{ padding: 20, flex: 1 }}>
            {this.renderList()}
          </View>
        }
        
      </WrappContainer>
         
    );
  }
}

const styles = StyleSheet.create({
  margin: { marginStart: 10 },
  boxWrapp: { 
    marginBottom: 10,
    backgroundColor: 'rgba(255,255,255,.3)',
    borderBottomColor: 'rgba(241,241,241,0.2)',
    borderBottomWidth: 1,
    // padding: 10, 
    alignItems: 'center',
    flexDirection: 'row',
  },
  avatar: { 
    width: 100,
    height: 85,
    borderWidth: 2,
    borderColor: 'white',
  },
  title: { marginStart: 10, fontSize: 16, marginBottom: 8, fontFamily: Fonts.Bahij },
  iconMargin: { marginHorizontal: 10 },
  wrappIcons: {
    paddingEnd: 40, 
  },
  iconStyle: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
    marginStart: 7,
  },
});

export default Categories;
