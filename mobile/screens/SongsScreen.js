import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Toolbar } from '../components';
import SongsList from '../components/SongsList';
import WrappContainer from './WrappContainer';
import { t } from '../locales/I18n';

const PER_PAGE = 25;
class SongsScreen extends Component {
  state = {
    page: 0,
    refreshing: true,
  }
  componentWillMount() {
    this.fetch = this.fetch.bind(this);
    this.fetch(true);
  }
  fetch(isRefresh) {
    const { page } = this.state;
    this.props.fetchSongs(isRefresh, {
      limit: PER_PAGE,
      offset: page * PER_PAGE,
    });
    this.setState({ page: page + 1 });
  }
  render() {
    return (
      <WrappContainer>
        <Toolbar
          onBack={() => this.props.navigation.goBack()} title={t('allaudios')}
          onSearch={() => this.props.navigation.navigate('SearchAudio')}
        />
        <SongsList
          refreshing={this.props.loading || false}
          onRefresh={() => this.setState({ page: 0 }, () => this.fetch(true))}
          onEndReached={() => this.fetch(false)}
          songs={this.props.songs} transparent
        />
      </WrappContainer>
    );
  }
}
const mapStateToProps = ({ songs }) => ({ ...songs });
export default connect(mapStateToProps, actions)(SongsScreen);
