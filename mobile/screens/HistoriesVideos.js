import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Toolbar, CenterContainer, Text } from '../components';
import VideosList from '../components/VideosList';
import WrappContainer from './WrappContainer';
import { t } from '../locales/I18n';

class History extends Component {
  render() {
    return (
      <WrappContainer background={require('../assets/images/2.jpg')}>
        <Toolbar
          title={t('historyVideos')}
          onDrawer
        />
        {this.props.videos.length ? 
          <VideosList
            navigation={this.props.navigation}
            videos={this.props.videos} transparent
          />
          :
          <CenterContainer flex={1}>
            <Text>{t('empty')}</Text> 
          </CenterContainer>
        }
      </WrappContainer>
    );
  }
}
const mapStateToProps = ({ historyVideos }) => ({ ...historyVideos });
export default connect(mapStateToProps)(History);
