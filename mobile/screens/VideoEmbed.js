import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import YouTube from 'react-native-youtube';
import PopupDialog from 'react-native-popup-dialog';
import * as actions from '../actions';
import VideosList from '../components/VideosList';
import WrappContainer from './WrappContainer';
import { Toolbar } from '../components';

class VideoEmbed extends PureComponent {
  navigateSearch=() => {
    this.props.hideVideo();
    this.props.navigate('SearchVideo');
  }
  render() {
    const { video } = this.props;
    if (!video) {
      return null;
    }
    return (
      <PopupDialog
        show={this.props.isShow}
        height={1}
        onDismissed={() => this.props.hideVideo()}
      >
        <WrappContainer>
          <Toolbar
            onBack={() => this.props.hideVideo()} 
            title={video.title}
            onSearch={this.navigateSearch}
          />

          <View style={{ flex: 0.63 }} >
            {this.props.isShow &&
          <YouTube
            apiKey="AIzaSyAUxViTtX_I1z206-YNLfL0hPui4Q6c7bY"
            videoId={video.videoId} 
            play 
            loop
            style={{ alignSelf: 'stretch', flex: 1 }}
          />
            }
          </View>
          <View style={{ flex: 1 }}>
            <VideosList
              videos={this.props.videos} 
              transparent
            />
          </View>
        </WrappContainer>
      </PopupDialog>
    );
  }
}

export default connect(({ videos }) => ({ ...videos }), actions)(VideoEmbed);
