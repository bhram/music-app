import React, { Component } from 'react';
import { connect } from 'react-redux';
import WrappContainer from './WrappContainer';
import { Form, Toolbar, View, Text, Loading } from '../components';
import { t } from '../locales/I18n';
import SongsList from '../components/SongsList';
import { Client, SEARCH_SONGS } from '../graphql';

class SearchAudioScreen extends Component {
    state = { 
      loading: false,
      songs: undefined,
      query: '',
    }
    componentDidMount() {
      this.onSearch = this.onSearch.bind(this);
      this.renderList = this.renderList.bind(this);
    }
    async onSearch() {
      try {
        if (this.state.query === '') { return; }
        this.setState({ loading: true });
        const { data } = await Client.query({
          query: SEARCH_SONGS,
          variables: {
            query: this.state.query,
          },
          fetchPolicy: 'network-only',
        });
        this.setState({ songs: data.searchSongs, loading: false });
      } catch (error) {
        this.setState({ songs: [], loading: false });
        console.log(error);
      }
    }
    renderList() {
      if (typeof this.state.songs === 'undefined') {
        return (
          <SongsList 
            songs={this.props.songs}
            transparent
          />
        );
      }
      if (typeof this.state.songs === 'object' && this.state.songs.length) {
        return (
          <SongsList 
            songs={this.state.songs}
            transparent
          />
        );
      }
      if (this.state.loading) {
        return (
          <View flex={1} alignItems='center' justifyContent='center' >
            <Loading />
          </View>
        );
      }
      return (
        <View flex={1} alignItems='center' justifyContent='center' >
          <Text bold >{t('noSearchResult')}</Text>
        </View>
      );
    }
    render() {
      return (
        <WrappContainer background={require('../assets/images/1.jpg')} >
          <Toolbar onBack={() => this.props.navigation.goBack()} title={t('search')} />
          <Form
            placeholder={t('searchAudio')}
            onChangeText={(tt) => this.setState({ query: tt })}
            returnKeyType={'search'}
            onSubmitEditing={this.onSearch}
          />
          {this.renderList()}

        </WrappContainer>
      );
    }
}

export default connect(({ songs }) => ({ ...songs }))(SearchAudioScreen);

