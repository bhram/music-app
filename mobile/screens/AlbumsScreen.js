import React, { PureComponent } from 'react';
import { ActivityIndicator, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Header, View, CenterContainer, Text } from '../components';
import { Colors } from '../utils/constances';
import SongComponent from '../components/SongComponent';
import { isArabic } from '../locales/I18n';

class Album extends PureComponent {
  state = {
    album: {},
  }
  componentWillMount() {
    this.isPlaying = this.isPlaying.bind(this);
    this.onPressButtonAlbum = this.onPressButtonAlbum.bind(this);
    this.setState({ album: this.props.navigation.getParam('album') });
  }
  async componentDidMount() {
    try {
      this.props.fetchSongs(true, { albumId: this.state.album.id });
    } catch (error) {
      console.log(error);
    }
  }

  onPressButtonAlbum() {
    // this.props.navigation.navigate('FullPlayer');
    const { currentTrack, songs, paused } = this.props;
    
    if ((!currentTrack.id || (this.state.album.id !== currentTrack.albumId))) {
      this.props.switchSong({
        ...songs[0],
        avatar: this.state.album.avatar,
        albumId: this.state.album.id });
      return;
    }
    if (paused) {
      this.props.play();
    } else {
      this.props.pause();
    }
  }
  isPlaying() {
    if (this.props.currentTrack.albumId !== this.state.album.id) {
      return true;
    }
    return !this.props.paused;
  }

  render() {
    const { songs } = this.props;
    const { album } = this.state;
    return (
      <View style={styles.container}>
        <Header 
          back={() => this.props.navigation.goBack()}
          background={album.cover ? album.cover : album.photo} 
          titleHeader={album[isArabic ? 'titleAr' : 'titleEn']}
          showPlayButton={this.onPressButtonAlbum}
          play={this.isPlaying()}
        />
        <View flex={0.6}>
          {!songs.length ? 
            <CenterContainer >
              {this.props.loading ?
                <ActivityIndicator size='small' color={Colors.btnColors} />
                :
                <Text style={{ color: Colors.gray }}>No Audios In this Album</Text>
              }
            </CenterContainer>
            :
            <FlatList
              showsVerticalScrollIndicator={false}
              data={songs}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <SongComponent
                  song={item}
                />
              )}
            />
          
          }
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: 'white' },
});
function mapStateToProps({ songs, player }) {
  return { ...songs, ...player };
}
export default connect(mapStateToProps, actions)(Album);
