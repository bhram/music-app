import React, { Component } from 'react';
import { StyleSheet, Keyboard, AsyncStorage, Alert } from 'react-native';
import { graphql, compose } from 'react-apollo';
import { Text, View, Form, ButtonText, Button, FormValiMsg, CenterContainer, Loading, Row, Toolbar } from '../components';
import { Colors } from '../utils/constances';
import { LOGIN, Client } from '../graphql';
import { signIn, getToken } from '../utils/tokens';
import { ResetActionNavigation } from '../navigations/AppNaigations';
import WrappContainer from './WrappContainer';
import { t } from '../locales/I18n';

class LoginScreen extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = { username: '', password: '', errors: {}, init: true, loading: false };
  }
  
  async componentWillMount() {
    this.handlerLogin = this.handlerLogin.bind(this);
    this.skipLogin = this.skipLogin.bind(this);
    this.navigate = this.props.navigation.getParam('navigate', false);
    if (!this.navigate) {
      const token = await getToken();
      if (token) {
        this.openHome();
        return;
      }
      const skip = await AsyncStorage.getItem('skip');
      if (skip) {
        this.openHome();
        return;
      }
    }
     
    this.setState({ init: false });
  }
    
  async handlerLogin() {
    Keyboard.dismiss();
    const { username, password, loading } = this.state;
    console.log(username);
    if (loading) { return; }
    if (!(username.trim().length > 2)) {
      this.setState({ errors: { username: t('userMustMoreThan3') } });
      return;
    }
    if (!(password.trim().length > 5)) {
      this.setState({ errors: { password: t('passwordMustMoreThan6') } });
      return; 
    }
    try {
      this.setState({ loading: true, errors: {} });
      const { data } = await this.props.login({
        variables: {
          username,
          password,
        },
      });
      await signIn(data.login.token);
      await Client.resetStore();
      this.openHome();
    } catch (error) {
      if (error.graphQLErrors[0]) {
        Alert.alert(t('notLogin'));
        this.setState({ errors: { username: t('notLogin') } });
        this.password.clear();
      }
      this.setState({ loading: false });
    }
  }
  async skipLogin() {
    await AsyncStorage.setItem('skip', 'true');
    this.openHome();
  }
  openHome() {
    this.props.navigation.dispatch(ResetActionNavigation({ routeName: 'Main' }));
  }
  render() {
    if (this.state.init) {
      return (
        <WrappContainer startColor>
          <CenterContainer flex={1}>
            <Loading />
          </CenterContainer>
        </WrappContainer>
      );
    }
    return (
      <WrappContainer startColor blurRadius={0}>
        <View flex={1} style={{ paddingTop: 20 }}>
          <Toolbar
            titleStyle={{ fontSize: 26 }}
            title={t('signIn')} onBack={this.navigate ? 
              () => this.props.navigation.goBack() : false}
          />
        </View>
        <View flex={1.5} >
          <Form
            icon='md-person'
            placeholder={t('usernameOrEmail')}
            onChangeText={(tt) => this.setState({ username: tt })}
            returnKeyType={'next'}
            blurOnSubmit={false}
            onSubmitEditing={() => { this.password.focus(); }}              
          />
          <FormValiMsg msg={this.state.errors.username} />                    
          <Form
            icon='md-lock'
            ref={(r) => { this.password = r; }}
            placeholder={t('password')}
            secureTextEntry
            onChangeText={(tt) => this.setState({ password: tt })}              
          />
          <FormValiMsg msg={this.state.errors.password} />                    
          <Row justifyContent="flex-end" marginHorizontal={5} marginTop={2}>
            <ButtonText 
              onPress={() => this.props.navigation.navigate('ForgatePassword')}
            >{t('fpassword')}</ButtonText>
          </Row>
          <View style={{ paddingVertical: 20, justifyContent: 'flex-end', flex: 1 }}>
            <Row style={[styles.margin]}>
              <View flex={1}>
                <Button
                  loading={this.state.loading}
                  onPress={this.handlerLogin}
                  title={t('login')} color={Colors.btnColors}
                />
              </View>
              {!this.navigate &&
              <View flex={1}>
                <Button
                  onPress={this.skipLogin}
                  title={t('skip')} color={'#2f2f4c'}
                />
              </View>
              }
            </Row>
            <View style={styles.siwtchText}>

              <Row>
                <Text style={{ color: '#ccc' }} >{t('dontHavAccount')} </Text>
            
                <ButtonText 
                  onPress={() => this.props.navigation.navigate('Register')}
                >
                  {t('signUp')}
                </ButtonText>
              </Row>
            </View>
          </View>
        </View>
      </WrappContainer>
    );
  }
}

const styles = StyleSheet.create({
  margin: { marginTop: 15 },
  padding: { paddingTop: 10, paddingHorizontal: 16, alignSelf: 'flex-end' },
  siwtchText: { marginTop: 15, alignItems: 'center', justifyContent: 'center' },
});

export default compose(
  graphql(LOGIN, { name: 'login' }),
)(LoginScreen);

