import React, { Component } from 'react';
import { connect } from 'react-redux';
import Video from 'react-native-video';
import * as actions from '../actions';
import { isAndroid } from '../utils/constances';

class PlayerComponent extends Component {
  state ={
    url: '',
  }
  
  componentWillMount() {
    this.loadStart = this.loadStart.bind(this);
    this.onTimedMetadata = this.loadStart.bind(this);
    this.setDuration = this.loadStart.bind(this);
    this.setTime = this.setTime.bind(this);
    this.onEnd = this.onEnd.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.seek === null) {
      return; 
    }
    if (nextProps.seek !== this.props.seek) {
      this.props.sendPayload({ seek: null });      
      this.player.seek(nextProps.seek);
    }
  }
  onEnd() {
    this.props.runShuffle();
  }
  get song() {
    const { currentTrack } = this.props;
    return currentTrack || {};
  }
  setDuration() {
  }
  setTime(e) {
    this.props.changeCurrentTime(e.currentTime);
    if (!isAndroid) {
      if (this.props.repeat) {
        if (e.currentTime >= (this.state.duration - 0.5)) {
          this.player.seek(0);
        } 
      }
    }
  }
  
  loadStart(e) {
    if (e.duration) {
      this.setState({ duration: e.duration });
      this.props.sendPayload({ paused: true, isBuffring: false, duration: e.duration });
    } else {
      this.props.sendPayload({ isBuffring: true });
    }
  }

  render() {
    if (!this.song.url) {
      return null;
    }
    return (
      <Video
        source={{ uri: this.song.url }} // Can be a URL or a local file.
        ref={(ref) => {
          this.player = ref;
        }} 
        paused={!this.props.paused}
        playInBackground 
        ignoreSilentSwitch={'ignore'} 
        playWhenInactive 
        audioOnly
        onFullscreenPlayerWillPresent={this.fullScreenPlayerWillPresent} // Callback before fullscreen starts
        onFullscreenPlayerDidPresent={this.fullScreenPlayerDidPresent} // Callback after fullscreen started
        onFullscreenPlayerWillDismiss={this.fullScreenPlayerWillDismiss} // Callback before fullscreen stops
        onFullscreenPlayerDidDismiss={this.fullScreenPlayerDidDismiss} // Callback after fullscreen stopped
        onEnd={this.onEnd}
        onLoadStart={this.loadStart} // Callback when video starts to load
        onLoad={this.setDuration} // Callback when video loads
        onProgress={this.setTime} // Callback every ~250ms with currentTime
        repeat={this.props.repeat}
        progressUpdateInterval={1000}        
      />
    );
  }
}

const mapStateToProps = ({ player }) => ({ ...player });
export default connect(mapStateToProps, actions)(PlayerComponent);
