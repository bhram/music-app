import React, { Component } from 'react';
import { Image, ScrollView, Dimensions } from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';
import { Text, View, Toolbar, Row } from '../components';
import { t } from '../locales/I18n';
import WrappContainer from './WrappContainer';

class EventDetails extends Component {
    state={
      event: this.props.navigation.getParam('event'),
    }
    render() {
      const { event } = this.state;
      const { width } = Dimensions.get('window');
      return (
        <WrappContainer background={require('../assets/images/6.jpg')}>
          <Toolbar title={t('eventDetails')} onBack={() => this.props.navigation.goBack()} />
          <ScrollView>
            {/* <Image style={{ width: null, height: 300 }} source={{ uri: event.photo }} /> */}
            <ImageZoom
              cropWidth={width}
              cropHeight={300}
              imageWidth={width}
              imageHeight={300}
            >
              <Image
                style={{ width, height: 300 }}
                source={{ uri: event.photo }}
              />
            </ImageZoom>
            <View style={{ padding: 16 }}>
              <Text >
                {event.title}
              </Text>
              <Row style={{ paddingTop: 10, marginTop: 10, borderTopColor: '#f1f1f1', borderTopWidth: 1 }}>
                <Text bold style={{ marginRight: 5 }} >{t('startDate')} :</Text>
                <Text>{event.date}</Text>
                {event.endDate &&
                <View style={{ marginTop: 10 }}>
                  <Text bold style={{ marginRight: 5 }} >{t('endDate')} :</Text>
                  <Text>{event.endDate}</Text>
                </View>
                }
              </Row>
            </View>
          </ScrollView>
        </WrappContainer>
      );
    }
}

export default EventDetails;
