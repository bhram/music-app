import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, Dimensions,SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import Carousel from 'react-native-snap-carousel';
import { Toolbar,
  Icon, Text, Row, CenterContainer, Loading, View, 
} from '../components';
import * as actions from '../actions';
import WrappContainer from './WrappContainer';
import { isArabic } from '../locales/I18n';
import { nFormatter } from '../utils';

class AllAlbumsScreen extends Component {
  state ={
    category: {},
  }
  
  componentWillMount() {
    this.onSelectAlbum = this.onSelectAlbum.bind(this);
    this._renderItem = this._renderItem.bind(this);
    const category = this.props.navigation.getParam('category');
    this.setState({ category });
  }
  
  componentDidMount() {
    this.props.fetchAlbums(this.state.category.id);
  }
  onSelectAlbum(album) {
    this.props.navigation.navigate('Album', { album });
  }
  _renderItem({ item }) {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => this.onSelectAlbum(item)}
        key={item.id} style={styles.container}
      >
        <View style={{ flex: 0.8 }}>
          <Image source={{ uri: item.photo }} style={{ ...StyleSheet.absoluteFillObject }} />
        </View>
        <View style={styles.box}>
          <Text style={styles.title}>{item[isArabic ? 'titleAr' : 'titleEn']}</Text>
          <Row style={styles.wrappIcons}>
            <Row style={styles.iconMargin} alignItems='center' >
              <Icon name={'ios-headset-outline'} size={25} color='gray' />
              <Text style={styles.iconStyle}>{nFormatter(item.listenCount || 0)}</Text>
            </Row>
            <Row style={styles.iconMargin} alignItems='center' >
              <Icon name={'ios-list-outline'} size={25} color='gray' />
              <Text style={styles.iconStyle}>{item.songCount}</Text>
            </Row>
          </Row>
        </View>
      </TouchableOpacity>
    );
  }
  renderList() {
    return (
      <Carousel
        firstItem={Math.round(this.props.albums.length / 2) - 1}
        data={this.props.albums}
        renderItem={this._renderItem}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width - 100}
      />

    );
  }
  render() {
    return (
      <WrappContainer background={require('../assets/images/4.jpg')}>
        <Toolbar onBack={() => this.props.navigation.goBack()} title={this.state.category[isArabic ? 'titleAr' : 'titleEn']} />
        {this.props.loading ?
          <CenterContainer>
            <Loading />
          </CenterContainer>
          :
          <View style={{ flex: 1, marginTop: 40 }}>
            {this.renderList()}
          </View>
        }
        
      </WrappContainer>
         
    );
  }
}

const styles = StyleSheet.create({
  container: { height: '80%', width: '100%', elevation: 7, backgroundColor: 'white', alignSelf: 'center' },
  image: { height: null, width: null },
  title: { color: 'gray', marginStart: 10, fontSize: 16, marginBottom: 8 },
  iconMargin: { marginHorizontal: 10 },
  wrappIcons: {
    justifyContent: 'center',
  },
  iconStyle: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: 'gray',
    marginStart: 7,
  },
  box: { alignItems: 'center', flex: 0.2, justifyContent: 'space-evenly' },
});
export const mapStateToProps = ({ albums }) => ({ ...albums });
export default connect(mapStateToProps, actions)(AllAlbumsScreen);
