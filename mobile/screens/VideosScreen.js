import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Toolbar } from '../components';
import VideosList from '../components/VideosList';
import WrappContainer from './WrappContainer';
import { isArabic } from '../locales/I18n';

const PER_PAGE = 25;
class VideosScreen extends Component {
  state = {
    page: 0,
    refreshing: true,
    category: {},
  }
  componentWillMount() {
    this.fetch = this.fetch.bind(this);
    this.setState({ category: this.props.navigation.getParam('category') }, () => {
      this.fetch(true);
    });
  }
  fetch(isRefresh) {
    const { page } = this.state;
    this.props.fetchVideos(isRefresh, {
      limit: PER_PAGE,
      offset: page * PER_PAGE,
      categoryId: this.state.category.id,
    });
    this.setState({ page: page + 1 });
  }
  render() {
    return (
      <WrappContainer>
        <Toolbar
          onBack={() => this.props.navigation.goBack()} 
          title={this.state.category[isArabic ? 'titleAr' : 'titleEn']}
          onSearch={() => this.props.navigation.navigate('SearchVideo')}
        />
        <VideosList
          style={{ paddingHorizontal: 10 }}
          refreshing={this.props.loading || false}
          onRefresh={() => this.setState({ page: 0 }, () => this.fetch(true))}
          onEndReached={() => this.fetch(false)}
          videos={this.props.videos} 
          navigation={this.props.navigation}
          transparent
        />
      </WrappContainer>
    );
  }
}
const mapStateToProps = ({ videos }) => ({ ...videos });
export default connect(mapStateToProps, actions)(VideosScreen);
