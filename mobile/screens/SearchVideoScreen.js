import React, { Component } from 'react';
import { connect } from 'react-redux';
import WrappContainer from './WrappContainer';
import { Form, Toolbar, Loading, Text, View } from '../components';
import { t } from '../locales/I18n';
import VideosList from '../components/VideosList';
import { Client, SEARCH_VIDEOS } from '../graphql';

class SearchVideoScreen extends Component {
    state = { 
      videos: undefined,
      query: '',
    }
    componentDidMount() {
      this.onSearch = this.onSearch.bind(this);
    }
    async onSearch() {
      try {
        if (this.state.query === '') { return; }
        this.setState({ loading: true });
        const { data } = await Client.query({
          query: SEARCH_VIDEOS,
          variables: {
            query: this.state.query,
          },
          fetchPolicy: 'network-only',
        });
        this.setState({ videos: data.searchVideos, loading: false });
      } catch (error) {
        this.setState({ videos: [], loading: false });
        console.log(error);
      }
    }
    renderList() {
      if (typeof this.state.videos === 'undefined') {
        return (
          <VideosList 
            videos={this.props.videos}
            transparent
          />
        );
      }
      if (typeof this.state.videos === 'object' && this.state.videos.length) {
        return (
          <VideosList 
            videos={this.state.videos}
            transparent
          />
        );
      }
      if (this.state.loading) {
        return (
          <View flex={1} alignItems='center' justifyContent='center' >
            <Loading />
          </View>
        );
      }
      return (
        <View flex={1} alignItems='center' justifyContent='center' >
          <Text bold >{t('noSearchResult')}</Text>
        </View>
      );
    }
    render() {
      return (
        <WrappContainer background={require('../assets/images/2.jpg')} >
          <Toolbar onBack={() => this.props.navigation.goBack()} title={t('search')} />
          <Form
            placeholder={t('searchVideo')}
            onChangeText={(tt) => this.setState({ query: tt })}
            returnKeyType={'search'}
            onSubmitEditing={this.onSearch}
          />
          {this.renderList()}
        </WrappContainer>
      );
    }
}

export default connect(({ videos }) => ({ ...videos }))(SearchVideoScreen);
