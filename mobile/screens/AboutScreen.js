/* eslint-disable max-len */
import React, { Component } from 'react';
import { Image, StyleSheet, TouchableOpacity, Linking, ScrollView } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Text, Toolbar, View } from '../components';
import { t } from '../locales/I18n';
import WrappContainer from './WrappContainer';

class AboutScreen extends Component {
    state = { 
      ar: `هو ابن الرادود والشاعر والممثل الحسيني الحاج أحمد الأكرف.

      بدأ مسيرته في الثامنة من عمره، عندما انتسب لفرقة إنشادٍ كوراليةٍ في البحرين تابعة لمشروع التعليم الديني في قريته الدراز.
      
      انتقل عام 1984 في خطٍّ موازٍ للإنشاد إلى خدمة مواكب اللطم رادوداً حسينياً، ثمَّ منشداً أساسياً في فرقة الإمام الصادق عليه السلام في عمر الحادية عشرة.
      
      عام 1987 تأسست فرقة "قوافل كربلاء" التي تضمُّ نخبة من كبار الرواديد، وبعد بروزه في المواكب والإنشاد أصبح الشيخ الأكرف واحداً من أفرادها الأساسيين.
      
      حرص منذ تلك البدايات على أن تكون مشاركاته نوعية، وأن تعيش قصائده في الذاكرة، وتحمل قضايا الإنسان الإسلامية والعقدية والاجتماعية والسياسية.
      
      في عمر السادسة عشرة سنة، توجه إلى النجف الأشرف لدراسة العلوم الإسلامية. فأخذت الدراسة منه مأخذاً ولكنَّه لم ينقطع عن الإنشاد واللطم. 
      
      انتقل عام 1991 إلى قم المقدسة لاستكمال الدراسة الحوزوية وحضر عند كبار العلماء في العلوم الإسلامية المختلفة. وهناك أيضاً تعرف على مدارس الرثاء المتنوعة، فأخذ التجربة البحرينية والعراقية والإيرانية وأسس بعد حين مدرسة خاصة عرفت بها البحرين وأصبحت تشكل الهوية الأبرز لمدارس اللطميات فيها حتى انتشرت في دول الخليج.
      
      بعد سنتين وفي فترة الإجازة عاد إلى البحرين في أيام عاشوراء وشارك في الموكب المركزي بالعاصمة المنامة والذي لا يشارك فيه إلا النخبة من الرواديد. 
      
      في بداية عام 1995 عاد من قم المقدسة وتعرض للملاحقة من قبل النظام الحاكم، بسبب التأثير السياسي الكبير للطميات التي يرددها، وسجن لمدة ستة أشهر ليُعاد اعتقاله بعد شهرين ويقضي 4 سنوات ثم وبعد إطلاق سراحه لوحق لمدة سنة. 
      
      بعد انقضاء هذه الفترة عمل في التدريس، والتبليغ، والكتابة والتلحين والمشاركة في المواكب الحسينية.
      
      في مطلع عام 2002 بدأ نشاطه في استديوهات الكويت، ومنها بدأت أعماله تنتشر في الخليج وسوريا ولبنان والعراق.
      
      ففي الكويت ساهم في تأسيس معهد الامام الحسين للترتيل والإنشاد وقدم فيه فقه الفن وفقه الأداء، إلى جانب كونه أستاذاً في حوزة الإمام المجتبى عليه السلام بالرميثية.
      
      درب الكثير من الرواديد في كل من البحرين، الكويت، ولبنان، حتى أصبح صاحب رأي يرجع إليه في فن الإنشاد الإسلامي والحسيني.
      شارك في إحياء المجالس الحسينية في مختلف الدول العربية والإسلامية.
      
      لديه أرشيف ومكتبة ضخمة من الأعمال الحسينية والإنشادية الصوتية والمرئية مما يقصر المقام عن تعداده.
      
      اليوم يقدِّم الشيخ حسين الأكرف في الساحة الفنية والثقافية الإسلامية خدمات عديدة ومنها:
      
      -	مستشار شرعي في فن الإنشاد. 
      -	مدرب يبني الرواديد. 
      -	ملحن. 
      - محاضر ومدرّس في المواد الإسلامية المختلفة.
      `.split('\n').map((m) => m.trim()).join('\n'),
    }
    componentWillMount() {
      this.openUrl = this.openUrl.bind(this);
    }
    openUrl(url) {
      Linking.openURL(url);
    }
    render() {
      return (
        <WrappContainer background={require('../assets/images/2.jpg')}>
          <Toolbar onDrawer title={t('about')} />
          <Image
            source={require('../assets/images/logo.jpg')} 
            style={styles.avatar}
          />
          
          <View style={styles.scualContainer}>
            <TouchableOpacity onPress={() => this.openUrl('https://www.facebook.com/HussainAlakraf.ofc')} style={[styles.scauilIcon, { backgroundColor: '#3b5998' }]}>
              <FontAwesome name='facebook-f' color='white' size={20} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.openUrl('https://twitter.com/alakraf_ofc')} style={[styles.scauilIcon, { backgroundColor: '#1DA1F3' }]}>
              <FontAwesome name='twitter' color='white' size={20} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.openUrl('https://www.instagram.com/alakraf_ofc')} style={[styles.scauilIcon, { backgroundColor: '#8a3ab9' }]}>
              <FontAwesome name='instagram' color='white' size={20} />
            </TouchableOpacity>
          </View>
          <View style={styles.wrappText} >
            <ScrollView showsVerticalScrollIndicator={false} >
              <Text style={styles.desc} >{this.state.ar}</Text>
            </ScrollView>
          </View>
        </WrappContainer>
      );
    }
}

const styles = StyleSheet.create({
  scualContainer: { position: 'absolute', zIndex: 1000, right: 20, top: 20 },
  scauilIcon: {
    alignItems: 'center', 
    justifyContent: 'center',
    marginBottom: 6,
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
  },
  wrappText: {
    borderRadius: 5,
    flex: 1,
    margin: 16, 
    marginTop: 0,
    padding: 16,
    backgroundColor: 'white' },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: 'white',
    zIndex: 1000,
    transform: [{ translateY: 20 }],
  },
  desc:
    { textAlign: 'left', marginTop: 10, color: '#000' },
});
export default AboutScreen;
