import React, { PureComponent } from 'react';
import { StyleSheet, Animated, FlatList, Share } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Container, ButtonIcon, View, Text, Row, BackIcon, RowToolbar } from '../components';
import SongComponent from '../components/SongComponent';
import ProgressComponent from '../components/ProgressComponent';
import { isArabic } from '../locales/I18n';
import { isAndroid } from '../utils/constances';
import ButtonIconPlayer from '../components/ButtonIconPlayer';

const COLOR_GRAY = 'rgba(255, 255, 255, 0.5)';
class FullPlayerScreen extends PureComponent {
  state={
    value: 0.4,
  } 
  
  componentWillMount() {
    this.ontogglePlay = this.ontogglePlay.bind(this);
    this.backWard = this.backWard.bind(this);
    this.forWard = this.forWard.bind(this);
    this.onRepeat = this.onRepeat.bind(this);
    this.onShuffle = this.onShuffle.bind(this);
    this.addToFav = this.addToFav.bind(this);
    this.close = this.close.bind(this);
    this._animatedLeftOnPlay = new Animated.Value(0);
    this._animatedHeart = new Animated.Value(0);
  }
  
  componentWillReceiveProps(props) {
    if (props.switch) {
      Animated.sequence([
        Animated.timing(this._animatedLeftOnPlay, {
          toValue: 1,
          duration: 1000,
        }),
        Animated.timing(this._animatedLeftOnPlay, {
          toValue: 0,
          duration: 1000,
        }),
      ]).start();
    }
  }
  ontogglePlay() {
    if (this.props.paused) {
      this.props.play();
    } else {
      this.props.pause();
    }
  }
  onRepeat() {
    this.props.sendPayload({ repeat: !this.props.repeat, shuffle: false });
  }
  onShuffle() {
    this.props.sendPayload({ shuffle: !this.props.shuffle, repeat: false });
  }
  close() {
    if (this.props.back) {
      this.props.back();
      return;
    }
    this.props.navigation.goBack(); 
  }
  backWard() {
    this.props.backward();
  }
  forWard() {
    this.props.forward();
  }
  addToFav() {
    if (!this.props.fav) {
      this.props.addToFav(this.props.currentTrack);
      Animated.spring(this._animatedHeart, {
        toValue: 1,
        friction: 10,
        tension: 20,
      }).start();
    } else {
      this.props.removeFav(this.props.currentTrack);
      Animated.spring(this._animatedHeart, {
        toValue: 0,
        friction: 10,
        tension: 20,
      }).start();
    }
  }
  shareApp() {
    Share.share({
      message: 'حمل التطبيق الرسمي لشيخ حسين الاكرف \n على سوق بلي \nhttps://goo.gl/3qC4ht \n او على متجر ابل \n https://goo.gl/qYngkc',
      title: 'تطبيق الشيخ حسين الاكرف',
      url: 'https://goo.gl/3qC4ht',
    }, {
      dialogTitle: 'شارك تطبيق الشيخ حسين الاكرف ',
      excludedActivityTypes: [
        'com.apple.UIKit.activity.PostToTwitter',
        'com.apple.uikit.activity.mail',
      ],
      tintColor: 'blue',
    });
  }
  render() {
    const { currentTrack } = this.props;
    const Title = Animated.createAnimatedComponent(Text);
    return (
      <Container background={{ uri: currentTrack.photo }} blurRadius={0.8} >
        <View {...this.props.dragHandlers} flex={1.3}>
          <RowToolbar style={[styles.toolbar, !isAndroid && { marginTop: 16 }]} >
            <BackIcon onPress={this.close} />
            <Row alignItems='center'>
              <ButtonIconPlayer onPress={this.shareApp} name='share' />
              <Animated.View style={{
                padding: 20,
                margin: -20,
                marginLeft: 16,
                transform: [{ scale: this._animatedHeart.interpolate({
                  inputRange: [0, 0.5, 1],
                  outputRange: [1, 2, 1],
                }) }], 
              }}
              >
                <ButtonIcon
                  onPress={this.addToFav}
                  name='md-heart'
                  color={this.props.fav ? 'red' : 'white'} size={30}
                />
              </Animated.View>
              
            </Row>
          </RowToolbar>
          <View flex={1} style={[styles.padding]}>
            <Title style={[styles.title, this._animatedLeftOnPlay && {
              transform: [{
                rotate: this._animatedLeftOnPlay.interpolate({
                  inputRange: [0, 1],
                  outputRange: ['0deg', '360deg'],
                }),
              }],
            }]}
            >{currentTrack[isArabic ? 'titleAr' : 'titleEn']}</Title>
            <Row
              justifyContent='space-evenly'
              alignItems='center'
            >
              <ButtonIconPlayer
                name='random'
                onPress={this.onShuffle} 
                color={!this.props.shuffle ? COLOR_GRAY : 'white'}
              />          
              <ButtonIconPlayer
                style={isArabic && { transform: [{ scaleX: isArabic ? -1 : 1 }] }}
                name='backward' 
                onPress={this.backWard}
              />
              <ButtonIconPlayer
                onPress={this.ontogglePlay}
                name={!this.props.paused ? 'play' : 'pause'} style={styles.btnPlay}
              />
              <ButtonIconPlayer
                style={isArabic && { transform: [{ scaleX: isArabic ? -1 : 1 }] }}
                name='forward' onPress={this.forWard}
              />
              <ButtonIconPlayer
                name='repeat' 
                onPress={this.onRepeat}
                color={!this.props.repeat ? COLOR_GRAY : 'white'}
              />
            </Row>
            <Row flex={1} alignItems='center'>
              <ProgressComponent duration={this.props.duration} />
            </Row>
          </View>
        </View>
        <View flex={1.4} >
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.props.songs}
            style={[{ marginBottom: 15 }, !isAndroid ? { marginLeft: 10, marginRight: 10 } : { paddingHorizontal: 10 }]}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <SongComponent
                song={item}
                transparent
              />
            )}
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  padding: { padding: 20 },
  toolbar: { justifyContent: 'space-between', flexDirection: 'row' },
  title: { 
    fontSize: 25,
    marginBottom: 30,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  btnPlay:
  {
    width: 60, 
    height: 60,
    paddingStart: isArabic ? 1.8 : 2.5,
    paddingEnd: !isArabic ? 1.8 : 2.5,
    borderRadius: 40, 
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center', 
    borderWidth: 2,
    backgroundColor: 'rgba(255,255,255,0.4)', 
  },
});
const mapStateToProps = ({ player, songs, favs }) => ({ ...player, ...songs, favs });
export default connect(mapStateToProps, actions)(FullPlayerScreen);
