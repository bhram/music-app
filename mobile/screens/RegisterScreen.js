import React, { Component } from 'react';
import { StyleSheet, Keyboard, KeyboardAvoidingView } from 'react-native';
import { graphql, compose } from 'react-apollo';
import { Container, Text, View, Form, ButtonText, Button, FormValiMsg, Toolbar } from '../components';
import { Colors, isAndroid } from '../utils/constances';
import { Client, REGISTER } from '../graphql';
import { signIn } from '../utils/tokens';
import { ResetActionNavigation } from '../navigations/AppNaigations';
import { t } from '../locales/I18n';

class RegisterScreen extends Component {
    state = { 
      name: '',
      username: '',
      password: '',
      email: '',
      loading: false,
      errors: {} }
    componentDidMount() {
      this.handerRegister = this.handerRegister.bind(this);
    }
    
    async handerRegister() {
      Keyboard.dismiss();
      const reg = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;
      const { username, email, password, loading, name } = this.state;
      if (loading) { return; }
      if (!(reg.test(email))) {
        this.setState({ errors: { email: t('emailNotVaild') } });
        return;
      }
      if (!(username.trim().length > 2)) {
        this.setState({ errors: { email: t('userMustMoreThan3') } });
        return;
      }
      if (!(password.trim().length > 2)) {
        this.setState({ errors: { password: t('passwordMustMoreThan6') } });
        return; 
      }
      if (!(name.trim().length > 1)) {
        this.setState({ errors: { name: t('nameNotEmpty') } });
        return; 
      }
      try {
        this.setState({ loading: true, errors: {} });
        const { data } = await this.props.register({
          variables: {
            email,
            name,
            username,
            password,
          },
        });
        await signIn(data.register.token);
        await Client.resetStore();
        this.openHome();
      } catch (error) {
        if (error.graphQLErrors[0]) {
          this.setState({ errors: { email: t('emailUsed') } });
          this.password.clear();
        }
        this.setState({ loading: false });
      }
    }
    openHome() {
      this.props.navigation.dispatch(ResetActionNavigation({ routeName: 'Main' }));
    }
    render() {
      return (
        <Container startColor background={require('../assets/images/login.png')} >
          <View flex={1.4} style={{ paddingTop: 20 }}>
            <Toolbar
              titleStyle={{ fontSize: 26 }}
              title={t('signUp')} 
              onBack={() => this.props.navigation.goBack()}
            />
          </View>
          <KeyboardAvoidingView behavior='padding' enabled={!isAndroid} flex={3} >
            <Form
              icon='ios-contact'
              placeholder={t('name')}
              onChangeText={(r) => this.setState({ name: r })}
              autoCapitalize={'sentences'}
              returnKeyType={'next'}
              blurOnSubmit={false}
              onSubmitEditing={() => { this.username.focus(); }}
            />
            <FormValiMsg msg={this.state.errors.name} />                                
            <Form
              icon='ios-person'
              ref={i => { this.username = i; }}            
              placeholder={t('username')}
              onChangeText={(r) => this.setState({ username: r })}
              returnKeyType={'next'}
              blurOnSubmit={false}  
              onSubmitEditing={() => { this.email.focus(); }}                          
            />
            <FormValiMsg msg={this.state.errors.username} />                    
            <Form
              icon='ios-mail'
              ref={i => { this.email = i; }}            
              placeholder={t('email')}
              onChangeText={(r) => this.setState({ email: r })}
              returnKeyType={'next'}
              blurOnSubmit={false}
              onSubmitEditing={() => { this.password.focus(); }}              
            />
            <FormValiMsg msg={this.state.errors.email} />                    
            <Form
              icon='ios-lock'
              ref={i => { this.password = i; }}            
              placeholder={t('password')}
              secureTextEntry
              onChangeText={(r) => this.setState({ password: r })}
              returnKeyType={'done'}
            />

            <View style={styles.margin}>
              <Button
                onPress={this.handerRegister}
                loading={this.state.loading}
                title={t('register')} color={Colors.btnColors}
              />
            </View>
            <View style={styles.siwtchText}>
              <Text style={{ color: '#ccc' }} >{t('notHaveAccount')}</Text>
              <ButtonText onPress={() => this.props.navigation.navigate('Login')}>
                {t('signIn')}
              </ButtonText>
            </View>
          </KeyboardAvoidingView>
        </Container>
      );
    }
}

const styles = StyleSheet.create({
  margin: { marginTop: 15 },
  topTitle: { fontSize: 26, marginTop: 20, alignSelf: 'center' },
  padding: { paddingTop: 10, paddingHorizontal: 16, alignSelf: 'flex-end' },
  siwtchText: { flex: 1, alignItems: 'center', justifyContent: 'center' },
});

export default compose(
  graphql(REGISTER, { name: 'register' }),
)(RegisterScreen);
  
