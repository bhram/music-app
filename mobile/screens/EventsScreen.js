import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Toolbar, DrawerIcon } from '../components';
import WrappContainer from './WrappContainer';
import EventsList from '../components/EventsList';
import { t } from '../locales/I18n';

const PER_PAGE = 25;
class EventsScreen extends Component {
  state = {
    page: 0,
    refreshing: true,
  }
  componentWillMount() {
    this.fetch = this.fetch.bind(this);
    this.fetch(true);
  }
  fetch(isRefresh) {
    const { page } = this.state;
    this.props.fetchEvents(isRefresh, {
      limit: PER_PAGE,
      offset: page * PER_PAGE,
    });
    this.setState({ page: page + 1 });
  }
  render() {
    return (
      <WrappContainer background={require('../assets/images/6.jpg')}>
        <Toolbar title={t('events')} onDrawer />
        <EventsList
          refreshing={this.props.loading || false}
          onRefresh={() => this.setState({ page: 0 }, () => this.fetch(true))}
          onEndReached={() => this.fetch(false)}
          events={this.props.events} 
          transparent
        />
      </WrappContainer>
    );
  }
}
const mapStateToProps = ({ events }) => ({ ...events });
export default connect(mapStateToProps, actions)(EventsScreen);
