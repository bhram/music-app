import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Toolbar, DrawerIcon } from '../components';
import WrappContainer from './WrappContainer';
import BooksList from '../components/BooksList';
import { t } from '../locales/I18n';

const PER_PAGE = 25;
class BooksScreen extends Component {
  state = {
    page: 0,
    refreshing: true,
  }
  componentWillMount() {
    this.fetch = this.fetch.bind(this);
    this.fetch(true);
  }
  fetch(isRefresh) {
    const { page } = this.state;
    this.props.fetchBooks(isRefresh, {
      limit: PER_PAGE,
      offset: page * PER_PAGE,
    });
    this.setState({ page: page + 1 });
  }
  render() {
    return (
      <WrappContainer background={require('../assets/images/5.jpg')}>
        <Toolbar title={t('eBook')} onDrawer />
        <BooksList
          refreshing={this.props.loading || false}
          onRefresh={() => this.setState({ page: 0 }, () => this.fetch(true))}
          onEndReached={() => this.fetch(false)}
          books={this.props.books} 
          transparent
        />
      </WrappContainer>
    );
  }
}
const mapStateToProps = ({ books }) => ({ ...books });
export default connect(mapStateToProps, actions)(BooksScreen);
