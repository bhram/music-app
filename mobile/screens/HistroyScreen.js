import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Toolbar, CenterContainer, Text } from '../components';
import SongsList from '../components/SongsList';
import WrappContainer from './WrappContainer';
import { t } from '../locales/I18n';

class History extends Component {
  render() {
    return (
      <WrappContainer background={require('../assets/images/3.jpg')}>
        <Toolbar title={t('history')} onDrawer />
        {this.props.songs.length ? 
          <SongsList songs={this.props.songs} transparent />
          :
          <CenterContainer flex={1}>
            <Text>{t('empty')}</Text> 
          </CenterContainer>
        }
      </WrappContainer>
    );
  }
}
const mapStateToProps = ({ history }) => ({ ...history });
export default connect(mapStateToProps)(History);
