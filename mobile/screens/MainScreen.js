import React, { Component } from 'react';
import { View, StyleSheet, Image, FlatList } from 'react-native';
import { connect } from 'react-redux';
import Placeholder from 'rn-placeholder';
import * as actions from '../actions';
import { ButtonIcon, DrawerIcon, CenterContainer, SeeMore, Text, Button, RowToolbar, Row } from '../components';
import { Tabs } from '../components/tabs';
import Swiper from '../utils/Swiper';
import SlideSwiper from '../utils/SlideSwiper';
import SongsList from '../components/SongsList';
import VideosList from '../components/VideosList';
import BooksList from '../components/BooksList';
import WrappContainer from './WrappContainer';
import { t, isArabic } from '../locales/I18n';
import SlideComponent from '../components/SlideComponent';
import { isAndroid } from '../utils/constances';

class MainScreen extends Component {
    state = {
      loading: true,
      tabSelected: isArabic ? 2 : 0,
      tabs: [{ id: isArabic ? 2 : 0, name: t('audios') }, 
        { id: 1, name: t('videos') }, 
        { id: isArabic ? 0 : 2, name: t('books') }],
    }
    
    componentWillMount() {
      this.toggleDrawer = this.toggleDrawer.bind(this);
      this.onSelectAlbum = this.onSelectAlbum.bind(this);
      this.props.fetchProfile();
    }

    onSelectAlbum(album) {
      this.props.navigation.navigate('Album', { album });
    }
    toggleDrawer() {
      this.props.toggleDrawer();
    }

    render() {
      if (this.props.error) {
        return (
          <WrappContainer>
            <CenterContainer flex={1}>
              <Text> {t('errorConnection')}</Text>
              <Button
                onPress={() => this.props.fetchProfile()}
                title={t('tryAgain')}
              />
            </CenterContainer>
          </WrappContainer>
        );
      }
      const { loading, getAlbums, getVideos, getSongs, getBooks, navigation } = this.props;
      return (
        <WrappContainer >
          <RowToolbar style={styles.toolbar}>
            <DrawerIcon onPress={this.toggleDrawer} />
            <ButtonIcon
              onPress={() => this.props.navigation.navigate('SearchAudio')}
              name='ios-search' size={35} color='white'
            />
          </RowToolbar>
          <View style={{ flex: 0.4 }}>
            <SlideSwiper showsPagination dir='y' autoplayTimeout={3.5} autoplay loop >
              {!loading ?
                getAlbums.map((album) => (
                  <SlideComponent
                    key={album.id}
                    album={album}
                    onPress={() => navigation.navigate('Album', { album })}
                  />
                ))
                : <SlideComponent loading />}
            </SlideSwiper>
            <Image
              source={require('../assets/images/logo.jpg')} 
              style={styles.avatar}
            />
          </View>
          <View style={styles.bottomContent} flex={0.6}>
            <Tabs 
              tabs={this.state.tabs} 
              onSelect={(e) => !loading && this.swper.scrollBy(e)}
              selectedId={this.state.tabSelected}
            />
            <View style={[styles.margin]}>
              {!loading ?
                <Swiper
                  ref={ref => { this.swper = ref; }}
                  index={this.state.tabSelected}
                  showsButtons={false} showsPagination={false}
                  loop={false}
                  initialPage={isArabic ? 2 : 0}
                  onIndexChanged={(e) => {
                    this.setState({ tabSelected: e });
                  }}
                  onMomentumScrollEnd={(e, state, context) => console.log(state, context.state)}
                >
                  <Row style={{ flex: 1 }}>
                    <SongsList
                      songs={getSongs}
                      isAnimted
                      ListFooterComponent={
                        <SeeMore onPress={() => this.props.navigation.navigate('Songs')} />
                      }
                    />
                  </Row>
                  <Row style={{ flex: 1 }}>
                
                    <VideosList
                      videos={getVideos}
                      navigation={this.props.navigation}
                      ListFooterComponent={
                        <SeeMore onPress={() => this.props.navigation.navigate('Videos')} />
                      }
                    />
                  </Row>
                  <Row style={{ flex: 1 }}>
                
                    <BooksList
                      books={getBooks}
                      ListFooterComponent={
                        <SeeMore onPress={() => this.props.navigation.navigate('Books')} />
                      }
                    />
                  </Row>
                
                </Swiper> :
                <FlatList 
                  showsVerticalScrollIndicator={false}
                  scrollEnabled={false}
                  data={[0, 1, 3, 4, 5, 6, 7, 8, 9, 10]}
                  renderItem={() => (
                    <View style={{ marginBottom: 20 }}>
                      <Placeholder.ImageContent
                        size={60}
                        animate="fade"
                        lineNumber={3}
                        lineSpacing={5}
                        lastLineWidth="30%"
                        onReady={this.state.isReady}
                      />
                    </View>
                  )}                
                />
    
              }
            </View>
          </View>
        </WrappContainer>
      );
    }
}

const styles = StyleSheet.create({
  bottomContent: { paddingHorizontal: 16, backgroundColor: 'white' },
  margin: { marginTop: 20, flex: 1 },
  toolbar: { 
    justifyContent: 'space-between',
    top: !isAndroid ? 16 : 0,
    left: 0, 
    right: 0,
    zIndex: 100,
    position: 'absolute',
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
  },
});
const mapStateToProps = ({ profile }) => ({ ...profile });
export default connect(mapStateToProps, actions)(MainScreen);
