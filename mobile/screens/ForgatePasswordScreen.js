import React, { Component } from 'react';
import { Keyboard, AsyncStorage, Alert, KeyboardAvoidingView } from 'react-native';
import { graphql, compose } from 'react-apollo';
import { View, Form, Button, FormValiMsg, Toolbar } from '../components';
import { Colors, isAndroid } from '../utils/constances';
import { RecoverPassword, VertfiyCode, ChangeUserPassword } from '../graphql';
import { ResetActionNavigation } from '../navigations/AppNaigations';
import WrappContainer from './WrappContainer';
import { t } from '../locales/I18n';

const ContainerWrapp = (props) => (
  <WrappContainer blurRadius={0}>
    <View flex={1} style={{ paddingTop: 20 }}>
      <Toolbar
        titleStyle={{ fontSize: 23 }}
        title={t('accoutn_recovery')} onBack={
          () => props.navigation.goBack()}
      />
    </View>
    <KeyboardAvoidingView enabled={!isAndroid} flex={1} behavior='padding'>
      <View flex={1} >
        {props.children}
      </View>
    </KeyboardAvoidingView>
  </WrappContainer>
);
class ForgatePasswordScreen extends Component {
    state = { 
      email: '',
      password: '',
      rePassword: '',
      code: '',
      loading: false,
      errors: {}, 
    }

    async componentWillMount() {
      this.onRecoverPassword = this.onRecoverPassword.bind(this);
      this.checkCode = this.checkCode.bind(this);
      this.updatePassword = this.updatePassword.bind(this);
    }
  
    async onRecoverPassword() {
      Keyboard.dismiss();
      const { email, loading } = this.state;
      if (loading) { return; }
      try {
        this.setState({ loading: true, errors: {} });
        await this.props.recoverPassword({
          variables: {
            email,
          },
        });
        this.email = email;
        this.setState({ email: '', code: '' });
        Alert.alert(t('codeSended'));
        this.setState({ codeState: true, loading: false });
      } catch (error) {
        this.setState({ errors: { email: t('emailNotExists') }, loading: false });
      }
    }

    async checkCode() {
      Keyboard.dismiss();
      const { code, loading } = this.state;
      if (loading) { return; }
      try {
        this.setState({ loading: true, errors: {} });
        await this.props.vertfiyCode({
          variables: {
            email: this.email,
            code,
          },
        });
        this.setState({ passwordState: true, codeState: false, loading: false });
      } catch (error) {
        this.setState({ errors: { email: t('codeInvaild') }, loading: false });
      }
    }
    async skipLogin() {
      await AsyncStorage.setItem('skip', 'true');
      this.openHome();
    }
    async updatePassword() {
      Keyboard.dismiss();
      const { password, rePassword, loading } = this.state;
      if (loading) { return; }
      if (!(password.trim().length > 5)) {
        this.setState({ errors: { password: t('passwordMustMoreThan6') } });
        return; 
      }
      if (password !== rePassword) {
        this.setState({ errors: { password: t('passwordNotMatch') } });
        return;
      }
      
      try {
        this.setState({ loading: true, errors: {} });
        await this.props.changeUserPassword({
          variables: {
            email: this.email,
            password,
          },
        });
        Alert.alert(t('updatePasswordDone'));
        this.props.navigation.dispatch(ResetActionNavigation({ routeName: 'Login' }));
      } catch (error) {
        console.log(error);
        this.setState({ errors: { email: t('emailNotExists') }, loading: false });
      }
    }
    render() {
      if (this.state.codeState) {
        return (
          <ContainerWrapp navigation={this.props.navigation}>
            <Form
              icon='md-code-working'
              placeholder={t('code')}
              value={this.state.code}
              onChangeText={(tt) => this.setState({ code: tt })}
            />
            <FormValiMsg msg={this.state.errors.email} />                    
            <Button
              style={{ marginTop: 15 }}
              loading={this.state.loading}
              onPress={this.checkCode}
              title={t('enter')} color={Colors.btnColors}
            />
          </ContainerWrapp>
        );
      }
      if (this.state.passwordState) {
        return (
          <ContainerWrapp navigation={this.props.navigation}>
            <View >
              <Form
                icon='md-lock'
                placeholder={t('newPassword')}
                secureTextEntry
                onChangeText={(r) => this.setState({ password: r })}
              />
              <Form
                icon='md-lock'
                placeholder={t('rePassword')}
                secureTextEntry
                onChangeText={(r) => this.setState({ rePassword: r })}
              />
              <FormValiMsg msg={this.state.errors.password} />        
                        
              <Button
                style={{ marginTop: 15 }}
                loading={this.state.loading}
                onPress={this.updatePassword}
                title={t('updatePassword')} color={Colors.btnColors}
              />
            </View>
          </ContainerWrapp>
        );
      }
      return (
        <ContainerWrapp navigation={this.props.navigation}>
          <KeyboardAvoidingView behavior='position' >
            <Form
              icon='md-mail'
              placeholder={t('email')}
              value={this.state.email}
              onChangeText={(tt) => this.setState({ email: tt })}
            />
            <FormValiMsg msg={this.state.errors.email} />                    
            <Button
              style={{ marginTop: 15 }}
              loading={this.state.loading}
              onPress={this.onRecoverPassword}
              title={t('check')} color={Colors.btnColors}
            />
          </KeyboardAvoidingView>
        </ContainerWrapp>
      );
    }
}

export default compose(
  graphql(RecoverPassword, { name: 'recoverPassword' }),
  graphql(VertfiyCode, { name: 'vertfiyCode' }),
  graphql(ChangeUserPassword, { name: 'changeUserPassword' }),
)(ForgatePasswordScreen);

