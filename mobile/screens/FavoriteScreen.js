import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Toolbar, CenterContainer, Text } from '../components';
import SongsList from '../components/SongsList';
import WrappContainer from './WrappContainer';
import { t } from '../locales/I18n';

class Favortie extends Component {
  render() {
    return (
      <WrappContainer background={require('../assets/images/2.jpg')}>
        <Toolbar title={t('favorite')} onDrawer />
        {this.props.songs.length ? 
          <SongsList songs={this.props.songs.reverse()} transparent />
          :
          <CenterContainer flex={1}>
            <Text>{t('empty')}</Text> 
          </CenterContainer>
        }
      </WrappContainer>
    );
  }
}
const mapStateToProps = ({ favs }) => ({ ...favs });
export default connect(mapStateToProps)(Favortie);
