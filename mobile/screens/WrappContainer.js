import React from 'react';
import { Container } from '../components';

export default (props) => (
  <Container
    background={props.background || require('../assets/images/login.png')} 
    blurRadius={props.blurRadius || 0.8}
    startColor={props.startColor}
  >
    {props.children}
  </Container>
);
