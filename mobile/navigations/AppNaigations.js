import { 
  createStackNavigator,
  NavigationActions,
  StackActions,
} from 'react-navigation';
import { Easing, Animated } from 'react-native';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import SongsScreen from '../screens/SongsScreen';
import AlbumsScreen from '../screens/AlbumsScreen';
import CategoriesScreen from '../screens/CategoriesScreen';
import SearchAudioScreen from '../screens/SearchAudioScreen';
import SearchVideoScreen from '../screens/SearchVideoScreen';
import VideosScreen from '../screens/VideosScreen';
import AllAlbumsScreen from '../screens/AllAlbumsScreen';
import ForgatePasswordScreen from '../screens/ForgatePasswordScreen';
import FavoriteScreen from '../screens/FavoriteScreen';
import HistroyScreen from '../screens/HistroyScreen';
import HistroyVideosScreen from '../screens/HistoriesVideos';
import AboutScreen from '../screens/AboutScreen';
import CategoriesVideos from '../screens/CategoriesVideos';

import MainScreen from '../screens/MainScreen';
import BooksScreen from '../screens/BooksScreen';
import EventsScreen from '../screens/EventsScreen';
import EventDetails from '../screens/EventDetails';

export default createStackNavigator({
  Login: {
    screen: LoginScreen,
  },
  Register: {
    screen: RegisterScreen,
  },
  Main: {
    screen: MainScreen,
  },
  Album: {
    screen: AlbumsScreen,
  },
  Songs: {
    screen: SongsScreen,
  },
  VideosScreen: {
    screen: VideosScreen,
  },
  SearchAudio: {
    screen: SearchAudioScreen,
  },
  SearchVideo: {
    screen: SearchVideoScreen,
  },
  Albums: {
    screen: AllAlbumsScreen,
  },
  ForgatePassword: {
    screen: ForgatePasswordScreen,
  },
  Categories: {
    screen: CategoriesScreen,  
  },
  Videos: {
    screen: CategoriesVideos,  
  },
  Books: {
    screen: BooksScreen, 
  },
  Events: {
    screen: EventsScreen,  
  },
  Favorite: {
    screen: FavoriteScreen,  
  },
  History: {
    screen: HistroyScreen, 
  },
  HistoryVideos: {
    screen: HistroyVideosScreen,  
  },
  About: {
    screen: AboutScreen,  
  },
  EventDetail: {
    screen: EventDetails,
  },
}, {
  headerMode: 'none',
  mode: 'modal',
  navigationOptions: {
    gesturesEnabled: false,
  },
  transitionConfig: () => ({
    transitionSpec: {
      duration: 600,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;
      const { index } = scene;

      const height = layout.initHeight;
      const translateY = position.interpolate({
        inputRange: [index - 1, index, index + 1],
        outputRange: [height, 0, 0],
      });

      const opacity = position.interpolate({
        inputRange: [index - 1, index - 0.99, index],
        outputRange: [0, 1, 1],
      });

      return { opacity, transform: [{ translateY }] };
    },
  }),
});
  
export const ResetActionNavigation = (params) => StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate(params),
  ],
});
  
