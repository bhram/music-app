import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import JellySideMenu from '../components/JellySideMenu';
import Drawers from '../navigations/Drawers';
import * as actions from '../actions';
import ReduxNavigation from '../navigations/ReduxNavigation';

class DrawerNavigator extends PureComponent {
  componentWillReceiveProps() {
    setTimeout(() => {
      this.jsm.toggleSideMenu();
    }, 1); 
  }
  render() {
    return (
      <JellySideMenu
        ref={(view) => { this.jsm = view; }}
        fill={'#FFF'}
        fillOpacity={1.0}
        width={100}
        renderMenu={() => <Drawers />}
        onClose={() => this.props.toggleDrawer()}
      >
        <ReduxNavigation />
      </JellySideMenu>
    );
  }
}

export default connect(({ drawer }) => ({ ...drawer }), actions)(DrawerNavigator);
