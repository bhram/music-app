import React, { PureComponent } from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Icon, Text } from '../components';
import { Colors } from '../utils/constances';

class DrawerItem extends PureComponent {
  onPress=() => {
    this.view.lightSpeedOut(200).then(
      () => { 
        this.props.onPress(this.props.item);
        this.view.lightSpeedIn(0); 
      }
    );
  }
  handleViewRef = ref => { this.view = ref; };
  render() {
    const { isActive, item } = this.props;
    return (
      <TouchableOpacity style={styles.row2} onPress={this.onPress} activeOpacity={0.8}>
        <Animatable.View style={[styles.row, isActive && styles.activeDrawer]} ref={this.handleViewRef}>
          <Icon
            style={{ marginLeft: 16 }} name={item.icon} size={20}
            color={!isActive ? '#525252' : Colors.stateBar}
          />
          <Text style={[{ margin: 16 }, !isActive ? { color: '#525252' } : { color: Colors.stateBar }]}>
            {item.title}
          </Text>
        </Animatable.View>
      </TouchableOpacity>
    );
  }
}

export default DrawerItem;

const styles = StyleSheet.create({
  row: { 
    flex: 1,
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 1,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white', 
    
  },
  activeDrawer: { 
    flex: 1,
    borderLeftColor: Colors.btnColors, 
    borderRightColor: Colors.btnColors,
    borderRightWidth: 3,
    borderLeftWidth: 3,
    backgroundColor: 'rgba(0, 0, 0, .04)', 
  },
  row2: { flexDirection: 'row', backgroundColor: 'white', flex: 1, alignItems: 'center' },
});
