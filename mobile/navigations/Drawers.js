import React, { PureComponent } from 'react';
import { ScrollView, View, AsyncStorage, Image, ActivityIndicator, StyleSheet } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ActionSheet from 'react-native-actionsheet';
import { ReactNativeFile } from 'apollo-upload-client';
import MusicControl from 'react-native-music-control';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Client, UPDATE_AVATAR } from '../graphql';
import { Text, Container, ButtonIcon } from '../components';
import LineTransparent from '../components/LineTransparent';
import { Colors, isAndroid } from '../utils/constances';
import { changeLangage, t } from '../locales/I18n';
import { signOut } from '../utils/tokens';
import { ResetActionNavigation } from './AppNaigations';
import DrawerItem from './DrawerItem';

const HEIGHT = isAndroid ? 240 : 270; 
class Drawers extends PureComponent {
  constructor(props, context) {
    super(props, context);
    this.list = [
      { screen: 'Main', icon: 'ios-home', title: t('main') },
      { screen: 'Categories', icon: 'ios-list', title: t('albums') },
      { screen: 'Videos', icon: 'ios-videocam', title: t('Videos') },
      { screen: 'Books', icon: 'ios-book', title: t('eBook') },
      { screen: 'Events', icon: 'md-calendar', title: t('events') },
      { screen: 'Favorite', icon: 'md-heart-outline', title: t('favorite') },
      { screen: 'History', icon: 'md-time', title: t('history') },
      { screen: 'HistoryVideos', icon: 'md-time', title: t('historyVideos') },
      { screen: 'Langauge',
        icon: 'ios-settings',
        title: t('language'), 
        func: () => {
          this.ActionSheet.show();
        }, 
      },
      { screen: 'About', icon: 'md-information-circle', title: t('about') },
    ];
    this.state = {
      loadingAvatar: false,
      currentState: 'Main',
      list: this.list,
    };
  }
  
  async componentWillMount() {
    this.changePhoto = this.changePhoto.bind(this);
    this.onChangeScreen = this.onChangeScreen.bind(this);
  }
  componentWillReceiveProps(props) {
    if (props.me) {
      this.setState({ list: this.list.concat(this.LogoutButton) }); 
    } else {
      this.setState({ list: [...this.list, ...this.LoginButtons] }); 
    }
  }
  onChangeScreen(item) {
    if (item.func && typeof item.func === 'function') {
      item.func();
    } else {
      this.props.navigate(item.screen);
      this.props.toggleDrawer();
      this.setState({ currentState: item.screen });
    }
  }
  LogoutButton = { 
    screen: 'Logout',
    icon: 'md-log-in',
    title: t('logout'),
    func: async () => { 
      MusicControl.stopControl();
      this.props.play();
      this.props.logout();
      await signOut();
      await AsyncStorage.removeItem('skip');
      this.props.navigation.dispatch(ResetActionNavigation({ routeName: 'Login' }));
    }, 
  }
  LoginButtons = [
    { screen: 'Login',
      icon: 'ios-log-in',
      title: t('loginTwo'), 
      func: () => {
        this.props.navigate('Login', { navigate: true });
        this.props.toggleDrawer();
      } }, 
    { screen: 'Register',
      icon: 'md-log-in',
      title: t('signUp'),
      func: () => {
        this.props.navigate('Register', { navigate: true });
        this.props.toggleDrawer();
      },
    },
  ];
  changePhoto() {
    ImagePicker.launchImageLibrary({
      quality: 0.6,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    }, async (response) => {
      if (response.didCancel) {
        return;
      } else if (response.error) {
        return;
      } 
      try {
        const file = new ReactNativeFile({
          uri: response.uri,
          type: response.type,
          name: response.fileName,
        });
        this.setState({ loadingAvatar: true });
        await Client.mutate({
          mutation: UPDATE_AVATAR,
          variables: { avatar: file },
        });
        this.setState({ loadingAvatar: false, avatar: response.uri });
      } catch (error) {
        console.log(error);
      }
    });
  }
  
  renderImageButtonChild() {
    if (this.state.loadingAvatar || this.props.loading) {
      return (<ActivityIndicator size='small' color={Colors.btnColors} />);
    }
    if (this.props.me.avatar || this.state.avatar) {
      const avatar = this.state.avatar ? this.state.avatar : this.props.me.avatar;
      return (
        <Image 
          style={styles.borderView}
          source={{ uri: avatar }}
        />);
    }
    return null;
  }  
 
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ overflow: 'hidden' }}>
          <Container
            style={[styles.drawerContainer, this.props.me && { height: HEIGHT + 20 }]}
            blurRadius={6}
            background={require('../assets/images/3.jpg')}
          >
            <View style={styles.center}>
              {!this.props.me ? 
                <Image
                  style={styles.avatar}
                  source={require('../assets/images/logo.jpg')}
                />
                :
                <ButtonIcon
                  onPress={this.changePhoto}
                  style={[styles.borderView, styles.btnCamera]}
                  name='ios-camera-outline'
                  size={45}
                >
                  {this.renderImageButtonChild()}
                </ButtonIcon>
              }
              <View style={styles.nameArt}>       
                <Text
                  bold
                >
                  { t('nameArt')}
                </Text>
              </View>
              {this.props.me &&
              <View style={styles.nameArt}>       
                <Text
                  bold
                >
                  {this.props.me.name.toUpperCase()}
                </Text>
              </View>
              }
            </View>
          </Container>
          <LineTransparent />
        </View>
        <View>
          {this.state.list.map((l) => (
            <DrawerItem
              isActive={l.screen === this.state.currentState} 
              onPress={this.onChangeScreen}
              item={l} key={l.screen}
            />
          ))}
        </View>
          
        <ActionSheet
          ref={o => { this.ActionSheet = o; }}
          title={t('selectlang')}
          options={[t('arabic'), t('english'), t('Cancel')]}
          cancelButtonIndex={2}
          onPress={(index) => { 
            if (index !== 2) {
              changeLangage(index === 0);
            }
          }}
        />
      </ScrollView> 
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapp: { paddingVertical: 25,
    backgroundColor: '#f1f1f1',
    borderBottomColor: '#eee',
    borderBottomWidth: 1, 
  },

  btnCamera:
  { 
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.gray, 
  },
  borderView: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: 'center',
  },
  titleHeader: {
    paddingTop: 10,
    fontSize: 20,
    color: Colors.gray,
    alignSelf: 'center',
  },
  dropdownStyle: {
    marginLeft: 16,
    height: 100,
    width: 150,
  },
  langaugeWrapp: { 
    paddingHorizontal: 16,
    marginTop: 5,
    alignItems: 'center',
    justifyContent: 'space-between', 
  },
 
  center: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  nameArt: {
    marginTop: 10,
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderLeftColor: Colors.btnColors, 
    borderRightColor: Colors.btnColors,
    borderRightWidth: 3,
    borderLeftWidth: 3,
    backgroundColor: 'rgba(0, 0, 0, .04)', 
  },
  drawerContainer: { width: null, height: isAndroid ? 270 : 290 },
  avatar: { 
    width: 80,
    height: 80,
    borderRadius: 40, 
  },
});

const mapStateToProps = ({ profile }) => ({ ...profile });
export default connect(mapStateToProps, actions)(Drawers);

