import React, { PureComponent } from 'react';
import { BackHandler } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { AppWithNavigationState } from '../utils/Store';

class ReduxNavigation extends PureComponent {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch, navigation } = this.props;
    if (navigation.index === 0) {
      return false;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    const { navigation, dispatch } = this.props;
    return (<AppWithNavigationState 
      state={navigation}
      dispatch={dispatch}
    />);
  }
}

const mapStateToProps = state => ({
  navigation: state.nav,
});

export default connect(mapStateToProps)(ReduxNavigation);
