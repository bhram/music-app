import I18 from 'react-native-i18n';
import { I18nManager } from 'react-native';
import RNRestart from 'react-native-restart';
import en from './en';
import ar from './ar';

I18.fallbacks = true;

I18.translations = {
  en,
  ar,
};

export const changeLangage = (isArabic) => {
  I18nManager.forceRTL(isArabic);
  I18nManager.allowRTL(isArabic);
  RNRestart.Restart();
};

export const t = (word) => I18.t(word, { locale: isArabic ? 'ar' : 'en' });
export const isArabic = I18nManager.isRTL;
