import { Dimensions, Platform } from 'react-native';

export const isAndroid = Platform.OS === 'android';

export const Colors = {
  Primary: '',
  btnColors: '#6c6bce',
  stateBar: '#954978',
  gray: '#252525',
};

export const Fonts = {
  Bahij: 'BahijTheSansArabic-Light',
  BahijBold: Platform.OS === 'android' ? 'BahijTheSansArabic-Bold' : 'BahijTheSansArabicBold',
};

export const URI = 'http://vps571030.ovh.net:4000/';
// export const URI = 'http://192.168.1.15:4000/';
export const packageBundle = 'com.app.akraf';

export const WIDTH = Dimensions.get('window').width;
