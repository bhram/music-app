import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore } from 'redux-persist';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

import thunk from 'redux-thunk';
import reducers from '../reducers';
import AppNavigator from '../navigations/AppNaigations';

const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav,
);

const store = createStore(
  reducers, 
  {},
  compose(
    applyMiddleware(thunk, middleware),
  )
);
  
persistStore(store);
const AppWithNavigationState = reduxifyNavigator(AppNavigator, 'root');

export {
  store, 
  AppWithNavigationState,
};
