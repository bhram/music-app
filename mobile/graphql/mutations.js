import gql from 'graphql-tag';

export const LOGIN = gql`
  mutation login ($username:String!, $password:String!){
      login(username:$username ,password:$password ){
        token
      }
    }    
`;
export const RecoverPassword = gql`
  mutation recoverPassword($email:String!){
      recoverPassword(email:$email)
    }    
`;
export const VertfiyCode = gql`
  mutation vertfiyCode($email:String!,$code:String!){
       vertfiyCode(email:$email,code:$code)
    }    
`;
export const ChangeUserPassword = gql`
  mutation changeUserPassword($email:String!,$password:String!){
    changeUserPassword(email:$email,password:$password)
    }    
`;

export const REGISTER = gql`
  mutation register ($email:String!,$username:String!,$name:String!, $password:String!){
      register(email:$email,username:$username,name:$name ,password:$password ){
        token
      }
    }    
`;

export const UPDATE_AVATAR = gql`
  mutation setAvatar ($avatar:Upload!){
    setAvatar(avatar:$avatar )
    }     
`;

export const INCREMENT_LISTEN = gql`
  mutation incListeng ($songId:Int!){
    incListeng(songId:$songId )
    }     
`;

export const INCREMENT_VIEW_VIDEO = gql`
  mutation incShow ($videoId:Int!){
    incShow(videoId:$videoId )
  }     
`;

export const INCREMENT_BOOK_DOWNLOAD = gql`
  mutation incDownload ($bookId:Int!){
    incDownload(bookId:$bookId )
  }     
`;

export const ADD_TO_FAVOURITE = gql`
  mutation addFav ($songId:Int!){
    addFav(songId:$songId )
    }     
`;

export const REMOVE_FROM_FAVOURITE = gql`
  mutation removeFav ($songId:Int!){
    removeFav(songId:$songId )
  }     
`;

