import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { createUploadLink } from 'apollo-upload-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { getToken } from '../utils/tokens';
import { URI } from '../utils/constances';

const cache = new InMemoryCache();
const httpLink = createUploadLink({
  uri: URI,
});

const authLink = setContext(async (req, { headers }) => {
  const token = await getToken();
  return {
    ...headers,
    headers: {
      authorization: token,
    },
  };
});

const link = ApolloLink.from([
  authLink,
  httpLink,
]);
export const Client = new ApolloClient({
  link, cache,
});

export * from './mutations';
export * from './queries';

