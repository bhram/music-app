import gql from 'graphql-tag';

export const ME = gql`
{
  me{
    name
    username
  	avatar
	}
  getSongs(limit:15,sortBy:"listenCount"){
    id
    titleEn
    titleAr
    favsCount
    listenCount
    duration
    url
  	photo
    albumId
  }
  getAlbums(limit:6){
    id
    titleAr
    titleEn
    photo
    cover
    songCount
    listenCount
  }
  getVideos(limit:15,sortBy:"showCount"){ 
    id
    title
    showCount
    thumbnail
    videoId
    year
    duration
  }
  getBooks(limit:15,sortBy:"id"){ 
    id
    title
    photo
    url
    downloadCount
  }
}
`;
export const HOME_WITHOUT_LOGIN = gql`
{
  getSongs(limit:15,sortBy:"listenCount"){
    id
    titleEn
    titleAr
    favsCount
    listenCount
    duration
    url
  	photo
    albumId
  }
  getAlbums(limit:6){
    id
    titleAr
    titleEn
    photo
    cover
    songCount
    listenCount
  }
  getVideos(limit:15,sortBy:"showCount"){ 
    id
    title
    showCount
    thumbnail
    videoId
    year
    duration
  }
  getBooks(limit:15,sortBy:"id"){ 
    id
    title
    photo
    url
    downloadCount
  }
}
`;

export const GET_CATEGORIES = gql`
query($type:String){
  getCategories(type:$type) {
    id
    titleAr
    titleEn
    photo
    albumsCount
  }
}
`;
export const GET_SONG_ALBUM = gql`
 query($albumId:Int,$limit:Int,$offset:Int,$soryBy:String) {
  getSongs(albumId:$albumId, limit:$limit, offset:$offset, sortBy:$soryBy){
    id
    titleEn
    titleAr
    favsCount
    listenCount
    duration
    url
  	photo
    albumId
  }    
 }
`;
export const GET_VIDEOS = gql`
 query($categoryId:Int,$limit:Int,$offset:Int,$soryBy:String) {
  getVideos(categoryId:$categoryId,limit:$limit, offset:$offset, sortBy:$soryBy){
    id
    title
    duration
    showCount
    thumbnail
    videoId
    year
  }    
 }
`;

export const GET_BOOKS = gql`
 query($limit:Int,$offset:Int,$soryBy:String) {
  getBooks(limit:$limit, offset:$offset, sortBy:$soryBy){
    id
    title
    photo
    url
    downloadCount
  }    
 }
`;

export const GET_EVENTS = gql`
 query($limit:Int,$offset:Int) {
  getEvents(limit:$limit, offset:$offset){
    id
    title
    date
    dateEnd
    photo
  }    
 }
`;

export const GET_ALBUMS = gql`
query($categoryId:Int){
	getAlbums(categoryId:$categoryId){
    id
    titleAr
    titleEn
    photo
    cover
    songCount
    listenCount
  }
}
`;

export const SEARCH_SONGS = gql`
 query($query:String) {
  searchSongs(query:$query){
    id
    titleEn
    titleAr
    favsCount
    listenCount
    duration
    url
  	photo
    albumId
  }    
 }
`;
export const SEARCH_VIDEOS = gql`
 query($query:String) {
  searchVideos(query:$query){
    id
    title
    showCount
    thumbnail
    videoId
    year
  }    
 }
`;
