-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2018 at 06:06 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `musicapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `titleEn` varchar(255) DEFAULT NULL,
  `titleAr` varchar(255) DEFAULT NULL,
  `photo` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL DEFAULT '0',
  `categoryId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `titleEn`, `titleAr`, `photo`, `year`, `categoryId`) VALUES
(1, 'Doafaf', 'ضفاف', 'http://vps571030.ovh.net:4000//images/BJF5ytIZX.jpeg', '', 1),
(2, 'El Waad El Sadeaa', 'الوعد الصادق', 'http://vps571030.ovh.net:4000//images/SJrL7tLWX.jpeg', '', 1),
(3, 'Haq El Awda', 'حق العوده', 'http://vps571030.ovh.net:4000//images/rJNKkKLZm.jpeg', '', 1),
(4, 'Horas El Aqeda', 'حراس العقيدة', 'http://vps571030.ovh.net:4000//images/Hyz0EY8bm.jpeg', '', 1),
(5, 'Maany', 'معاني', 'http://vps571030.ovh.net:4000//images/rk9vyK8b7.jpeg', '', 2),
(6, 'Mady El Walaa', 'ماضي الولاء', 'http://vps571030.ovh.net:4000//images/S1D8JYLWX.jpeg', '', 2),
(7, 'Mnk Wa Elaik', 'منك و اليك', 'http://vps571030.ovh.net:4000//images/BJVHytIb7.jpeg', '', 2),
(8, 'Mshaaer', 'مشاعر', 'http://vps571030.ovh.net:4000//images/B1inVK8Z7.jpeg', '', 3),
(9, 'Qonsoren', 'قنسرين', 'http://vps571030.ovh.net:4000//images/B1_71tLWQ.jpeg', '', 3),
(10, 'Tazakar', 'تذكر', 'http://vps571030.ovh.net:4000//images/ByFckAUb7.jpeg', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `downloadCount` int(11) NOT NULL DEFAULT '0',
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `url`, `downloadCount`, `photo`) VALUES
(2, 'كتاب 1', 'https://book.khudher.com/books-2016/259.pdf', 0, ''),
(3, 'كتاب 2', 'https://book.khudher.com/books-2016/316.pdf', 3, ''),
(4, 'كتاب 3', 'https://book.khudher.com/books-2016/283.pdf', 2, ''),
(5, 'كتاب للتجربه ', 'http://vps571030.ovh.net:4000//books/rkIlwFYZX.pdf', 6, 'http://vps571030.ovh.net:4000//books/HySlwtF-X.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `titleAr` varchar(255) NOT NULL,
  `titleEn` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `type` enum('audios','videos') NOT NULL DEFAULT 'audios'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `titleAr`, `titleEn`, `photo`, `type`) VALUES
(1, 'ندبيات', 'Ndbeat', 'http://vps571030.ovh.net:4000//images/SkrzrhcZX.jpg', 'videos'),
(2, 'محاضرات', 'Mhadrat', 'http://vps571030.ovh.net:4000//images/rkHcB3qW7.jpg', 'audios'),
(3, 'أناشيد', 'Anasheed', 'http://vps571030.ovh.net:4000//images/SkD3Bn5bQ.jpg', 'audios'),
(4, 'asdlkas', 'sdkawdkl', 'http://localhost:4000/images/H1-6O5RmQ.png', 'videos');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `date`, `photo`) VALUES
(1, 'حفل توقيع كتاب في مدينة بيروت في البيال الساعة التاسعة مساءً', '06/06/2018', ''),
(2, 'مناسبة أخرى هنا', '06/18/2018', ''),
(3, 'مناسبة جديدة ', '06/22/2018', 'http://vps571030.ovh.net:4000//images/SJaiH0qbQ.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `songsId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `userId`, `songsId`) VALUES
(1, 2, 4),
(2, 2, 3),
(3, 3, 62),
(4, 1, 2),
(6, 4, 1),
(7, 8, 62),
(8, 8, 1),
(14, 2, 62),
(15, 2, 52),
(16, 1, 1),
(20, 2, 46),
(21, 3, 17),
(30, 9, 22),
(32, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

CREATE TABLE `songs` (
  `id` int(11) NOT NULL,
  `titleEn` varchar(255) DEFAULT NULL,
  `titleAr` varchar(255) DEFAULT NULL,
  `listenCount` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `duration` int(11) NOT NULL,
  `albumId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `songs`
--

INSERT INTO `songs` (`id`, `titleEn`, `titleAr`, `listenCount`, `url`, `duration`, `albumId`) VALUES
(1, 'Ala Defaf Al Hob', 'على ضفاف الحب', 60, 'http://melody4arab.com/music/baharain/hussain_al_akraf/doafaf/Ala_Defaf_Al_Hob_Melody4Arab.Com.mp3', 370, 10),
(2, 'Emam El Salah', 'امام الصلاة', 4, 'http://melody4arab.com/music/baharain/hussain_al_akraf/doafaf/Emam_El_Salah_Melody4Arab.Com.mp3', 343, 1),
(3, 'Harb El Mazaheb', 'حرب المذاهب', 5, 'http://melody4arab.com/music/baharain/hussain_al_akraf/doafaf/Harb_El_Mazaheb_Melody4Arab.Com.mp3', 281, 1),
(4, 'Mazraaet El Akhera', 'مزرعة الاخرة', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/doafaf/Mazraaet_El_Akhera_Melody4Arab.Com.mp3', 523, 1),
(5, 'Resala Ela Moghtareb', 'رسالة الى مغترب', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/doafaf/Resala_Ela_Moghtareb_Melody4Arab.Com.mp3', 386, 1),
(6, 'Ya Habeb Allah', 'يا حبيب الله', 6, 'http://melody4arab.com/music/baharain/hussain_al_akraf/doafaf/Ya_Habeb_Allah_Melody4Arab.Com.mp3', 354, 1),
(7, 'Al Fares', 'الفارس', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/el_waad_el_sadeaa/Al_Fares_Melody4Arab.Com.mp3', 686, 2),
(8, 'El Waad El Sadaa', 'El Waad El Sadaa', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/el_waad_el_sadeaa/El_Waad_El_Sadaa_Melody4Arab.Com.mp3', 508, 2),
(9, 'Qadary El Nasr', 'قدري النصر', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/el_waad_el_sadeaa/Qadary_El_Nasr_Melody4Arab.Com.mp3', 227, 2),
(10, 'Qadary El Nasr Maasofa', 'قدري النصر - المعزوفة ', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/el_waad_el_sadeaa/Qadary_El_Nasr_Melody4Arab.Com (1).mp3', 647, 2),
(11, 'Taman El Horaya', 'ثمن الحرية', 3, 'http://melody4arab.com/music/baharain/hussain_al_akraf/el_waad_el_sadeaa/Taman_El_Horaya_Melody4Arab.Com.mp3', 680, 2),
(12, 'Taman El Horaya Maasofa', 'ثمن الحرية - معزوفة', 2, 'http://melody4arab.com/music/baharain/hussain_al_akraf/el_waad_el_sadeaa/Taman_El_Horaya_Maasofa_Melody4Arab.Com.mp3', 202, 2),
(13, 'اتمنى', NULL, 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/haq_el_awda/Atamana_Melody4Arab.Com.mp3', 402, 3),
(14, 'Haa El Awda', 'حق العودة', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/haq_el_awda/Haa_El_Awda_Melody4Arab.Com.mp3', 353, 3),
(15, 'Kol El Gehat Ganob', 'كل الجهات جنوب', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/haq_el_awda/Kol_El_Gehat_Ganob_Melody4Arab.Com.mp3', 553, 3),
(16, 'Maa Haiaty', 'ماء حياتي', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/haq_el_awda/Maa_Haiaty_Melody4Arab.Com.mp3', 388, 3),
(17, 'Sbah El Kheer Ya Gary', 'صباح الخير يا جاري', 3, 'http://melody4arab.com/music/baharain/hussain_al_akraf/haq_el_awda/Sbah_El_Kheer_Ya_Gary_Melody4Arab.Com.mp3', 468, 3),
(18, 'Ala Aatabak', 'على اعتابك', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/horas_el_aqeda/Ala_Aatabak_Melody4Arab.Com.mp3', 687, 4),
(19, 'Dama El Hasan', 'دمع الحسن', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/horas_el_aqeda/Dama_El_Hasan_Melody4Arab.Com.mp3', 656, 4),
(20, 'Gaa Nasr Allah', 'جاء نصر الله', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/horas_el_aqeda/Gaa_Nasr_Allah_Melody4Arab.Com.mp3', 608, 4),
(21, 'Horas El Akeda', 'حراس العقيدة', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/horas_el_aqeda/Horas_El_Akeda_Melody4Arab.Com.mp3', 652, 4),
(22, 'Ma Ntghayar', 'ما نتغير', 2, 'http://melody4arab.com/music/baharain/hussain_al_akraf/horas_el_aqeda/Ma_Ntghayar_Melody4Arab.Com.mp3', 403, 4),
(23, 'Sahwanyat', 'صحوانيات', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/horas_el_aqeda/Sahwanyat_Melody4Arab.Com.mp3', 485, 4),
(24, 'Serag El Waseya', 'سراج الوصية', 3, 'http://melody4arab.com/music/baharain/hussain_al_akraf/horas_el_aqeda/Serag_El_Waseya_Melody4Arab.Com.mp3', 569, 4),
(25, 'Al Ebdaa', 'الابداع', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/maany/Al_Ebdaa_Melody4Arab.Com.mp3', 56, 5),
(26, 'El Gamal', 'الجمال', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/maany/El_Gamal_Melody4Arab.Com.mp3', 63, 5),
(27, 'El Karama', 'الكرامه', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/maany/El_Karama_Melody4Arab.Com.mp3', 59, 5),
(28, 'El Nadam', 'الندم', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/maany/El_Nadam_Melody4Arab.Com.mp3', 336, 5),
(29, 'El Taamol', 'التامل', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/maany/El_Taamol_Melody4Arab.Com.mp3', 47, 5),
(30, 'El Walaa', 'الولاء', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/maany/El_Walaa_Melody4Arab.Com.mp3', 50, 5),
(31, 'Music', 'مسيقى', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/maany/Music_Melody4Arab.Com.mp3', 68, 5),
(32, 'Alemeny', 'علميني', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mady_el_walaa/Alemeny_Melody4Arab.Com.mp3', 998, 6),
(33, 'Anaween', 'علويون', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mady_el_walaa/Anaween_Melody4Arab.Com.mp3', 492, 6),
(34, 'Hob Ahl El Bet', 'حب اهل البيت', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mady_el_walaa/Hob_Ahl_El_Bet_Melody4Arab.Com.mp3', 668, 6),
(35, 'Mn Gerahat El Hoda', 'من جراحات الهدى', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mady_el_walaa/Mn_Gerahat_El_Hoda_Melody4Arab.Com.mp3', 512, 6),
(36, 'Qad Rasamna El Darb', 'قد رسمنا الدرب', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mady_el_walaa/Qad_Rasamna_El_Darb_Melody4Arab.Com.mp3', 509, 6),
(37, 'Ya Kholoud El Sawra', 'يا خلود الثورة', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mady_el_walaa/Ya_Kholoud_El_Sawra_Melody4Arab.Com.mp3', 576, 6),
(38, 'Ya Zahraa', 'يا زهراء', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mady_el_walaa/Ya_Zahraa_Melody4Arab.Com.mp3', 528, 6),
(39, 'Lw Kont Maay', 'لو كنت معي', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Lw_Kont_Maay_Melody4Arab.Com.mp3', 554, 7),
(40, 'Maa El Aoion', 'ماء العيون', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Maa_El_Aoion_Melody4Arab.Com.mp3', 588, 7),
(41, 'Mnk Wa Elaik', 'منك و اليك', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Mnk_Wa_Elaik_Melody4Arab.Com.mp3', 406, 7),
(42, 'Saheb El Sarat', 'صاحب الثارات', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Saheb_El_Sarat_Melody4Arab.Com.mp3', 456, 7),
(43, 'Shabeeh El Mostafa', 'شبيه المصطفى', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Shabeeh_El_Mostafa_Melody4Arab.Com.mp3', 286, 7),
(44, 'Sultan El Hozn', 'سلطان الحزن', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Sultan_El_Hozn_Melody4Arab.Com.mp3', 448, 7),
(45, 'Tashyeaa El Aqsad', 'تشييع الاجساد', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Tashyeaa_El_Aqsad_Melody4Arab.Com.mp3', 542, 7),
(46, 'Tawaf El Wadaa', 'طواف الوداع', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mnk_wa_elaik/Tawaf_El_Wadaa_Melody4Arab.Com.mp3', 472, 7),
(47, 'El Hob Ya Omy', 'الحب يا امي', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mshaaer/El_Hob_Ya_Omy_Melody4Arab.Com.mp3', 604, 8),
(48, 'Hag El Domoa', 'حج الدموع', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mshaaer/Hag_El_Domoa_Melody4Arab.Com.mp3', 597, 8),
(49, 'Mangat El Asheqen', 'مناجاة العاشقين', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mshaaer/Mangat_El_Asheqen_Melody4Arab.Com.mp3', 327, 8),
(50, 'Mraaet Haiaty', 'مراة حياتي', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mshaaer/Mraaet_Haiaty_Melody4Arab.Com.mp3', 550, 8),
(51, 'Redak Aby', 'رضاك ابي', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mshaaer/Redak_Aby_Melody4Arab.Com.mp3', 651, 8),
(52, 'Wahdy Aghany', 'وحدي اغني', 7, 'http://melody4arab.com/music/baharain/hussain_al_akraf/mshaaer/Wahdy_Aghany_Melody4Arab.Com.mp3', 323, 8),
(53, 'Awraq Fe Ghaza', 'اوراق في غزة', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/qonsoren/Awraq_Fe_Ghaza_Melody4Arab.Com.mp3', 339, 9),
(54, 'Helm', 'حلم', 3, 'http://melody4arab.com/music/baharain/hussain_al_akraf/qonsoren/Helm_Melody4Arab.Com.mp3', 558, 9),
(55, 'Qonsoren', 'قنسرين', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/qonsoren/Qonsoren_Melody4Arab.Com.mp3', 355, 9),
(56, 'Ser Fe Sadry', 'سر في صدري - موال', 3, 'http://melody4arab.com/music/baharain/hussain_al_akraf/qonsoren/Ser_Fe_Sadry_Melody4Arab.Com.mp3', 120, 9),
(57, 'El Fakeer', 'الفقير', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/tazakar/El_Fakeer_Melody4Arab.Com.mp3', 843, 10),
(58, 'El Masged', 'المسجد', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/tazakar/El_Masged_Melody4Arab.Com.mp3', 550, 10),
(59, 'El Taklef', 'التكليف', 1, 'http://melody4arab.com/music/baharain/hussain_al_akraf/tazakar/El_Taklef_Melody4Arab.Com.mp3', 702, 10),
(60, 'El Yatem', 'اليتيم', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/tazakar/El_Yatem_Melody4Arab.Com.mp3', 441, 10),
(61, 'Hokok El Awlad', 'حقوق الاولاد', 0, 'http://melody4arab.com/music/baharain/hussain_al_akraf/tazakar/Hokok_El_Awlad_Melody4Arab.Com.mp3', 618, 10),
(62, 'Sefat El Motakeen', 'صفات المتقين', 14, 'http://melody4arab.com/music/baharain/hussain_al_akraf/tazakar/Sefat_El_Motakeen_Melody4Arab.Com.mp3', 511, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `avatar` blob,
  `isAdmin` tinyint(4) NOT NULL DEFAULT '0',
  `code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `username`, `avatar`, `isAdmin`, `code`) VALUES
(1, 'admin@gmail.com', 'Admin', '$2a$10$5Ri.DlqL4.otniddoVXrI.k0nD/xiFZScL3hX1.iOtPUIkIUeme.q', 'admin', NULL, 1, NULL),
(2, 'soufan@soufanmail.com', 'Mohamad soufan', '$2a$10$mV/wNQbXQHqaePnellabz.x4htbxAT6AoL4yLMA0Ip7sUD6AxBZru', 'soufan', NULL, 0, NULL),
(3, 'hevger@iibrahim.me', 'Hevger', '$2a$10$rEdnHxY0bS40tm4Gjv3.YuyWHMqPkR7PVCqV./yKCwKE/kRRkMD.i', 'Hevger', NULL, 0, NULL),
(4, 'bob@test.com', 'Ibrahim ', '$2a$10$WRCP5A51zfMklvuv2mfgqORaZNLVNZ816aBEx5mb24w1Y4zStfBRW', 'Ibrahim ', NULL, 0, NULL),
(5, 'ahodroj89@gmail.com', 'alaa', '$2a$10$1Lg5NQUAiCgn1Ar79NQBEeGiJxDb0XQAAkd0vWrBNl15wPnJ9Uw7K', 'alaa', NULL, 0, NULL),
(6, 'aa@gmail.com', 'Alaa', '$2a$10$W0nyir/u5GVGG2GvHvUjUuiOyrg8robHUU2m7KOpLPN1HbfOl1jbu', 'hod', NULL, 0, NULL),
(7, 'ali.aboud.2489@gmail.com', 'Ali ', '$2a$10$hlnwMomt/91zaHkOfvoLIOgF/mvk89o9kuPT1b10sSJNiCPYEEK8m', 'ali', NULL, 0, NULL),
(8, 'ahodroj@gmail.com', 'Alaa', '$2a$10$U/rLFCHLRu1yltEBGnYKs.hm5AL2/ZepLA/eB85KA86rs8rgiWGby', 'hodroj', NULL, 0, NULL),
(9, 'kawthar.berjawi@hotmail.com', 'Kawthar', '$2a$10$z98gHlrjkRrS52XhVg5GrONjqdEGO4.IJZfxJEnGvy49DBadccC/i', 'kawthar', NULL, 0, NULL),
(10, 'hassanjalloul6@gmail.com', 'Hassan Jalloul', '$2a$10$hzgCdp81iHElrK10vSdjaOKiqa0cw.uXzgK3H6YmFNn16YmJnqjJO', 'hassanjalloul', NULL, 0, NULL),
(11, 'bhram12@gmail.com', 'Bhram Alzuhairy', '$2a$10$5Ri.DlqL4.otniddoVXrI.k0nD/xiFZScL3hX1.iOtPUIkIUeme.q', 'bhram', NULL, 1, '2308');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `showCount` int(11) NOT NULL DEFAULT '0',
  `videoId` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL DEFAULT '0',
  `duration` int(11) NOT NULL,
  `categoryId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `showCount`, `videoId`, `thumbnail`, `year`, `duration`, `categoryId`) VALUES
(4, 'سنصلي في القدس | الشيخ حسين الأكرف', 1, 'thyszJbMJNM', 'https://i.ytimg.com/vi/thyszJbMJNM/hqdefault.jpg', '2018', 0, NULL),
(5, 'طال انتظاري | الشيخ حسين الأكرف', 5, '5ox2OP_VzK0', 'https://i.ytimg.com/vi/5ox2OP_VzK0/hqdefault.jpg', '2018', 0, NULL),
(6, 'خذك الي | الشيخ حسين الأكرف', 0, 'p3K0zk2Cbt0', 'https://i.ytimg.com/vi/p3K0zk2Cbt0/hqdefault.jpg', '2018', 558, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ada786e65651b162a26227da18d` (`categoryId`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_e747534006c6e3c2f09939da60f` (`userId`),
  ADD KEY `FK_e622906b90c52b38bd72192678c` (`songsId`);

--
-- Indexes for table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_3807642f5c436d2492f486567fc` (`albumId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `IDX_97672ac88f789774dd47f7c8be` (`email`),
  ADD UNIQUE KEY `IDX_fe0bb3f6520ee0469504521e71` (`username`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_7ceffb040a9f42475d30237f511` (`categoryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `songs`
--
ALTER TABLE `songs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `albums`
--
ALTER TABLE `albums`
  ADD CONSTRAINT `FK_ada786e65651b162a26227da18d` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `FK_e622906b90c52b38bd72192678c` FOREIGN KEY (`songsId`) REFERENCES `songs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_e747534006c6e3c2f09939da60f` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `FK_3807642f5c436d2492f486567fc` FOREIGN KEY (`albumId`) REFERENCES `albums` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `FK_7ceffb040a9f42475d30237f511` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
