import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("books")
export class Book extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    title: string;

    @Column({ nullable: false})
    url: string;

    @Column()
    photo: string;

    @Column({default: 0})
    downloadCount: number;
}
