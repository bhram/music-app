import {
  Entity,
  Column,
  BaseEntity,
  PrimaryGeneratedColumn,
  BeforeInsert,
} from "typeorm";
import * as bcrypt from "bcryptjs";

@Entity("users")
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: true, nullable: false})
  email: string;

  @Column()
  name: string;

  @Column()
  password: string;

  @Column({unique: true})
  username: string;

  @Column("text", {nullable: true})
  avatar: string;

  @Column({default: false})
  isAdmin: boolean;

  @Column({nullable: true})
  code: string;

  @BeforeInsert()
  async hashPassword() {
    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
    }
  }
}
