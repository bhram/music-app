import {
  Entity,
  BaseEntity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { User } from "./User";
import { Song } from "./Song";

@Entity("favorites")
export class Favorite extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn()
  user: User|number;

  @ManyToOne(() => Song, {onDelete: "CASCADE", onUpdate: "CASCADE"})
  @JoinColumn()
  songs: Song[]|number;
}
