import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("events")
export class Event extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    title: string;

    @Column()
    photo: string;

    @Column({ nullable: false})
    date: string;

    @Column({ nullable: true})
    dateEnd: string;
}
