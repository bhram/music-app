import {
  Entity,
  Column,
  BaseEntity,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from "typeorm";
import { Song } from "./Song";
import { Category } from "./Category";

@Entity("albums")
export class Album extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  titleEn: string;

  @Column({ nullable: true })
  titleAr: string;

  @Column()
  photo: string;

  @Column()
  cover: string;

  @Column({default: "0"})
  year: string;

  @OneToMany(() => Song, (song) => song.Album)
  songs: Song[]|number;

  @ManyToOne(() => Category, (category) => category.albums, {onDelete: "SET NULL", onUpdate: "CASCADE"})
  category: Category;
}
