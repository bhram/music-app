import {
    Entity,
    Column,
    BaseEntity,
    PrimaryGeneratedColumn,
    ManyToOne,
  } from "typeorm";
import { Category } from "./Category";

@Entity("videos")
  export class Video extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    title: string;

    @Column({default: 0})
    showCount: number;

    @Column()
    videoId: string;

    @Column()
    duration: number;

    @Column()
    thumbnail: string;

    @ManyToOne(() => Category, (category) => category.videos, {onDelete: "SET NULL", onUpdate: "CASCADE"})
    category: Category;

    @Column({default: "0"})
    year: string;
}
