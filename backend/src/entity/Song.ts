import {
  Entity,
  Column,
  BaseEntity,
  PrimaryGeneratedColumn,
  ManyToOne,
} from "typeorm";
import { Album } from "./Album";

@Entity("songs")
export class Song extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  titleEn: string;

  @Column({ nullable: true })
  titleAr: string;

  @Column({default: 0})
  listenCount: number;

  @Column({nullable: false})
  url: string;

  @Column()
  duration: number;

  @ManyToOne(() => Album, (album) => album.songs, {onDelete: "CASCADE", onUpdate: "CASCADE"})
  Album: Album;
}
