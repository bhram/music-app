import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Album } from "./Album";
import { Video } from "./Video";

@Entity("categories")
export class Category extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    titleAr: string;

    @Column()
    titleEn: string;

    @Column()
    photo: string;

    @Column({type: "enum", enum: ["audios", "videos"], default: "audios"})
    type: "audios"| "videos" | string;

    @OneToMany(() => Album, (album) => album)
    albums: Album[] | number;

    @OneToMany(() => Video, (video) => video)
    videos: Video[] | number;
}
