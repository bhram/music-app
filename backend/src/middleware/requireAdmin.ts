import { User } from "../entity/User";

export default (parent: any) =>  async (_: any, __: any, ctx: any) => {
  const msg = "Not admin";
  if (!ctx.user || !ctx.user.id) {
    throw new Error(msg);
  }
  const user = await User.findOne({ where: {id: ctx.user.id }});
  if (!user) {
    throw new Error (msg);
  }
  if (!user.isAdmin) {
    throw new Error (msg);
  }
  return parent(_, __, ctx);
};
