import * as jwt from "jsonwebtoken";
import { User } from "../entity/User";
import { SECRET } from "../utils/constance";

export const VertifyUser = async (req: any, __: any, next: any) => {
  const token = req.headers.authorization || req.session.login;
  if (token && token.length > 10) {
    try {
        const decodeToken: any = await jwt.verify(token, SECRET);
        if (decodeToken) {
          const { id } = decodeToken;
          const user = await User.findOne({where: { id }});
          req.body.user = user;
        }
      } catch (error) {
        console.log(error);
      }
    }
  next();
};
