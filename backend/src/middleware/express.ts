import { GraphQLServer } from "graphql-yoga";
import express = require("express");
import * as session from "express-session";
import * as morgan from "morgan";
import bodyParser = require("body-parser");
import { UPLOAD_FOLDER, FOLDER_CLIENT, SECRET } from "../utils/constance";

export default (server: GraphQLServer ) => {
    server.express.set("view engine", "ejs");
    server.express.use(session({
        secret: SECRET,
        resave: false,
        saveUninitialized: false,
        cookie: { maxAge: 604800000 },
      }));

    server.use(morgan("dev"));

    server.use(bodyParser.json({limit: "50mb"}));
    server.use(bodyParser.urlencoded({limit: "50mb" , extended: true }));

    server.express.use(express.static(UPLOAD_FOLDER));
    server.express.use(express.static(FOLDER_CLIENT));
};
