import "reflect-metadata";

import { GraphQLServer } from "graphql-yoga";
import { createConnection, getConnectionOptions } from "typeorm";
import { VertifyUser } from "./middleware/vertifyUser";
import { getSchema } from "../src/utils/getSchema";
import * as fs from "fs";
import { Album } from "./entity/Album";
import { Song } from "./entity/Song";
import { PORT } from "./utils/constance";
import { User } from "./entity/User";
import routes from "./routes";
import middleware from "./middleware/express";

const server = new GraphQLServer({
  schema: getSchema(),
  context: ({ request }) => {
    const baseUrl = `${request.protocol}://${request.hostname}:${PORT}`;
    return {
      user: request.body.user,
      baseUrl,
    };
  },
});

middleware(server);

routes(server);

( async () => {

const connectionOptions = await getConnectionOptions();
const options = {...connectionOptions, password: ""};
if (process.env.NODE_ENV === "server") {
  options.password = "123123";
}
createConnection(options).then(async () => {

  server.use(VertifyUser);
  const email = "admin@gmail.com";
  const user = await User.findOne({ where: { email } });
  if (!user) {
    User.create({
      id: 1,
      name: "Admin",
      username: "admin",
      email,
      password: "123123", // 123123
      isAdmin: true,
    }).save();
  }
  const seeds = false;
  if (seeds) {
    const o = await fs.readFileSync("./json.json");
    const json = JSON.parse(o.toString());

    for (const a of json) {
      const album = new Album();
      album.id = a.id;
      album.titleEn = a.titleEn;
      album.titleAr = a.titleAr;
      album.photo = a.songs[0].photo;
      await album.save();
      for (const s of a.songs) {
        const song = new Song();
        song.titleEn = s.titleEn;
        song.titleAr = s.titleAr;
        song.url = s.url;
        song.duration = s.duration;
        song.Album = album;
        await song.save();
      }
    }
  }

  server.start({
    playground: false,
    port: PORT,
    bodyParserOptions: {
      limit: "50mb",
    },
  }, () => console.log("Server is running on localhost:" + PORT));
});
})();
