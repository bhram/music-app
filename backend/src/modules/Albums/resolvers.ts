import { getConnection } from "typeorm";
import { Album } from "../../entity/Album";
import requireAdmin from "../../middleware/requireAdmin";
import { storeUpload, TYPE_FILE } from "../../utils/storeUpload";
import { deleteFile } from "../../utils";
import { Category } from "../../entity/Category";

export const resolvers = {
    Album: {
        category:  async (album: any) => {
            const a = await Category.findOne(album.categoryId);
            if (a) {
                return a.titleAr;
            }
            return "غير معروف";
        },
    },
    Query: {
        getAlbums:  async (_: any, {categoryId, limit, offset}: any) => {
          return getConnection().query(`
          SELECT * ,
          (SELECT SUM(songs.listenCount) FROM songs WHERE songs.albumId = albums.id ) as listenCount,
          (SELECT COUNT(*) FROM songs WHERE songs.albumId = albums.id ) as songCount
           from albums
           ${categoryId ? `WHERE albums.categoryId = ${categoryId}` : "" }
           ORDER by id DESC
           ${limit ? `LIMIT ${limit}` : "LIMIT 10000"}
           ${offset ? `OFFSET ${offset}` : ""}
        `);
        },
        getAlbumCount : () => {
           return Album.count();
        },

    },
    Mutation: {
        getAlbum: async (_: any, { albumId }: any) => {
            const a = await Album.findOne(albumId, { relations: ["category"]}) as any;
            a.categoryId = a.category ? a.category.id : null ;
            return a;
        },
        deleteAlbum: requireAdmin(async (_: any, { albumId }: any) => {
            const album = await Album.findOne(albumId) as Album;
            await deleteFile(album.photo);
            return album.remove();
        }),
        addAlbum: requireAdmin(async (_: any, args: any, {baseUrl}: any) => {
            const { stream, filename } = await args.photo;
            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.IMAGE});
            const url = `${baseUrl}/${newFilename}`;
            return Album.create({
                ...args,
                photo: url,
                category: args.category,
            }).save();
        }),
        updateAlbum: requireAdmin(async (_: any, args: any, {baseUrl}: any) => {
            const album = await Album.findOne(args.albumId) as Album;
            deleteFile(album.photo);
            if (album.cover) {
                deleteFile(album.cover);
            }
            const { stream, filename } = await args.photo;
            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.IMAGE});
            const url = `${baseUrl}/${newFilename}`;
            return Album.update(args.albumId, {
                ...args,
                photo: url,
                category: args.category,
            });
        }),
        updateAlbumString: requireAdmin(async (_: any, args: any) => {
            return Album.update(args.albumId, {
                ...args,
            });
        }),
        uploadPhoto: requireAdmin(async (_: any, {photo}: any, {baseUrl}: any) => {
            const { stream, filename } = await photo;
            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.IMAGE});
            return `${baseUrl}/${newFilename}`;
        }),
    },
};
