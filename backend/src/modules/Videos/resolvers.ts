import { getConnection } from "typeorm";
import { Video } from "../../entity/Video";
import { youtubeParseId, getVideoInfo } from "../../utils";
import requireAdmin from "../../middleware/requireAdmin";
import { Category } from "../../entity/Category";

export const resolvers = {
    Video: {
        category:  async (video: any) => {
            const a = await Category.findOne(video.categoryId);
            if (a) {
                return a.titleAr;
            }
            return "غير معروف";
        },
    },
    Query: {
        getVideos: (_: any, {categoryId, sortBy , limit, offset}: any) => {
            return getConnection().query(`
            SELECT * from videos
            ${categoryId ? "where categoryId = " + categoryId : ""}
            ${sortBy ? `ORDER by ${sortBy} DESC` : "ORDER by id DESC"}
            ${limit ? `LIMIT ${limit}` : "LIMIT 15"}
            ${offset ? `OFFSET ${offset}` : ""}
            `);
        },
        getVideosCount: () => {
            return Video.count();
        },
        searchVideos: (_: any, {query}: any) => {
            return getConnection().query(`select * from videos where title like '%${query}%' `);
        },
    },
    Mutation: {
        incShow: ((_: any, {videoId}: any) => {
            return getConnection()
            .getRepository(Video)
            .increment({ id: videoId }, "showCount", 1);
        }),
        getVideo: requireAdmin(async (_: any, { videoId }: any) => {
            const a = await Video.findOne(videoId, {relations: ["category"]}) as Video;
            return {...a, categoryId: a.category ? a.category.id : null};
        }),
        deleteVideo: requireAdmin(async (_: any, { videoId }: any) => {
            return Video.delete(videoId);
        }),
        addVideo: requireAdmin( async (_: any, {videoUrl, category, year}: any) => {
            try {
                const videoId = youtubeParseId(videoUrl);
                const data = await getVideoInfo(videoId);
                return Video.insert({
                    ...data,
                    year,
                    videoId,
                    category,
                });
            } catch (error) {
                console.log(error);
            }
            return false;
        }),
        updateVideo: requireAdmin(async (_: any, args: any) => {
            const videoId = youtubeParseId(args.videoUrl);
            const data = await getVideoInfo(videoId);
            return Video.update(args.videoId, {
              ...data,
              year: args.year,
              category: args.category,
              videoId,
            });
        }),
    },
};
