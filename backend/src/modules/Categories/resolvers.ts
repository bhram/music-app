import requireAdmin from "../../middleware/requireAdmin";
import { Category } from "../../entity/Category";
import { TYPE_FILE, storeUpload } from "../../utils/storeUpload";
import { deleteFile } from "../../utils";
import { getConnection } from "typeorm";

export const resolvers = {
    Query: {
        getCategories: (_: any, {type}: any) => {
            if (type === "audios") {
                return getConnection().query(`
                select * ,
                (SELECT COUNT(*) FROM albums WHERE categories.id = albums.categoryId) as
                albumsCount
                from categories
                ${type ? `where type = '${type}'` : ""}
                `);
            }
            return getConnection().query(`
            select * ,
            (SELECT COUNT(*) FROM videos WHERE categories.id = videos.categoryId) as
            albumsCount
            from categories
            ${type ? `where type = '${type}'` : ""}
            `);

        },
        getCategoriesCount: () => {
            return Category.count();
        },
    },
    Mutation: {
        deleteCategory: requireAdmin(async (_: any, { categoryId }: any) => {
            const category = await Category.findOne(categoryId) as Category;
            await deleteFile(category.photo);
            return Category.delete(categoryId);
        }),

        addCategory: requireAdmin(async (_: any, args: any, { baseUrl }: any) => {
            const { stream, filename } = await args.photo;
            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.IMAGE });
            const url = `${baseUrl}/${newFilename}`;
            return Category.create({
                ...args,
                photo: url,
            }).save();
        }),

        getCategory: requireAdmin((_: any, { categoryId }: any) => {
            return Category.findOne(categoryId);
        }),

        updateCategory: requireAdmin(async (_: any, { categoryId, titleAr, titleEn, photo }: any, { baseUrl }: any) => {
            const { stream, filename } = await photo;
            const category = await Category.findOne(categoryId) as Category;
            await deleteFile(category.photo);

            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.IMAGE });
            const url = `${baseUrl}/${newFilename}`;
            category.titleAr = titleAr;
            category.titleEn = titleEn;
            category.photo = url;
            return category.save();
        }),
        updateCategoryString: requireAdmin(async (_: any, args: any) => {
            return Category.update(args.categoryId, {
                ...args,
            });
        }),
    },
};
