import { getConnection } from "typeorm";
import requireAdmin from "../../middleware/requireAdmin";
import { Event } from "../../entity/Event";
import { TYPE_FILE, storeUpload } from "../../utils/storeUpload";
import { deleteFile } from "../../utils";

export const resolvers = {
    Query: {
        getEvents: (_: any, { limit, offset}: any) => {
            return getConnection().query(`
            SELECT * from events
            ORDER by id DESC
            ${limit ? `LIMIT ${limit}` : "LIMIT 1000"}
            ${offset ? `OFFSET ${offset}` : ""}
            `);
        },
        getEventsCount: () => {
            return Event.count();
        },
    },
    Mutation: {
        deleteEvent: requireAdmin(async (_: any, { eventId }: any) => {
            const e = await Event.findOne(eventId) as Event;
            if (e.photo) {
                await deleteFile(e.photo);
            }
            return Event.delete(eventId);
        }),

        addEvent: requireAdmin(async (_: any, {title, date, photo, dateEnd}: any, {baseUrl}: any ) => {
            let url = "";
            if (photo) {
            const { stream, filename } = await photo;
            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.IMAGE });
            url = `${baseUrl}/${newFilename}`;
            }
            return Event.insert({
                title, date, photo: url,
                dateEnd,
            });
        }),

        getEvent: requireAdmin( (_: any, {eventId}: any) => {
            return Event.findOne(eventId);
        }),

        updateEvent: requireAdmin( async (_: any, {eventId, title, date, dateEnd}: any) => {
            return Event.update(eventId, {
                title,
                date,
                dateEnd,
            });
        }),
    },
};
