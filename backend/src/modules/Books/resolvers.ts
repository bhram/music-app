import { getConnection } from "typeorm";
import { Book } from "../../entity/Book";
import requireAdmin from "../../middleware/requireAdmin";
import { storeUpload, TYPE_FILE } from "../../utils/storeUpload";
import { deleteFile } from "../../utils";

export const resolvers = {
    Query: {
        getBooks: (_: any, { sortBy , limit, offset}: any) => {
            return getConnection().query(`
            SELECT * from books
            ${sortBy ? `ORDER by ${sortBy} DESC` : "ORDER by id DESC"}
            ${limit ? `LIMIT ${limit}` : "LIMIT 15"}
            ${offset ? `OFFSET ${offset}` : ""}
            `);
        },
        getBooksCount: () => {
            return Book.count();
        },
    },
    Mutation: {
        incDownload: ((_: any, {bookId}: any) => {
            return getConnection()
            .getRepository(Book)
            .increment({ id: bookId }, "downloadCount", 1);
        }),
        deleteBook: requireAdmin(async (_: any, { bookId }: any) => {
            const book = await Book.findOne(bookId) as Book;
            if (book.url) {
                await deleteFile(book.url);
            }
            if (book.photo) {
                await deleteFile(book.photo);
            }
            return book.remove();
        }),

        uploadPhotoBook: requireAdmin(async (_: any, { photo }: any, {baseUrl}: any) => {
            const { stream, filename } = await photo;
            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.BOOKS});
            return `${baseUrl}/${newFilename}`;
        }),

        addBook: requireAdmin(async (_: any, { book, title, photo }: any, {baseUrl}: any) => {
            const { stream, filename } = await book;
            const newFilenameBook = await storeUpload({ stream, filename, typeFile: TYPE_FILE.BOOKS});
            const url = `${baseUrl}/${newFilenameBook}`;
            return Book.insert({
                url,
                title,
                photo,
            });
        }),

        getBook: requireAdmin( (_: any, {bookId}: any) => {
            return Book.findOne(bookId);
        }),

        updateBook: requireAdmin( async (_: any, {bookId, title}: any) => {
            return Book.update(bookId, {
                title,
            });
        }),
    },
};
