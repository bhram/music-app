import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";
import { SECRET } from "../../utils/constance";

import { User } from "../../entity/User";
import RequireAuth from "../../middleware/requireAuth";
import requireAdmin from "../../middleware/requireAdmin";
import { transporter } from "../../utils/mail";
import { deleteFile } from "../../utils";
import { storeUpload, TYPE_FILE } from "../../utils/storeUpload";

export const createToken = (id: any) => {
  return jwt.sign({ id }, SECRET , { expiresIn: "1y" });
};

const me = (user: any) => ({where: { id: user.id }});

export const resolvers = {
  Query: {
    me: RequireAuth(async (_: any, __: any, { user }: any) =>  User.findOne(me(user))),
  },
  Mutation: {
    recoverPassword: async (_: any, {email}: any ) => {
      const user = await User.findOne({where: {email}});
      if (user) {
        const newCode = generateCode();
        try {
          await transporter.sendMail({
            to: user.email,
            subject: "Recover Code from Akraf App",
            html: `<div style="text-align: center;">

            <h3>This is recovery code </h3><br>
            <Strong style="background: #eee;
            padding: 14px 35px;
            font-size: 20px;">
            ${newCode}
            </Strong>
            <h5>Username :</h5><br>
            <strong>${user.username}</strong>
            </div>`,
          });
          return User.update(user.id, {code: newCode});
        } catch (error) {
          console.log(error);
        }
      }
      throw new Error("email not exits ");

    },
    vertfiyCode: async (_: any, {email, code}: any ) => {
      const user = await User.findOne({where: {email}});
      if (user) {
        if (user.code === code) {
          return true;
        }
      }
      throw new Error("code not true ");
    },
    register: async (_: any, args: any) => {
      const auth = User.create(args);
      await auth.save();
      return {
        token: createToken(auth.id),
      };
    },
    login: async (_: any, { username, password }: any) => {
      const regEmail = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;
      let auth;
      if (regEmail.test(username)) {
         auth = await User.findOne({ where: { email: username } });
      } else {
        auth = await User.findOne({ where: { username } });
      }
      if (!auth) {
        throw new Error("not user found ");
      }
      const vaild = await bcrypt.compare(password, auth.password);
      if (!vaild) {
        throw new Error("password not vaild");
      }

      return {
        token: createToken(auth.id),
      };
    },
    setAvatar: RequireAuth( async (_: any, {avatar}: any, {user, baseUrl}: any) => {
      const e = await User.findOne(user.id) as User;
      if (e.avatar) {
          await deleteFile(e.avatar);
      }
      const { stream, filename } = await avatar;
      const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.IMAGE });
      const photoUrl = `${baseUrl}/${newFilename}`;
      return User.update(user.id, {
          avatar: photoUrl,
      });
    }),
    updatePassword: requireAdmin( async (_: any, {oldPassword, newPassword}: any, {user}: any) => {
      const u = await User.findOne(user.id) as User;
      const isEqual = await bcrypt.compare( oldPassword, u.password);
      if (isEqual) {
        const newPasswordHashed = await bcrypt.hash(newPassword, 10);
        u.password = newPasswordHashed;
        return u.save();
      }
      throw new Error("password not vaild");
    }),
    changeUserPassword:  async (_: any, {email, password}: any) => {
      const u = await User.findOne({where: {email}}) as User;
      const newPasswordHashed = await bcrypt.hash(password, 10);
      return User.update(u.id, {password: newPasswordHashed});
    },
  },
};

function generateCode(): string {
  const length = 4;
  const charset = "0123456789";
  let retVal = "";
  for (let i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
}
