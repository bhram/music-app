import { getConnection } from "typeorm";
import * as mm from "music-metadata";
import requireAuth from "../../middleware/requireAuth";
import { Song } from "../../entity/Song";
import { Favorite } from "../../entity/Favorite";
import requireAdmin from "../../middleware/requireAdmin";
import { storeUpload, TYPE_FILE } from "../../utils/storeUpload";
import { UPLOAD_FOLDER } from "../../utils/constance";
import { deleteFile } from "../../utils";
import { Album } from "../../entity/Album";

export const resolvers = {
    Song: {
        album: async (song: any) => {
            const a = await Album.findOne(song.albumId);
            if (a) {
                return a.titleAr;
            }
            return "غير معروف";
        },
    },
    Query: {
        getSongs: (_: any, { albumId, sortBy , limit, offset}: any) => {
            return getConnection().query(`
            SELECT songs.*,
            albums.photo,
            (SELECT COUNT(*) FROM favorites WHERE songs.id =favorites.songsId) as favsCount
            from songs
            left join albums on albums.id = songs.albumId
            ${albumId ? `WHERE songs.albumId = ${albumId}` : "" }
            ${sortBy ? `ORDER by ${sortBy} DESC` : "ORDER by id DESC"}
            ${limit ? `LIMIT ${limit}` : "LIMIT 15"}
            ${offset ? `OFFSET ${offset}` : ""}
            `);
        },
        myFavorites: requireAuth ((_: any, __: any, { user }: any) => {
            return getConnection().query(`
                SELECT songs.*,
                (SELECT COUNT(*) FROM favorites WHERE songs.id =favorites.songsId) as favsCount FROM favorites
                LEFT JOIN users on users.id = favorites.userId
                LEFT JOIN songs ON songs.id = favorites.songsId
                WHERE userId = ${user.id}
            `);
        }),
        searchSongs: (_: any, {query}: any) => {
            console.log(query);
            return getConnection().query(`
            SELECT songs.*,
            albums.photo,
            (SELECT COUNT(*) FROM favorites WHERE songs.id =favorites.songsId) as favsCount
            from songs
            left join albums on albums.id = songs.albumId
            WHERE songs.titleEn like '%${query}%' OR songs.titleAr like '%${query}%'
            `);
        },
        getSongsCount: () => {
            return Song.count();
        },

    },
    Mutation: {
        getSong: ( async (_: any, {songId}: any) => {
            const a = await Song.findOne(songId, { relations: ["Album"]}) as any;
            a.albumId = a.Album.id;
            return a;
        }),
        incListeng: ((_: any, {songId}: any) => {
            return getConnection()
            .getRepository(Song)
            .increment({ id: songId }, "listenCount", 1);
        }),
        addFav: requireAuth( async (_: any, {songId}: any, { user }: any) => {
            const data = {songs: songId, user: user.id};
            const fav = await Favorite.findOne({where: data  });
            if (!fav) {
                return Favorite.insert(data);
            }
            return null;
        }),
        removeFav: requireAuth( async (_: any, {songId}: any, { user }: any) => {
            const fav = await Favorite.findOne({where: {songs: songId, user: user.id}  }) as Favorite;
            return Favorite.delete(fav.id);
        }),
        deleteSong: requireAdmin(async (_: any, { songId }: any) => {
            const song = await Song.findOne(songId) as Song;
            await deleteFile(song.url);
            return song.remove();
        }),
        addSong: requireAdmin(async (_: any, args: any, {baseUrl}: any) => {
            const { stream, filename } = await args.audio;
            const newFilename = await storeUpload({ stream, filename, typeFile: TYPE_FILE.AUDIO});
            const meta = await mm.parseFile(`${UPLOAD_FOLDER}/${newFilename}`, {native: true});
            const url = `${baseUrl}/${newFilename}`;
            return Song.insert({
                ...args,
                url,
                Album: args.albumId,
                duration: meta.format.duration,
            });
        }),
        updateSong: requireAdmin(async (_: any, args: any) => {
            return Song.update(args.songId, {
                ...args,
                Album: args.albumId,
            });
        }),
    },
};
