import { GraphQLServer } from "graphql-yoga";
import express = require("express");
import * as bcrypt from "bcryptjs";
import { createToken } from "../modules/Auth/resolvers";
import { User } from "../entity/User";

export default (server: GraphQLServer) => {

  server.post("/login", async (req: any, res: express.Response) => {
    const { email, password } = req.body;
    const auth = await User.findOne({ where: { email } }) as User;
    if (!auth) {
      res.render("login", { error: "لايوجد ايميل مسجل  " });
    }
    if (!auth.isAdmin) {
      res.render("login", { error: "لاتمتلك صلاحية الدخول " });
    }
    const vaild = await bcrypt.compare(password, auth.password);
    if (!vaild) {
      res.render("login", { error: "كلمة السر خطا " });
    }
    req.session.login = createToken(auth.id);
    res.redirect("/");
  });

  server.express.get("/*", async (req: any, res: express.Response) => {
    if (/logout/.test(req.url)) {
      req.session.destroy();
    }
    if (req.session && req.session.login) {
      res.render("home", { token: req.session.login });
    } else {
      res.render("login", { error: false });
    }
  });

};
