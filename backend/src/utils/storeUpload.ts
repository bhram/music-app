import { createWriteStream } from "fs";
import * as mkdirp from "mkdirp";
import * as shortid from "shortid";
import { FOLDER_AUDIO, FOLDER_IMAGES, UPLOAD_FOLDER, FOLDER_BOOKS } from "./constance";

export enum TYPE_FILE {
    IMAGE,
    AUDIO,
    BOOKS,
}
interface IUpload {
    stream: any;
    filename: any;
    typeFile: TYPE_FILE;
}
mkdirp.sync(FOLDER_AUDIO);
mkdirp.sync(FOLDER_IMAGES);
mkdirp.sync(FOLDER_BOOKS);

export const storeUpload = async ({ stream, filename, typeFile }: IUpload): Promise<any> => {
    const id = shortid.generate();
    const fileExtention = filename.split(".").pop();
    const newFilename = `${id}.${fileExtention}`;
    let path = `${FOLDER_BOOKS}/${newFilename}`;
    if (typeFile === TYPE_FILE.IMAGE) {
     path = `${FOLDER_IMAGES}/${newFilename}`;
    } else if (typeFile === TYPE_FILE.AUDIO) {
     path = `${FOLDER_AUDIO}/${newFilename}`;
    }
    return new Promise((resolve, reject) =>
      stream
        .pipe(createWriteStream(path))
        .on("finish", () => resolve(path.replace(UPLOAD_FOLDER, "").replace("\\", "")))
        .on("error", reject),
    );
};
