import * as fs from "fs";
import * as path from "path";
import * as moment from "moment";
import * as URL from "url";
import axios from "axios";
import { UPLOAD_FOLDER } from "./constance";
export const youtubeParseId = (url: string) => {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\\&\\?]*).*/;
    const match = url.match(regExp);
    return (match && match[7].length === 11) ? match[7] : "";
};

export const deleteFile = (url: string ) => {

    const urlPath  = URL.parse(url).path as string ;
    const filePath = path.join(UPLOAD_FOLDER, urlPath);
    if (fs.existsSync(filePath)) {
        fs.unlinkSync(filePath);
    }
};

export const getVideoInfo =  async (videoId: string) => {
    const {data} = await axios.get(
        `https://www.googleapis.com/youtube/v3/videos?part=contentDetails,snippet&id=${videoId}
        &key=AIzaSyAK7H0kFZDbNSzk36d3Qs26Ppa5cqoO7ws`);
    const item = data.items[0];
    const title = item.snippet.title;
    const thumbnail = `https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`;
    const duration = moment.duration(item.contentDetails.duration).asSeconds();
    return {title, thumbnail, duration};
};
