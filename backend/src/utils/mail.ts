import * as nodemailer from "nodemailer";
import { GMAIL_PASS, GMAIL_USER } from "./constance";

export const transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
      user: GMAIL_USER,
      pass: GMAIL_PASS,
    },
  });
